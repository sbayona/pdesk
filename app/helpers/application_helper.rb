module ApplicationHelper

  def container_class
    controller.controller_name == 'dashboard' ? "container-fluid" : "container"
  end

  def hour_options
    {
      "12 AM" => 0, "1 AM" => 1, "2 AM" => 2, "3 AM" => 3, "4 AM" => 4, "5 AM" => 5, "6 AM" => 6, "7 AM" => 7,
      "8 AM" => 8, "9 AM" => 9, "10 AM" => 10, "11 AM" => 11, "12 PM" => 12, "1 PM" => 13, "2 PM" => 14, "3 PM" => 15,
      "4 PM" => 16, "5 PM" => 17, "6 PM" => 18, "7 PM" => 19, "8 PM" => 20, "9 PM" => 21, "10 PM" => 22, "11 PM" => 23
    }
  end

  def form_errors(object)
    render(partial: 'shared/form_errors', locals: {
      object: object
    })
  end

  def link_to_with_icon(title, url, icon, options = {})
    icon_tag = content_tag(:span, nil, class: "glyphicon glyphicon-#{icon}")
    title_with_icon = "#{icon_tag} #{h(title)}".html_safe

    link_to(title_with_icon, url, options)
  end

  def inside_layout(parent_layout)
    view_flow.set :layout, capture { yield }
    render template: "layouts/#{parent_layout}"
  end

  def account_has_one_schedule?
    current_account.schedules.count == 1
  end

  def decorated_submit(f, options={})
    options = options.merge(class: 'btn-primary', cancel_link: cancel_link(f))
    "#{f.input :submit, as: :decorated_submit, input_html: options}".html_safe
  end

  def cancel_link(f)
    scope = 'helpers.application_helper'
    if f.options[:wrapper] == 'modal_form'
      button_tag t('close', scope: scope), "class" => "btn btn-link", "data-dismiss" => "modal"
    else
      url = url_for(action: :index) rescue :back
      link_to t('cancel', scope: scope), url
    end
  end

end
