module SchedulesHelper

  def frequency_options
    scope = 'helpers.schedules_helper.frequency_options'
    {
      t('minutes', scope: scope, min: 5) => 5,
      t('minutes', scope: scope, min: 10) => 10,
      t('minutes', scope: scope, min: 15) => 15,
      t('minutes', scope: scope, min: 20) => 20,
      t('minutes', scope: scope, min: 30) => 30,
      t('hour', scope: scope) => 60
    }
  end

  def length_options
    scope = 'helpers.schedules_helper.length_options'
    %w(5 10 15 20 25 30 35 40 45 50 55 60 90 120 150).inject({}) do |hash, arr|
      hash.merge!(t('minutes', scope: scope, min: arr) => arr.to_i)
    end
  end

  def business_days_options
    t('date.abbr_day_names').each_with_index.to_h
  end

  def reminder_preference_options
    Schedule::REMINDER_PREFERENCES
  end

  def is_active?(controller, action)
    t('helpers.schedules_helper.is_active') if (controller_name == controller && action_name == action)
  end

end
