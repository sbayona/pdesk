module DashboardHelper

  def schedule_options
    options_from_collection_for_select(current_account.schedules, 'id', 'name')
  end

  def calendar_classes
    classes = ""
    classes << "one-hour-slots" if one_hour_frequency?
    classes << "half-hour-slots" if half_hour_frequency?
    classes
  end

  def one_hour_frequency?
    @dashboard.frequency == 60
  end

  def half_hour_frequency?
    @dashboard.frequency == 30
  end

end
