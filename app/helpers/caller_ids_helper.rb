module CallerIdsHelper

  def wait_time(schedule)
    distance_of_time_in_words(Time.zone.now - 5.minutes, schedule.caller_id.created_at)
  end
  
end
