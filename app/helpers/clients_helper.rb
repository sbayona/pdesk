module ClientsHelper

  def event_tab_active?(boolean)
    boolean ? 'active' : ''
  end

end
