module Mailers::UserTransactionMailHelper

  def was_rescheduled?
    @event.rescheduled?
  end

  def client
    @client ||= @event.client
  end

  def updater_name
    @updater_name ||= @event.updater.name rescue nil
  end

  def creator_name
    @creator_name ||= @event.creator.name
  end

  def old_formatted_start_time
    @event.old_start_time.to_s(:event_time)
  end

  def new_formatted_start_time
    @event.new_start_time.to_s(:event_time)
  end

  def old_formatted_datetime
    @event.old_start_time.to_s(:event_datetime)
  end

  def old_formatted_datetime
    @event.new_start_time.to_s(:event_datetime)
  end

  def old_formatted_start_date
    @event.old_start_time.to_s(:event_date)
  end

  def new_formatted_start_date
    @event.new_start_time.to_s(:event_date)
  end

  def changed_on_the_same_day?
    @event.on_the_same_day?
  end

end
