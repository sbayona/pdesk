module Mailers::ClientTransactionMailHelper

  def cancellation_link
    cancel_event_url(id: @event.uuid, t: @event.client.token, protocol: "https")
  end

  def confirmation_link
    confirm_event_url(id: @event.uuid, t: @event.client.token, protocol: "https")
  end

  def unsubscribe_link
    unsubscribe_url(token: @event.client.token, protocol: "https")
  end

  def account_name
    @event.account.name
  end

end
