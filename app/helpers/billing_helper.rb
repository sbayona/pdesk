module BillingHelper

  def credit_card_info
    card = ActiveSupport::SafeBuffer.new
    return card << "No credit card info." unless current_account.card.present?
    card << content_tag(:strong) do
      current_account.card_brand
    end
    card << " Last four digits: "
    card << content_tag(:strong) do
      current_account.card_last4
    end
    card << " Expiration: "
    card << content_tag(:strong) do
      current_account.card_expiration
    end
  end

  def subscription_info(subscription_plan)
    scope = 'helpers.billing_helper'
    "#{t('used', scope: scope).capitalize} #{current_account.month_usage} #{t('of', scope: scope)} #{max_events_per_month(subscription_plan)} #{current_account.event_name} #{t('this_month', scope: scope)}."
  end

  def remaining_events(subscription_plan)
    scope = 'helpers.billing_helper'
    "#{(current_account.month_usage - max_events_per_month(subscription_plan)).abs} #{current_account.event_name} #{t('remaining', scope: scope)}."
  end


  def next_payment_due
    sf = ActiveSupport::SafeBuffer.new
    sf << "Next payment due: "
    sf << content_tag(:strong) do
      if current_account.next_payment_due.present?
        current_account.next_payment_due.strftime('%B %-d, %Y')
      else
        "No payment scheduled"
      end
    end
  end

  def max_events_per_month(subscription_plan)
    subscription_plan.max_events_per_month
  end

  def plan_selector_button(subscription_plan)
    text = subscription_plan.nil? ? "Select Plan" : "Change Plan"
    link_to text, edit_subscription_plans_path, class: "action-button btn btn-default", "data-toggle" => "ajaxModal"
  end

  def progress_bar(subscription_plan)
    usage = current_account.month_usage
    max_events = subscription_plan.max_events_per_month
    percent = events_usage(usage, max_events)
    content_tag(:div, class: 'progress') do
     content_tag(:div, "class" => "progress-bar", "role" => "progressbar", "aria-valuenow" => "#{usage}", "aria-valuemin" => "0", "aria-valuemax" => "#{max_events}", "style" => "width: #{percent}%") do
       "#{percent}"
     end
    end
  end

  def events_usage(usage, max)
    ((usage.to_f/max.to_f) * 100).to_i
  end


  def update_payment_button(subscription_plan)
    options = {class: "action-button btn btn-default", "data-no-turbolink": true}
    options.merge!(disabled: true, title: "Please select a plan first") unless subscription_plan
    link_to "Update payment method", edit_payments_path, options
  end
end
