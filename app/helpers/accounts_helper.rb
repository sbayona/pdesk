module AccountsHelper
  def client_name_options
    I18n.t('helpers.accounts_helper.client_name').invert
  end

  def event_name_options
    I18n.t('helpers.accounts_helper.event_name').invert
  end
end
