# encoding: utf-8
module EventsHelper

  def client_name
    @client_name ||= (@event && @event.account_client_name) || current_account.client_name
  end

  def event_name
    @event_name ||= (@event && @event.account_event_name) || current_account.event_name
  end


  def formatted_event_datetime(event)
    return "#{event.start_time.to_s(:event_datetime)}" if event.start_time.present? && event.end_time.blank?
    if normalize_datetime(event.start_time) == normalize_datetime(event.end_time)
      "#{event.start_time.to_s(:event_datetime)} – #{event.end_time.to_s(:event_time)}"
    else
      "#{event.start_time.to_s(:event_datetime)} to #{event.end_time.to_s(:event_datetime)}"
    end
  end

  def formatted_event_date(event)
    event.start_time.to_s(:event_date)
  end

  def formatted_event_time(event)
    "#{event.start_time.to_s(:event_time)} - #{event.end_time.to_s(:event_time)}"
  end

  def reminder_times
    reminder_times = @event.trigger_times.collect do |tt|
      if tt["offset_in_seconds"].zero?
        "Immediately after booking"
      else
        ((@event.start_time - tt["days_before"].to_i) - tt["offset_in_seconds"]).to_s(:short_datetime)
      end
    end.to_sentence
  end

  def normalize_datetime(datetime)
    datetime.strftime("%a, %B %-d")
  end

  def indefinite_articlerize(params_word)
    %w(a e i o u).include?(params_word[0].downcase) ? "an #{params_word}" : "a #{params_word}"
  end

  def period_menu(form)
    form.select :period, period_options, {include_blank: true}
  end

  def period_options
    {"yearly" => :yearly, "monthly" => :monthly, "weekly" => :weekly, "daily" => :daily}
  end

  def event_status(event)
    if event.has_no_contact_info
      t('.no_contact_info', client_name: client_name)
    else
      t("models.event.states.#{event.state}", client_name: client_name, event_name: event_name)
    end
  end

  def event_state
    @event.state && @event.state.humanize
  end

  def spoken_start_time
    date = @event.start_time.strftime("%A, %B #{@event.start_time.day.ordinalize}")
    minutes = @event.start_time.strftime("%M")
    hour = @event.start_time.strftime("%l")
    meridian = @event.start_time.strftime("%P")
    if minutes == "00"
      time = "#{hour}#{meridian}"
    else
      time = "#{hour}:#{minutes}#{meridian}"
    end
    "#{time} #{date}"
  end

  def event_title
    "#{@event.client.name} &mdash; #{@event.start_time.to_s(:short_datetime)}".html_safe
  end

  def end_time_menu(event)
    "test"
  end

  def event_editable?(event)
    return true if unavailable_mode? #always editable on unavailable mode
    event.unavailable_time? ? false : true #cannot edit unavailable events
  end

end
