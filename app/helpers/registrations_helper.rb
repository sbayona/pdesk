module RegistrationsHelper
  def subscription_plan_options
    SubscriptionPlan.active.collect {|a| [plan_name(a), a.id]}
  end

  def plan_name(plan)
    scope = 'helpers.registrations_helper.plan_name'
    return t('no_plan', scope: scope) if plan.nil?
    "#{plan.name} - #{money_without_cents_and_with_symbol(plan.price)}/#{t('month', scope: scope)}"
  end

  def expiration_month_options
    months = [['', '']]
    (1..12).each {|m| months << ["#{m} - #{Date::MONTHNAMES[m]}", m]}
    options_for_select months
  end

  def expiration_year_options
    years = (Date.today.year..Date.today.year + 10).to_a
    options_for_select years
  end

  def registering?
    controller_name == 'registrations'
  end
end
