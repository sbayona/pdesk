module ScheduleTimingsHelper

  def days_before_options(option)
    options = ScheduleTiming::DAYS_BEFORE.collect {|p| [ p['LABEL'], p['KEY'] ] }
    options_for_select(options, option['days_before'])
  end

  def timing_options(option)
    options = ScheduleTiming::OPTIONS.collect{|a| [a["LABEL"], a["KEY"]] }
    options_for_select(options, option['value'])
  end

end
