class ScheduleSubscription < ActiveRecord::Base

  belongs_to :user
  belongs_to :schedule

  validates_presence_of :user
  validates_presence_of :schedule

  validates_uniqueness_of :user_id, scope: :schedule_id

  validate :less_than_three_entries_per_schedule

  def less_than_three_entries_per_schedule
    return if schedule.nil? || errors.present?
    errors.add(:base, :entries_per_schedule) if schedule.schedule_subscriptions.count >= 3
  end
end
