class Note < ActiveRecord::Base
  belongs_to :account
  belongs_to :event

  validates :description, :account, :event, presence: true

  delegate :name, to: :account, prefix: true

  scope :sorted, -> {order(:created_at)}
end
