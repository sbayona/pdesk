class DemoPhoneCall
  include Concerns::Twilio

  def initialize(user)
    @user = user
  end

  def run
    demo = create_greeting_demo
    call(demo.uuid)
    demo
  end

  def call(uuid)
    make_call from: caller_id, to: recipient_number, url: reminder_demo_uri(uuid)
  end

  def create_tts_file
    @speech ||= begin
      temp = Tempfile.new("tts")
      temp.binmode
      temp.write(create_speech.response.body)
      temp
    end
  end

  def create_speech
    client.create_speech(demo_speech)
  end

  def recipient_number
    "+14049695121"
  end

  def reminder_demo_uri(uuid=nil)
    reminder_demo_url(host: ENV['HOST'], format: 'xml')
  end

  def create_greeting_demo
    demo = @user.demos.build
    file = create_tts_file
    demo.greeting = file
    demo.save

    file.close
    file.unlink
    demo
  end

  def demo_speech
    I18n.t('.models.demo_script.voice', account_name: account_name, appointment_datetime: demo_datetime)
  end

  def demo_datetime
    @demo_datetime ||= Time.now.tomorrow.to_datetime.to_s(:short_datetime)
  end

  def account_name
    @user.account_name
  end

  def client
    @client = IvonaSpeechCloud::Client.new do |config|
      config.access_key = ENV["IVONA_ACCESS_KEY"]
      config.secret_key = ENV["IVONA_SECRET_KEY"]
      config.region = ENV["IVONA_REGION"]
    end
  end
  
  def caller_id
    ENV['TWILIO_NUMBER']
  end
end

