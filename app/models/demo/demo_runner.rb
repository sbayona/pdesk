class DemoRunner

  def initialize(user)
    @user = user
  end

  def perform
    make_demo_phone_call
    send_demo_sms
    send_demo_email
  end

  def make_demo_phone_call
    DemoPhoneCall.new(@user).run
  end
  
end
