class ScheduleTiming < ActiveRecord::Base
  belongs_to :schedule
  has_many :events

  include Concerns::ScheduleTimingHelpers

  DAYS_BEFORE = [
    { "KEY" => nil, "LABEL" => I18n.t('models.schedule_timing.days_before.nil_label') },
    { "KEY" => 0, "LABEL" => I18n.t('models.schedule_timing.days_before.0_label') },
    { "KEY" => 1, "LABEL" => I18n.t('models.schedule_timing.days_before.1_label') },
    { "KEY" => 2, "LABEL" => I18n.t('models.schedule_timing.days_before.2_label') },
    { "KEY" => 3, "LABEL" => I18n.t('models.schedule_timing.days_before.3_label') },
    { "KEY" => 4, "LABEL" => I18n.t('models.schedule_timing.days_before.4_label') },
    { "KEY" => 5, "LABEL" => I18n.t('models.schedule_timing.days_before.5_label') },
    { "KEY" => 6, "LABEL" => I18n.t('models.schedule_timing.days_before.6_label') },
    { "KEY" => 7, "LABEL" => I18n.t('models.schedule_timing.days_before.7_label') },
    { "KEY" => 8, "LABEL" => I18n.t('models.schedule_timing.days_before.8_label') },
    { "KEY" => 9, "LABEL" => I18n.t('models.schedule_timing.days_before.9_label') },
    { "KEY" => 10, "LABEL" => I18n.t('models.schedule_timing.days_before.10_label') },
    { "KEY" => 11, "LABEL" => I18n.t('models.schedule_timing.days_before.11_label') },
    { "KEY" => 12, "LABEL" => I18n.t('models.schedule_timing.days_before.12_label') },
    { "KEY" => 13, "LABEL" => I18n.t('models.schedule_timing.days_before.13_label') },
    { "KEY" => 14, "LABEL" => I18n.t('models.schedule_timing.days_before.14_label') },
    { "KEY" => 15, "LABEL" => I18n.t('models.schedule_timing.days_before.15_label') },
  ]



  INTERVAL_TIME_OPTIONS = [
      { "KEY" => nil, "LABEL" => I18n.t('models.schedule_timing.interval_time_options.nil_label') },
      { "KEY" => "0", "LABEL" => I18n.t('models.schedule_timing.interval_time_options.0_label') },
      { "KEY" => "15",  "LABEL" => I18n.t('models.schedule_timing.interval_time_options.15_label') },
      { "KEY" => "30",  "LABEL" => I18n.t('models.schedule_timing.interval_time_options.30_label') },
      { "KEY" => "60",  "LABEL" => I18n.t('models.schedule_timing.interval_time_options.60_label') },
      { "KEY" => "90",  "LABEL" => I18n.t('models.schedule_timing.interval_time_options.90_label') },
      { "KEY" => "120", "LABEL" => I18n.t('models.schedule_timing.interval_time_options.120_label') },
      { "KEY" => "150", "LABEL" => I18n.t('models.schedule_timing.interval_time_options.150_label') },
      { "KEY" => "180", "LABEL" => I18n.t('models.schedule_timing.interval_time_options.180_label') },
      { "KEY" => "210", "LABEL" => I18n.t('models.schedule_timing.interval_time_options.210_label') },
      { "KEY" => "240", "LABEL" => I18n.t('models.schedule_timing.interval_time_options.240_label') },
      { "KEY" => "270", "LABEL" => I18n.t('models.schedule_timing.interval_time_options.270_label') },
      { "KEY" => "300", "LABEL" => I18n.t('models.schedule_timing.interval_time_options.300_label') }
  ]



  FIXED_TIME_OPTIONS = [
      { "KEY" => "00:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.0_label') },
      { "KEY" => "01:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.1_label') },
      { "KEY" => "02:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.2_label') },
      { "KEY" => "03:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.3_label') },
      { "KEY" => "04:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.4_label') },
      { "KEY" => "05:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.5_label') },
      { "KEY" => "06:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.6_label') },
      { "KEY" => "07:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.7_label') },
      { "KEY" => "08:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.8_label') },
      { "KEY" => "09:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.9_label') },
      { "KEY" => "10:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.10_label') },
      { "KEY" => "11:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.11_label') },
      { "KEY" => "12:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.12_label') },
      { "KEY" => "13:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.13_label') },
      { "KEY" => "14:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.14_label') },
      { "KEY" => "15:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.15_label') },
      { "KEY" => "16:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.16_label') },
      { "KEY" => "17:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.17_label') },
      { "KEY" => "18:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.18_label') },
      { "KEY" => "19:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.19_label') },
      { "KEY" => "20:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.20_label') },
      { "KEY" => "21:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.21_label') },
      { "KEY" => "22:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.22_label') },
      { "KEY" => "23:00", "LABEL" => I18n.t('models.schedule_timing.fix_time_options.23_label') }
    ]

  OPTIONS = INTERVAL_TIME_OPTIONS + FIXED_TIME_OPTIONS

  validates_presence_of :name
  validates_presence_of :schedule

  before_validation :set_first_record_as_default

  private

  def set_first_record_as_default
    return if schedule.nil?

    # if marked default, make all other record 'not default' execept the current oen
    if self.default?
      schedule.schedule_timings.where.not(id: id).update_all(default: false)
    end

    self.default = true if schedule.schedule_timings.empty?
  end

  def self.default_values
    {
      name: I18n.t('models.schedule_timing.default_value'),
      default: true,
      options: [
        {value: 60, days_before: 0},
        {value: 60, days_before: 1}
      ]
    }
  end

end
