class EventReport
  class Query
    include Virtus.model(strict: true, nullify_blank: true)

    attribute :user_id, Integer, required: false
    attribute :schedule_id, Integer, required: false
    attribute :client_id, Integer, required: false
    attribute :state, String, required: false

    attribute :date_range_left, DateTime, required: false
    attribute :date_range_right, DateTime, required: false

    attribute :start_time, Object, required: false, lazy: true, default: :set_start_time

    def criteria
      {}.tap { |h|
        h.update(schedule_id: schedule_id) if schedule_id
        h.update(client_id: client_id) if client_id
        h.update(state: state) if state_valid?
        h.update(user_id: user_id) if user_id
        h.update(start_time: start_time) if start_time
      }
    end

    def state_valid?
      Event::STATES.include?(state)
    end

    private

    def set_start_time
      left = date_range_left
      right = date_range_right

      return unless left && right

      if left != right
        left.beginning_of_day..right.end_of_day
      else
        left.beginning_of_day..left.end_of_day
      end
    end
  end

  attr_reader :account, :page, :query

  delegate :total_pages, :current_page, :limit_value, to: :events

  def initialize(account, options = {})
    @account = account
    @page = options.fetch(:page, 1)
    @query = Query.new(options.fetch(:q, {}))
  end

  def each
    events.each { |event| yield(event) }
  end

  def schedules
    account.schedules.order(:name)
  end

  def clients
    account.clients.order(:name)
  end

  def users
    account.users.sort_by(&:name)
  end

  def states
    Event::STATES
  end

  # Lazy-load events so that pagination can work regardless of call order
  def events
    @events ||= account.events.where(query.criteria).page(page)
  end
end
