class Account < ActiveRecord::Base
  attr_accessor :stripe_token, :validate_subscription_plan

  has_paper_trail on: [:update, :create], only: [:event_name, :client_name]

  include Concerns::AccountState

  has_many :users, dependent: :destroy
  has_many :clients, dependent: :destroy
  has_many :schedules, dependent: :destroy
  has_many :events
  has_many :charges
  has_one :account_owner
  belongs_to :subscription_plan
  has_many :notes

  validates :name, :client_name, :event_name, :time_zone, presence: true
  #validate subscription plan after the account has been created
  validates_presence_of :subscription_plan, if: :validate_subscription_selection?
  validates_numericality_of :trial_period_in_days, allow_nil: true
  validates_uniqueness_of :name, allow_nil: true

  after_create :create_default_schedule!
  after_update :update_liquid_templates
  before_save :update_stripe
  before_create :set_trial_period
  before_destroy :delete_stripe_subscriptions

  delegate :email, to: :account_owner, allow_nil: true, prefix: false
  delegate :last4, :brand, :expiration, to: :card, allow_nil: true, prefix: true


  scope :on_trial, -> {
    trial_date = "(created_at + CAST(trial_period_in_days || ' days' AS INTERVAL))"
    where("#{trial_date} > '#{Time.zone.now}'").reorder(trial_date)
  }


  def validate_subscription_selection?
    @validate_subscription_plan
  end

  def month_usage
    events.this_month.count
  end

  def scripts
    @scripts ||= self.schedules.map(&:email_scripts).flatten
  end

  def last_version
    @last_version ||= self.versions.last.reify if self.versions.any?
  end

  def card
    @card ||= StripeCard.new(stripe_default_card)
  end

  def stripe_customer
    return if stripe_id.nil?
    @stripe_customer ||= Stripe::Customer.retrieve(stripe_id)
  end

  def upcoming_invoice
    @upcoming_invoice ||= begin
      StripeInvoice.new(stripe_customer.upcoming_invoice)
    rescue Exception => e
      StripeInvoice.new
    end
  end

  def next_payment_due
    upcoming_invoice.date
  end

  def stripe_deleted?
    stripe_customer.respond_to?(:deleted)
  end

  def trial_period_ends_on
    created_at + trial_period_in_days.send('days')
  end

  def trial_period_ends_on_days
    (trial_period_ends_on.to_date - Time.zone.now.to_date).to_i
  end

  def trial_expired?
    trial_period_ends_on_days <= 0
  end

  def needs_subscription_info?
    trial_expired? && subscription_plan.blank? && stripe_subscription_id.blank?
  end

  def trial_about_to_expire?
    trial_period_ends_on_days < 7 && subscription_plan.blank? && stripe_subscription_id.blank?
  end

  private

  def set_trial_period
    self.trial_period_in_days ||= begin
      attributes["trial_period_in_days"].present? ?
      attributes["trial_period_in_days"] :
      ENV['TRIAL_PERIOD_IN_DAYS']
    end
  end

  def create_default_schedule!
    self.schedules.create!(Schedule::DEFAULT_VALUES)
  end

  def update_liquid_templates
    return unless event_name_changed? || client_name_changed?
    LiquidTemplateUpdater.new(self).update!
  end


  def stripe_description
    name
  end

  def update_stripe
    return if Rails.env.test?
    if stripe_id.nil?
      if stripe_token.present?
        customer = create_stripe_customer
        subscription = customer.update_subscription(plan: subscription_plan.stripe_code)
        self.stripe_subscription_id = subscription.id
        self.last_4_digits = customer.cards.first.last4
      end
    else
      customer = stripe_customer
      customer.card = stripe_token if stripe_token.present?

      # in case they've changed
      if subscription_plan_id_changed?
        subscription = customer.update_subscription(plan: subscription_plan.stripe_code)
        self.stripe_subscription_id = subscription.id
      end
      customer.email = email
      customer.description = stripe_description
      customer.save

      self.last_4_digits = customer.cards.first.last4 if customer.cards.any?
    end

    self.stripe_id = customer.id if customer
    self.stripe_token = nil
  end

  def stripe_default_card_id
    stripe_customer && stripe_customer['default_card']
  end

  def stripe_default_card
    return if stripe_default_card_id.nil?
    stripe_customer.cards.retrieve(stripe_default_card_id)
  end

  def create_stripe_customer
    Stripe::Customer.create(
      :email => email,
      :description => stripe_description,
      :source => stripe_token
    )
  end

end
