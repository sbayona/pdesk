class EventLog < ActiveRecord::Base
  serialize :data
  serialize :error

  SOURCE = {REQUEST_CONFIRMATION: I18n.t('models.event_logs.event_log.request_confirmation'), RESCHEDULE: I18n.t('models.event_logs.event_log.reschedule')}
  USER_TYPE = { CLIENT: I18n.t('models.event_logs.event_log.client'), USER: I18n.t('models.event_logs.event_log.user')}
  LOG_TYPE = {EMAIL: I18n.t('models.event_logs.event_log.email_log'), SMS: I18n.t('models.event_logs.event_log.sms_log'), VOICE: I18n.t('models.event_logs.event_log.voice_log')}

  scope :for_clients, -> { where(user_type: USER_TYPE[:CLIENT]) }
  scope :for_users, -> { where(user_type: USER_TYPE[:USER]) }
  scope :reschedules, -> { where(source: SOURCE[:RESCHEDULE]) }
  scope :confirmations, -> { where(source: SOURCE[:REQUEST_CONFIRMATION]) }

end
