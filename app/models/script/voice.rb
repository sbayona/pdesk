class Script::Voice < ActiveRecord::Base

  OPTIONS = { PRERECORDED: 0, CUSTOM: 1 }

  def self.default_values(account)
    {
      message: I18n.t('models.script.voice.message', account_name: account.name,event_name: account.event_name),
      confirmation_message: I18n.t('models.script.voice.confirmation_message',event_name: account.event_name),
      cancellation_message:  I18n.t('models.script.voice.cancellation_message',event_name: account.event_name),
      requested_contact_message: I18n.t('models.script.voice.requested_contact_message')
      }
  end

end
