class Script::Sms < ActiveRecord::Base
  include Concerns::Articlerize

  def self.default_values(account)
    {
      message: I18n.t('models.script.sms.message',an_event_name: self.indefinite_articlerize(account.event_name), account_name: account.name, event_name: account.event_name),
      confirmation_message: I18n.t('models.script.sms.confirmation_message',event_name: account.event_name),
      cancellation_message:  I18n.t('models.script.sms.cancellation_message',event_name: account.event_name),
      requested_contact_message: I18n.t('models.script.sms.requested_contact_message')
      }
  end
end
