class Script::Email < ActiveRecord::Base
  belongs_to :user

  include Concerns::Articlerize

  def self.default_values(account)
    {
        from_email: ENV['DEFAULT_SENDER_EMAIL'],
        reply_to_email: ENV['DEFAULT_REPLY_TO_EMAIL'],
        subject: I18n.t('models.script.email.subject',event_name: account.event_name, account_name: account.name),
        message: I18n.t('models.script.email.subject_body', event_name_first: self.indefinite_articlerize(account.event_name), client_name: account.client_name, account_name: account.name, event_name: account.event_name),
        cancellation_subject: I18n.t('models.script.email.cancellation_subject',event_name: account.event_name, account_name: account.name),
        cancellation_message:  I18n.t('models.script.email.cancellation_body', event_name: account.event_name, client_name: account.client_name, account_name: account.name),
        reschedule_subject: I18n.t('models.script.email.reschedule_subject',event_name: account.event_name, account_name: account.name),
        reschedule_message: I18n.t('models.script.email.reschedule_body', event_name: account.event_name, client_name: account.client_name, account_name: account.name)
      }
  end

end
