class Demo < ActiveRecord::Base
  belongs_to :user
  mount_uploader :greeting, GreetingUploader
end
