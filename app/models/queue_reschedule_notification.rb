
class QueueRescheduleNotification
  def self.run!
    Event.transaction do
      pending_for_client.each { |event| notify_client(event) }
      pending_for_users.each { |event| notify_user(event) }
    end
  end

  def self.start_time
    Time.now.utc - 1.hour
  end

  def self.end_time
    Rails.env.production? ? Time.now.utc - 3.minutes : Time.now.utc
  end

  def self.notify_client(event)
    log = event.email_logs.create!(source: EventLog::SOURCE[:RESCHEDULE], user_type: EventLog::USER_TYPE[:CLIENT])
    Delayed::Job.enqueue ClientRescheduleEmailJob.new(event.id, log.id), queue: "reschedules"
  end

  def self.notify_user(event)
    log = event.email_logs.create!(source: EventLog::SOURCE[:RESCHEDULE], user_type: EventLog::USER_TYPE[:USER])
     Delayed::Job.enqueue UserRescheduleEmailJob.new(event.id, log.id), queue: "reschedules"
  end

  def self.pending_for_client
    Event
      .unscoped
      .select("e.*")
      .from("events AS e")
      .joins("LEFT OUTER JOIN event_logs AS el ON el.event_id = e.id AND el.type = 'EmailLog' AND el.source = '#{EventLog::SOURCE[:RESCHEDULE]}' AND el.user_type = '#{EventLog::USER_TYPE[:CLIENT]}' AND el.created_at > e.rescheduled_at")
      .where(["e.rescheduled_at BETWEEN ? AND ?", start_time, end_time])
      .where("e.deleted_at IS NULL")
      .where("e.state IN ('pending', 'confirmed') AND el.event_id IS NULL")
      .where(["e.start_time > ?", Time.now.utc]) #events with a start time in the past are not relevant
  end

  def self.pending_for_users
    Event
      .unscoped
      .select("e.*")
      .from("events AS e")
      .joins("LEFT OUTER JOIN event_logs AS el ON el.event_id = e.id AND el.type = 'EmailLog' AND el.source = '#{EventLog::SOURCE[:RESCHEDULE]}' AND el.user_type = '#{EventLog::USER_TYPE[:USER]}' AND el.created_at > e.rescheduled_at")
      .where(["e.rescheduled_at BETWEEN ? AND ?", start_time, end_time])
      .where("e.deleted_at IS NULL")
      .where("e.state IN ('pending', 'confirmed') AND el.event_id IS NULL")
      .where(["e.start_time > ?", Time.now.utc]) #events with a start time in the past are not relevant
  end
end

class ClientRescheduleEmailJob < Struct.new(:event_id, :log_id)
  def perform
    event = Event.find(event_id)
    event.email_client_event_reschedule!
    event.email_logs.reschedules.for_clients.find(log_id).update_attributes(completed_at: Time.now.utc)
  end
end

class UserRescheduleEmailJob < Struct.new(:event_id, :log_id)
  def perform
    event = Event.find(event_id)
    event.email_subscribers_event_reschedule!
    event.email_logs.reschedules.for_users.find(log_id).update_attributes(completed_at: Time.now.utc)
  end
end
