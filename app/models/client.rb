require 'textacular/searchable'

class Client < ActiveRecord::Base
  extend Searchable(:name, :phone_number, :mobile_number, :email)

  include FullNameSplitter
  include Concerns::PhoneNumberFormatting

  has_many :events, dependent: :destroy
  has_many :upcoming_events, -> { where('events.start_time >= ?', Time.zone.now) }, class_name: 'Event'
  has_many :past_events, -> { where('events.start_time < ?', Time.zone.now) }, class_name: 'Event'
  has_many :schedules, through: :events
  belongs_to :account
  belongs_to :creator, class_name: "User", foreign_key: "creator_id"

  scope :with_outstanding_sms_notification, -> {
    joins(:events).
    joins("INNER JOIN event_logs ON events.id = event_logs.event_id").
    where("events.state = 'pending' AND events.start_time > '#{Time.now.utc.to_s(:db)}'").
    where("event_logs.type = 'SmsLog' AND event_logs.sent_at IS NOT NULL").distinct
  }

  scope :with_confirmed_sms_notifications, -> {
    joins(:events).
    joins("INNER JOIN event_logs ON events.id = event_logs.event_id").
    where("events.state = 'confirmed' AND events.start_time > '#{Time.now.utc.to_s(:db)}'").
    where("event_logs.type = 'SmsLog' AND event_logs.sent_at IS NOT NULL").distinct
  }

  scope :with_unconfirmed_past_sms_notifications, -> {
    joins(:events).
    joins("INNER JOIN event_logs ON events.id = event_logs.event_id").
    where("events.state = 'pending' AND events.start_time =< '#{Time.now.utc.to_s(:db)}'").
    where("event_logs.type = 'SmsLog' AND event_logs.sent_at IS NOT NULL")
  }

  phony_normalize :mobile_number, :default_country_code => 'US'
  phony_normalize :phone_number, :default_country_code => 'US'

  validates :mobile_number, phony_plausible: true, presence: false
  validates :phone_number, phony_plausible: true, presence: false
  validates :name, :creator, :account, presence: true
  validates :name, uniqueness: { case_sensitive: false, scope: :account_id}

  before_validation :set_account, on: :create
  before_create :generate_token

  def set_account
    return if account_id.present?
    self.account = self.creator.account if (self.creator && self.creator.account)
  end

  def first_name
    FullNameSplitter.split(name)[0]
  end

  def last_name
    FullNameSplitter.split(name)[1]
  end

  def accepts_emails?
    receive_email
  end

  def missing_contact_info?
    mobile_number.blank? && email.blank?
  end

 protected

  def generate_token
    self.token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless Client.exists?(token: random_token)
    end
  end

end
