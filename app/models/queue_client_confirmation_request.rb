class QueueClientConfirmationRequest

  QUERY_VARIABLES = {
    EMAIL:  {OFFSET: 0, LOG_TYPE: EventLog::LOG_TYPE[:EMAIL], REMINDER_PREFERENCE: Schedule::REMINDER_PREFERENCES[:EMAIL]},
    SMS:    {OFFSET: 5.minutes, LOG_TYPE: EventLog::LOG_TYPE[:SMS], REMINDER_PREFERENCE: Schedule::REMINDER_PREFERENCES[:SMS]},
    VOICE:  {OFFSET: 10.minutes, LOG_TYPE: EventLog::LOG_TYPE[:VOICE], REMINDER_PREFERENCE: Schedule::REMINDER_PREFERENCES[:VOICE]}
  }

  def self.run!
    Event.transaction do
      events_for_confirmation_request.each {|event| notify(event)}
    end
  end

  def self.start_time
    Time.now.utc.change(usec: 0) - 1.hour
  end

  def self.end_time(offset)
    Time.now.utc.change(usec: 0) - offset
  end

  def self.events_for_confirmation_request
    events = Array.new
    QUERY_VARIABLES.each do |key, var|
      events << query(var[:OFFSET], var[:LOG_TYPE], var[:REMINDER_PREFERENCE])
    end
    events = events.flatten
    log_events_found(events) if events.any?
    events
  end

  def self.log_events_found(events)
    events_info = events.collect {|e| "event_id: #{e.id} reminder_preference: #{Schedule::REMINDER_CODE_MAP[e.reminder_preference]}"}.join(', ')
    puts "Events found at #{Time.now.utc}: #{events_info}"
  end

  def self.notify(event)
    for option in Schedule::REMINDER_PREFERENCES.values
      if event.reminder_preference == option
        send("notify_via_#{Schedule::REMINDER_PREFERENCES.invert[option].downcase.to_s}", event)
      end
    end
  end

  def self.notify_via_email(event)
    count = event.email_logs.count
    log = event.email_logs.create!(trigger_time_id: count, source: EventLog::SOURCE[:REQUEST_CONFIRMATION], user_type: EventLog::USER_TYPE[:CLIENT])
    Delayed::Job.enqueue ClientConfirmationEmailJob.new(event.id, log.id), queue: "confirmations"
  end

  def self.notify_via_sms(event)
    count = event.sms_logs.count
    log = event.sms_logs.create!(trigger_time_id: count, source: EventLog::SOURCE[:REQUEST_CONFIRMATION], user_type: EventLog::USER_TYPE[:CLIENT])
    Delayed::Job.enqueue ClientConfirmationSmsJob.new(event.id, log.id), queue: "confirmations"
  end

  def self.notify_via_voice(event)
    count = event.voice_logs.count
    log = event.voice_logs.create!(trigger_time_id: count, source: EventLog::SOURCE[:REQUEST_CONFIRMATION], user_type: EventLog::USER_TYPE[:CLIENT])
    Delayed::Job.enqueue ClientConfirmationVoiceCallJob.new(event.id, log.id), queue: "confirmations"
  end

  # :::DEFINITIONS:::
  # EVENT: See event model. An event is the same as an appointment.
  # CLIENT: A person who receives event notifications.
  # EVENT NOTIFICATION: The act of notifying. there are 3 notification mechanisms: email, SMS and phone calls.
  # TRIGGER TIME: the date and time at which an event's notification should take place.
  # There are (as of now) two trigger times for each event. One, on the day before the event takes place and one on the day of the event.
  # The hour at which the notification is triggered is configured on the schedule_timing_option table.
  # EVENT LOG TABLE: a db table that stores records of each event notification.
  # It has a "completed_at" column that is timestamped when the communication event is completed.
  #
  # :::NOTIFICATION EXECUTION CRITERIA:::
  # An event notification is executed when its trigger time has passed, its state is 'pending', there is no record of it on the notifications log table.
  # its reminider preference matches and its start_time is in the future.
  # First step is to create a record of the notification on the notifications_log table. Then spin a delayed_work worker to execute the notification.
  # At the end of the execution, the completed_at column on the notifications_log is timestamped at which time the notification is considered complete.

  def self.query(offset, log_type, reminder_preference)
    Event
      .unscoped
      .select("e.*, '#{reminder_preference}'::integer AS reminder_preference, t.trigger_time_id")
      .from("events e")
      .joins("CROSS JOIN LATERAL jsonb_to_recordset(e.trigger_times) as t(trigger_time_id integer, days_before integer, offset_in_seconds integer)")
      .joins("LEFT OUTER JOIN event_logs AS el ON el.event_id = e.id AND el.trigger_time_id = t.trigger_time_id AND el.type = '#{log_type}'")
      .where("e.deleted_at IS NULL")
      .where(["offset_in_seconds = 0 OR e.start_time - cast(days_before || ' days' AS interval) - cast(offset_in_seconds || ' seconds' AS interval) BETWEEN ? AND ?", start_time, end_time(offset)])
      .where(["e.start_time > ?", Time.now.utc.change(usec: 0)]) #events with a start time in the past are not relevant
      .where("'#{reminder_preference}' = ANY(e.reminder_preferences)")
      .where("e.state in ('pending', 'confirmed') AND el.event_id IS NULL")
  end
end


class ClientConfirmationEmailJob < Struct.new(:event_id, :log_id)
  def perform
    event = Event.find(event_id)
    event.email_client_event_reminder!
    event.email_logs.find(log_id).update_attributes(completed_at: Time.now.utc)
  end
end

class ClientConfirmationSmsJob < Struct.new(:event_id, :log_id)
  def perform
    event = Event.find(event_id)
    response = event.sms_client_event_confirmation_request!(log_id)
    attributes = (response || {}).merge(completed_at: Time.now.utc)
    event.sms_logs.find(log_id).update_attributes(attributes)
  end
end

class ClientConfirmationVoiceCallJob < Struct.new(:event_id, :log_id)
  def perform
    event = Event.find(event_id)
    response = event.request_voice_call_confirmation_from_client!
    attributes = (response || {}).merge(completed_at: Time.now.utc)
    event.voice_logs.find(log_id).update_attributes(attributes)
  end
end
