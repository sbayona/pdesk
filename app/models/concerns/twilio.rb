require 'active_support/concern'

module Concerns
  module Twilio
    extend ActiveSupport::Concern
    include Rails.application.routes.url_helpers

    def twilio_client
      @twilio_client ||= ::Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
    end

    def send_message(options)
      begin
        response = twilio_client.api.account.messages.create sms_params(options)
        { data: {to: response.to, from: response.from, body: response.body, sid: response.sid } }
      rescue StandardError => e
        { error: e.message }
      end
    end

    def make_call(options)
      begin
        response = twilio_client.api.account.calls.create call_params(options)
        { data: { to: response.to, from: response.from, sid: response.sid } }
      rescue StandardError => e
        { error: e.message }
      end
    end

    def call_params(options)
      log_id = options.delete(:log_id) if options[:log_id]
      {from: ENV['TWILIO_NUMBER']}.merge(options)
    end

    def sms_params(options)
      log_id = options.delete(:log_id) if options[:log_id]
      {from: ENV['TWILIO_NUMBER'], status_callback: sms_callback_url(host: ENV['HOST'], log_id: log_id, protocol: "https")}.merge(options)
    end

  end
end
