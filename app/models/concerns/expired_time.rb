require 'active_support/concern'

module Concerns
  module ExpiredTime
    extend ActiveSupport::Concern

    included do
      attr_accessor :expired_time

      after_initialize :check_expired_time
    end

    def check_expired_time
      return unless attributes["start_time"].is_a?(ActiveSupport::TimeWithZone)
      self.expired_time = true if start_time < Time.zone.now
    end
  end
end
