require 'active_support/concern'

module Concerns
  module ScheduleTimings

    def schedule_timings
      @schedule_timings ||= schedule.schedule_timings
    end
  end
end
