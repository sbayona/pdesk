require 'active_support/concern'

module Concerns
  module UntilDate
    extend ActiveSupport::Concern

    included do
      after_initialize :set_until_date
    end

    def set_until_date
      return unless attributes["start_time"].is_a?(ActiveSupport::TimeWithZone)
      return if until_date.present? || start_time.blank?
      self.until_date = start_time + 1.day
    end
  end
end
