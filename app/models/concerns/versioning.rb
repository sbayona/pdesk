require 'active_support/concern'

module Concerns
  module Versioning
    extend ActiveSupport::Concern

    included do
      has_paper_trail class_name: 'EventVersion', meta: { action: :define_action, via: :define_via }
    end

    # returns the user who updated the event last time.
    def updater
      return unless paper_trail_originator
      @updater ||= begin
        if paper_trail_originator == 'client'
          client
        else
          user_originator
        end
      end
    end

    def define_action
      return 'reschedule' if rescheduled_at_changed? #special action that's not part of the state machine
      state if state_changed?
    end

    def define_via
      updated_via if state_changed?
    end

    # Creates custom event versions 
    # Options:
    # event: create, update, destroy
    # action: reminder, cancelation_req, cancelation_msg, confirmation_req, confirmation_msg
    #         callback_req, callback_msg
    # via: email, sms, voice
    # target: client, subscribers
    # info: test@info.com or 404-969-5121

    def create_event_notification_log(action, via, target, info)
      target = target.is_a?(Array) ? target.join(", ") : target
      versions.create(event: 'update', whodunnit: 'system', action: action, via: via, target: target, info: info)
    end

    def user_originator
      User.where(id: paper_trail_originator).first
    end

    # returns an array of old and new values for the start time
    def start_time_changeset
      @start_time_changeset ||= last_changeset["start_time"]
    end

    # returns an hash of attributes that changed along with the old and new values
    def last_changeset
      @last_changeset ||= (versions.any? && versions.last.changeset) || {}
    end

    def old_start_time
      @old_start_time ||= (start_time_changeset && start_time_changeset[0].in_time_zone) || nil
    end

    def new_start_time
      @new_start_time ||= (start_time_changeset && start_time_changeset[1].in_time_zone) || nil
    end

    def rescheduled?
      versions.any? && versions.last.action == 'reschedule'
    end

    def on_the_same_day?
      if length_changed?
        start_time.day == current_event_start_time.day
      else
        old_start_time.day == new_start_time.day
      end
    end

    #  For use on events with increased time
    def length_changed?
      last_rescheduled.decorate.duration_changed?
    end

    def last_rescheduled
      versions.where(action: "reschedule").last
    end

    def current_event_time_period
      start_time = current_event_start_time
      end_time = last_rescheduled.changeset["end_time"].last.in_time_zone

      "#{start_time.strftime('%l:%M%P')} - <strong>#{end_time.strftime('%l:%M%P')}</strong>"
    end

    def time_period
      start_time = current_event_start_time
      end_time = current_event_end_time

      "#{start_time.strftime('%l:%M%P')} - #{end_time.strftime('%l:%M%P')}"
    end

    def current_event_date
      start_date = current_event_start_time
      start_date.strftime('%a, %b %-d')
    end

    def current_event_start_time
      last_rescheduled.object['start_time'].in_time_zone
    end

    def current_event_end_time
      last_rescheduled.object['end_time'].in_time_zone
    end
    ###

    def updated_by?(type)
      raise ArgumentError, "#{type} is not allowed" unless %w(User Client).include?(type)
      self.updater.class.name == type
    end

    def updated_via?(method)
      normalized_method = method.to_s.downcase
      raise ArgumentError, "#{method} does not exist" unless Event::UPDATE_VIA.has_value?(normalized_method)
      updated_via == normalized_method
    end

  end
end
