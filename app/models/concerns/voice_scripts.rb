require 'active_support/concern'

module Concerns
  module VoiceScripts

    def voice_script
      script_group.voice_script
    end

    def voice_confirmation_request_message
      render_voice_template(voice_script.message)
    end

    def voice_confirmation_message
      render_voice_template(voice_script.confirmation_message)
    end

    def voice_cancellation_message
      render_voice_template(voice_script.cancellation_message)
    end

    def voice_callback_response_message
      render_voice_template(voice_script.requested_contact_message)
    end

    def render_voice_template(message)
      template = Liquid::Template.parse(message)
      template.render(
        account_event_name => AppointmentDrop.new(self),
        account_client_name => ClientDrop.new(client)
        )
    end

  end
end
