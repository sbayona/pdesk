require 'active_support/concern'

module Concerns
  module EndTimeResolution
    extend ActiveSupport::Concern

    included do
      before_validation :resolve_end_time, on: :create
    end

    def resolve_end_time
      return if start_time.blank? or schedule.nil?
      self.end_time = start_time + schedule.length.minutes
    end
  end
end
