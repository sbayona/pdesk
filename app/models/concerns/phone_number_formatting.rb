require 'active_support/concern'

module Concerns
  module PhoneNumberFormatting
    extend ActiveSupport::Concern

    def human_mobile_number
      human_formatted_number mobile_number
    end

    def human_phone_number
      human_formatted_number phone_number
    end

    private

    def human_formatted_number(number)
      return if number.blank?
      digits = number.gsub(/\D/, '').split(//)

      if (digits.length == 11)
        digits = digits.join
        '(%s) %s-%s' % [digits[1,3], digits[4,3], digits[7,4]]
      end
    end
  end
end
