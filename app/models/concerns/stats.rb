require 'active_support/concern'

module Concerns
  module Stats
    extend ActiveSupport::Concern

    included do
      scope :month_of, -> (month) do
        parsed_month = Chronic.parse(month)
        where("created_at BETWEEN '#{parsed_month.beginning_of_month.to_s(:db)}' AND '#{parsed_month.end_of_month.to_s(:db)}'")
      end

      scope :last_month, -> { where("created_at BETWEEN '#{Time.now.last_month.beginning_of_month.to_s(:db)}' AND '#{Time.now.last_month.end_of_month.to_s(:db)}'") }
      scope :this_month, -> { where("created_at BETWEEN '#{Time.now.beginning_of_month.to_s(:db)}' AND '#{Time.now.end_of_month.to_s(:db)}'")}
    end
  end
end
