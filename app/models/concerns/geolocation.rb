require 'active_support/concern'

module Concerns
  module Geolocation
    extend ActiveSupport::Concern

    def directions_url
      "https://maps.google.com/maps?f=d&hl=en&saddr=current+location&daddr="
    end

    def event_address
      account.address
    end

  end
end
