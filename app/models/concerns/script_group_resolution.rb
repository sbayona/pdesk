require 'active_support/concern'

module Concerns
  module ScriptGroupResolution
    extend ActiveSupport::Concern

    included do
      before_validation :resolve_script_group, on: :create
    end

    def resolve_script_group
      return if script_group || script_group_id
      self.script_group_id = default_script_group_id
    end

    def default_script_group_id
      schedule && schedule.default_script_group && schedule.default_script_group.id
    end
  end
end
