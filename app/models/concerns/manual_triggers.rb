require 'active_support/concern'

module Concerns
  module ManualTriggers
    extend ActiveSupport::Concern

    def manually_trigger(options={})
      originator = options.fetch(:originator, 'client')
      
      PaperTrail.whodunnit = 'client' if originator == 'client'

      via = options.fetch(:via, Event::UPDATE_VIA[:SMS])
      self.updated_via = via
      
      action = options.fetch(:action, 'confirm')
      
      self.send(:"#{action}!")
    end

  end
end
