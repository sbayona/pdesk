require 'active_support/concern'

module Concerns
  module SmsScripts

    def sms_script
      script_group.sms_script
    end

    def sms_recipient_number
      return unless client && client.mobile_number.present?
      client.mobile_number
    end

    def sms_sender_number
      raise ArgumentError, 'Twilio number is blank' if ENV['TWILIO_NUMBER'].blank?
      ENV['TWILIO_NUMBER']
    end

    def sms_confirmation_request_message
      render_sms_template(sms_script.message)
    end

    def sms_confirmation_message
      render_sms_template(sms_script.confirmation_message)
    end

    def sms_cancellation_message
      render_sms_template(sms_script.cancellation_message)
    end

    def sms_callback_response_message
      render_sms_template(sms_script.requested_contact_message)
    end

    def render_sms_template(message)
      template = Liquid::Template.parse(message)
      template.render(
        account_event_name => AppointmentDrop.new(self),
        account_client_name => ClientDrop.new(client)
      )
    end

  end
end
