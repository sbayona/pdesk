require 'active_support/concern'

module Concerns
  module ScheduleTimingResolution
    extend ActiveSupport::Concern

    included do
      before_validation :resolve_schedule_timing, on: :create
    end

    # schedule_timing_id is used for recalculating the reminder times when the event is start time is update
    def resolve_schedule_timing
      return if self.schedule_timing
      self.schedule_timing_id = default_timing_id
    end

    def default_timing_id
      schedule && schedule.default_timing && schedule.default_timing.id
    end
  end
end
