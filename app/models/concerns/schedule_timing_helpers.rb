require 'active_support/concern'

module Concerns
  module ScheduleTimingHelpers
    extend ActiveSupport::Concern

    def to_sentence
      sentence = []
      options.each do |option|
        sentence << "#{days_before(option.days_before)}, #{hours_before(option.value)} "
      end
      sentence.to_sentence
    end

    def days_before(number_of_days)
      ScheduleTiming::DAYS_BEFORE.select {|a| a["KEY"] == number_of_days}[0]["LABEL"]
    end

    def hours_before(key)
      ScheduleTiming::OPTIONS.select {|a| a["KEY"] == key}[0]["LABEL"]
    end
  end
end
