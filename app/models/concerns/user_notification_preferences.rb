module Concerns
  module UserNotificationPreferences

    def elegible_for_adding
      proxy_association.owner.account.users.to_a - self.to_a
    end

  end
end
