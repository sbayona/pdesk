require 'active_support/concern'

module Concerns
  module EventStateMachine
    extend ActiveSupport::Concern

    included do
      before_save :set_state

      Event::STATES.each do |state|
        scope(state, -> { where(state: state) })
      end
    end

    def confirm!
      confirmation.trigger(:confirm)
      save!
      email_subscribers_event_confirmation!
      respond_to_sms_confirmation_request! if updated_by?("Client") && updated_via?("sms")
    end

    def cancel!
      confirmation.trigger(:cancel)
      save!
      # deliver cancellation notification
      email_client_event_cancellation! if updated_by?("User")
      respond_to_sms_cancelation_request! if updated_by?("Client") && updated_via?("sms")
      email_subscribers_event_cancellation!
    end

    def show!
      confirmation.trigger(:show)
      save!
    end

    def no_show!
      confirmation.trigger(:no_show)
      save!
    end

    def callback!
      confirmation.trigger(:callback)
      save!
      email_subscribers_request_to_callback!
      respond_to_sms_callback_request! if updated_by?("Client") && updated_via?("sms")
    end

    def confirmation
      @confirmation ||= begin
        fsm = MicroMachine.new(state || "pending")

        fsm.when(:confirm,  "pending"   => "confirmed")
        fsm.when(:callback, "pending"   => "callback")
        fsm.when(:cancel,   "pending"   => "canceled",  "confirmed" => "canceled")
        fsm.when(:show,     "confirmed" => "show",      "pending"   => "show")
        fsm.when(:no_show,  "pending"   => "no_show",   "confirmed" => "no_show")

        fsm.on("confirmed") do
          write_attribute(:confirmed_at, Time.now.utc)
        end

        fsm.on("canceled") do
          write_attribute(:canceled_at, Time.now.utc)
        end

        fsm.on("show") do
          write_attribute(:showed_at, Time.now.utc)
        end

        fsm.on("no_show") do
          write_attribute(:no_showed_at, Time.now.utc)
        end

        fsm.on("callback") do
          write_attribute(:callback_at, Time.now.utc)
        end

        fsm.on(:any) do
          @state_changed = true
        end

        fsm
      end
    end

    def state_changed?
      @state_changed
    end

    def available_transitions
      @available_transitions ||= confirmation.transitions_for.values.map { |k,v| k[state] }.compact
    end

    private

    def set_state
      self.state = confirmation.state
    end


  end
end
