require 'active_support/concern'

module Concerns
  module Rescheduling
    extend ActiveSupport::Concern

    included do
      before_update :set_reschedule_timestamp
    end

    def set_reschedule_timestamp
      self.rescheduled_at = Time.zone.now if start_time_changed? || end_time_changed?
    end
  end
end
