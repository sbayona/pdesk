require 'active_support/concern'

module Concerns
  module SubscriberNotifications
    extend ActiveSupport::Concern

    included do
      after_create :email_subscribers_event_creation!
    end

    # triggered on the after_create callback on this file
    def email_subscribers_event_creation!
      return if unavailable_time?
      return unless has_notification_subscribers?
      SubscriberNotificationsMailer.delay.notify_created(id)
    end

    # triggered on QueueRescheduleNotification
    def email_subscribers_event_reschedule!
      return unless has_notification_subscribers?
      SubscriberNotificationsMailer.notify_updated(id).deliver_now
    end

    # triggered on the event machine (event_state.rb)
    def email_subscribers_event_cancellation!
      return unless has_notification_subscribers?
      SubscriberNotificationsMailer.delay.notify_canceled(id)
    end

    # triggered on the event machine (event_state.rb)
    def email_subscribers_event_confirmation!
      return unless has_notification_subscribers?
      SubscriberNotificationsMailer.delay.notify_confirmed(id)
    end

    # triggered on the state machine
    def email_subscribers_request_to_callback!
      return unless has_notification_subscribers?
      SubscriberNotificationsMailer.delay.client_callback_request(id)
    end

    def notification_subscribers
      @notification_subscribers ||= (schedule.subscribers if schedule) || []
    end

    def has_notification_subscribers?
      notification_subscribers.any?
    end

  end
end
