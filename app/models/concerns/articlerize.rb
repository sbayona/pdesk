require 'active_support/concern'

module Concerns
  module Articlerize
    extend ActiveSupport::Concern

    included do
      def self.indefinite_articlerize(params_word)
        %w(a e i o u).include?(params_word[0].downcase) ? "an #{params_word}" : "a #{params_word}"
      end
    end
  end
end
