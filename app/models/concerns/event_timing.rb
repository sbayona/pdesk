require 'active_support/concern'

module Concerns
  module EventTiming
    extend ActiveSupport::Concern

    included do
      before_save :set_trigger_times

      delegate :default_timing, to: :schedule, allow_nil: true
    end

    def set_trigger_times
      return if unavailable_time?

      schedule_options = []
      # fallback to validation if schedule or schedule_timing_id aren't present
      return if schedule.blank? || schedule_timing_id.blank?
      # Then we create them based on the selected schedule timing.
      schedule_timing.options.each_with_index do |option, index|
        if option['value'].blank?
          next
        else
          schedule_options << {
            trigger_time_id: index,
            days_before: option['days_before'],
            offset_in_seconds: calculate_offset(option)
          }
        end
      end

      self.trigger_times = schedule_options
    end

    def calculate_offset(option)
      if is_interval?(option['value'])
        option['value'].to_i.minutes
      else
        offset_for_fixed_time(option['value'])
      end
    end

    def offset_for_fixed_time(hour)
      Integer(start_time_in_tz - start_time_in_tz.change(hour: hour.to_i))
    end

    def start_time_in_tz
      @start_time_in_tz ||= self.start_time.in_time_zone(self.time_zone)
    end

    def is_interval?(value)
      ScheduleTiming::INTERVAL_TIME_OPTIONS.map{|a| a["KEY"]}.include?(value.to_s)
    end

    def time_zone
      @time_zone ||= ActiveSupport::TimeZone[account.time_zone]
    end


  end
end
