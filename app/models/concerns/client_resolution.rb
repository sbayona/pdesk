require 'active_support/concern'

module Concerns
  module ClientResolution
    extend ActiveSupport::Concern

    included do
      before_validation :resolve_client, on: :create
    end

    # The client can be assigned in 2 ways. One is by passing the client_attribute[:name] = "Foo" in which case the client name is looked up on the db or created. and by simply doing client = Client.new (standard Rails)
    def resolve_client
      return if unavailable_time? # no need to deal with client resolution if we're adding unavalable time
      return if account.nil? || (client && client.persisted?)

      client_attributes.delete_if{|k,v| k =="id" && v==""} if client_attributes.present? #if we pass an empty id it will try to create a new record. We don't want that.

      if client_for_event = client_by_name
        client_for_event.update_attributes(client_attributes)
      else
        atts = client_attributes.merge(creator: user)
        client_for_event = account.clients.create(atts)  
      end

      self.client = client_for_event
      errors.add(:base, "we need at least an email and/or mobile number") if client.missing_contact_info?
      self.client.touch(:scheduled_at) if self.client.persisted?
    end

    def client_by_name
      @client_by_name ||= account.clients.where(name: client_attributes[:name]).first
    end
  end
end
