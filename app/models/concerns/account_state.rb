require 'active_support/concern'

module Concerns
  module AccountState
    extend ActiveSupport::Concern

    PASIVE_STATES = %w(active canceled disabled)
    ACTIVE_STATES = %w(activate cancel disable)

    included do
      before_save :persist_confirmation

      PASIVE_STATES.each do |action|

        # Scopes for accounts in a specific state
        # e.g.: Account.active
        scope action.to_sym, -> { where(state: action) }

        # Define state constants
        # e.g.: Account::CANCELED
        const_set(action.upcase, action)

      end

    end

    ACTIVE_STATES.each do |action|

      # methods to trigger states
      # e.g.: account.disable!
      define_method "#{action}!" do
        confirmation.trigger(action.to_sym)
        save!
      end

    end

    PASIVE_STATES.each do |action|

      # returns boolean if an account is in a specific state
      # e.g.: account.is_active?
      define_method "is_#{action}?" do
        self.state == action
      end

    end

    def confirmation
      @confirmation ||= begin
        fsm = MicroMachine.new(state || "active")

        fsm.when(:disable, "active" => "disabled")
        fsm.when(:cancel, "active" => "canceled")
        fsm.when(:activate, "disabled" => "active", "canceled" => "active")

        fsm.on("canceled") do
          delete_stripe_subscriptions
        end

        fsm.on("disabled") do
          delete_stripe_subscriptions
        end

        fsm
      end
    end

    private

      def delete_stripe_subscriptions
        if stripe_id && stripe_subscription_id
          customer = Stripe::Customer.retrieve(stripe_id)
          customer.subscriptions.retrieve(stripe_subscription_id).delete
        end
      end

      def persist_confirmation
        self.state = confirmation.state
      end

  end
end
