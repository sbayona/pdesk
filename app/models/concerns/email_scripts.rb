require 'active_support/concern'

module Concerns
  module EmailScripts
    extend ActiveSupport::Concern

    def email_script
      script_group.email_script
    end

    def email_recipient
      return if client.email.blank?
      "#{client.name} <#{client.email}>"
    end

    def email_reply_to
      "#{email_script.reply_to_name} #{email_script.reply_to_email}"
    end

    def email_sender
      "#{email_script.from_name} <#{email_script.from_email}>"
    end

    def email_confirmation_request_subject
      render_email_template(email_script.subject)
    end

    def email_confirmation_request_message
      render_email_template(email_script.message)
    end

    def email_cancellation_notification_subject
      render_email_template(email_script.cancellation_subject)
    end

    def email_cancellation_notification_message
      render_email_template(email_script.cancellation_message)
    end

    def email_reschedule_notification_subject
      render_email_template(email_script.reschedule_subject)
    end

    def email_reschedule_notification_message
      render_email_template(email_script.reschedule_message)
    end

    private

    def render_email_template(text)
      template = Liquid::Template.parse(text)
      template.render(
        account.event_name => AppointmentDrop.new(self),
        account.client_name => ClientDrop.new(client)
        )
    end

  end
end
