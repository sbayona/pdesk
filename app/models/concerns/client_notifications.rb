require 'active_support/concern'

module Concerns
  module ClientNotifications
    extend ActiveSupport::Concern

    included do
      if Rails.env.development?
        after_create :email_client_event_reminder!
      end
    end

    # triggered on QueueRescheduleNotification
    def email_client_event_reschedule!
      return unless client && client.accepts_emails?
      ClientNotificationsMailer.notify_reschedule(id).deliver_now
    end

    # triggered on the event machine (event_state.rb)
    def email_client_event_cancellation!
      return unless client && client.accepts_emails?
      ClientNotificationsMailer.delay.notify_cancellation(id)
    end

    # triggered on QueueClientConfirmationRequest
    def email_client_event_reminder!
      return unless client && client.accepts_emails?
      ClientNotificationsMailer.reminder_message(id).deliver_now
    end

    # triggered on QueueClientConfirmationRequest
    def sms_client_event_confirmation_request!(log_id)
      ClientSmsMessenger.new(self).request_confirmation(log_id)
    end

    # triggered on the event state machine
    def respond_to_sms_cancelation_request!
      ClientSmsMessenger.new(self).respond_to_cancelation_request
    end

    # triggered on the event state machine
    def respond_to_sms_confirmation_request!
      ClientSmsMessenger.new(self).respond_to_confirmation_request
    end

    # triggered on the event state machine
    def respond_to_sms_callback_request!
      ClientSmsMessenger.new(self).respond_to_callback_request
    end

    def respond_to_sms_unknown_reply!
      ClientSmsMessenger.new(self).respond_to_unknown_reply
    end

    # triggered on EnqueuClientConfirmationRequest
    def request_voice_call_confirmation_from_client!
      caller = ClientNotificationCaller.new(self)
      caller.request_confirmation
    end

  end
end
