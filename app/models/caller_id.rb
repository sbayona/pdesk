class CallerId < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  include Concerns::Twilio
  attr_accessor :validation_code

  belongs_to :schedule

  validates :number, phony_plausible: true, presence: true
  validates :schedule, presence: true

  phony_normalize :number, :default_country_code => 'US'

  before_create :twilio_verify!
  before_destroy :twilio_destroy!

  def is_validating?
    validation_code.present? && verified_at.nil?
  end

  def is_verified?
    twilio_sid.present?
  end

  def is_invalid?
    validation_code.nil? && !verified?
  end

  def expired?
    validation_code.nil? && !verified? && created_at < 5.minutes.ago
  end

  def has_verification_answer?
    verified_at.present?
  end

  private

  def twilio_verify!
    return if Rails.env.test?
    begin
      caller_id = twilio_client.account.outgoing_caller_ids.create(verification_params)
      @validation_code = caller_id.validation_code
    rescue Exception => e
      if e.message == "Phone number is already verified."
        verify_locally
        return true
      else
        errors[:base] << "#{e.message}"
        return false
      end
    end 
  end

  def twilio_destroy!
    return if Rails.env.test?
    if twilio_sid.present?
      caller_id = twilio_client.account.outgoing_caller_ids.get(twilio_sid)
      caller_id.delete if caller_id
    end
  end

  def twilio_list(phone_number=nil)
    twilio_client.account.outgoing_caller_ids.list(phone_number: phone_number)
  end

  def verify_locally
    self.twilio_sid = twilio_get_sid_for_number(number)
    self.verified_at = Time.zone.now
    self.verified = true
  end

  def twilio_get_sid_for_number(phone_number=nil)
    twilio_list(phone_number).first.try(:sid)
  end

  def verification_params
    {friendly_name: friendly_name, phone_number: number, status_callback: callback_url}
  end

  def callback_url
    caller_id_confirmations_url(schedule_id: schedule.id, host: ENV['HOST'], protocol: 'https')
  end

end
