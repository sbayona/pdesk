class LiquidTemplateUpdater
  def initialize(account)
    @account = account
  end

  def update!
    @account.scripts.each do |script|
      update_by_script_type(script)
    end
  end

  def update_by_script_type(script)
    case script.class.name
    when "Script::Email"
      update_email(script)
    else
      raise ArgumentError
    end
  end

  def update_email(script)
    new_attributes = convert_attributes(script)
    script.update_attributes(new_attributes)
  end

  def new_message(script)
    message_template = Liquid::Template.parse(script.message)
    message_template.render(
      previous_event_name_value  => AppointmentTemplateDrop.new(@account),
      previous_client_name_value       => ClientTemplateDrop.new(@account)
      )
  end

  def new_subject(script)
    subject_template = Liquid::Template.parse(script.subject)
    subject_template.render(
      previous_event_name_value  => AppointmentTemplateDrop.new(@account)
      )
  end

  def previous_event_name_value
    @account.last_version && @account.last_version.event_name
  end

  def previous_client_name_value
    @account.last_version && @account.last_version.client_name
  end

  def convert_attributes(record)
    string_attributes = record.attributes.select {|k,v| v.is_a?(String)}
    string_attributes.each do |key, value|
      template = Liquid::Template.parse(value)
      string_attributes[key] = template.render(
        previous_event_name_value => AppointmentTemplateDrop.new(@account),
        previous_client_name_value      => ClientTemplateDrop.new(@account)
        )
    end
  end
end

