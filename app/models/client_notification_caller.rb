class ClientNotificationCaller
  include Concerns::Twilio

  def initialize(event)
    @event = event
    Time.zone = @event.time_zone
    @event.reload # we need to reload the record so its timestamps pick up the time zone
  end

  def request_confirmation
    result = make_call from: caller_id, to: @event.sms_recipient_number, url: voice_reminder_xml_url
    create_event_version('reminder_msg')
    result
  end

  private 
  def create_event_version(action)
    @event.create_event_notification_log(action, 'voice', 'client', @event.sms_recipient_number)
  end

  # this url renders the xml with the voice instructions for the event. VoiceController#reminder
  def voice_reminder_xml_url
    voice_reminder_url(host: ENV['HOST'], uid: @event.uuid, format: 'xml', protocol: "https")
  end

  def caller_id
    @event.caller_id_number || ENV['TWILIO_NUMBER']
  end

end
