class SubscriptionPlan < ActiveRecord::Base
  include ActiveModel::Dirty

  scope :active, -> { where(active: true).order(:price_in_cents) }

  monetize :price_in_cents, as: "price"

  has_many :accounts

  validates :name, :code, :price_in_cents, :currency, :max_events_per_month, :max_users, :country_code, presence: true

  validates_uniqueness_of :code, :name

  def stripe_code
    "#{code}-#{country_code}"
  end

end
