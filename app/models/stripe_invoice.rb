class StripeInvoice
  def initialize(invoice=nil)
    @invoice = invoice
  end

  def date
    return if @invoice.nil?
    Time.at(@invoice.date).to_date
  end
end
