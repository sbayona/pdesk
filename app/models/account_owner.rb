class AccountOwner < User
  belongs_to :account

  delegate :subscription_plan, to: :account

  def subscription_plan_name
    subscription_plan.try(:name)
  end

  def trial_period_ends_on
    account.trial_period_ends_on
  end
end
