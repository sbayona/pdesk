class StripeCard
  def initialize(card)
    @card = card
  end

  def present?
    !!@card
  end

  def last4
    @card.try(:last4)
  end

  def brand
    @card.try(:brand)
  end

  def expiration_month
    @card.try(:exp_month)
  end

  def expiration_year
    @card.try(:exp_year)
  end

  def expiration
    return if expiration_year.nil? || expiration_month.nil?
    "#{expiration_month} / #{expiration_year}"
  end
end
