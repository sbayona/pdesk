class Charge < ActiveRecord::Base
  belongs_to :account

  acts_as_sequenced scope: :account_id

  validates_presence_of :account
  validates_uniqueness_of :stripe_event_id

  scope :sorted, -> { order('charges.charged_at DESC') }

  STATUSES = %w(succeeded failed)

  STATUSES.each do |s|

    # Checks if charge instance is in a specific status
    # E.g.: charge.failed?
    define_method "#{s}?" do
      self.status == s
    end

    # Returns value for each status defined
    # E.g.: Charge::SUCCEEDED
    const_set(s.upcase, s)

  end

  def self.new_from_stripe_event(event)
    type = get_type_event(event)
    if STATUSES.include? type
      hash = event.data.object
      Charge.new(
        amount: amount_with_decimals(hash[:amount]),
        currency: hash[:currency],
        customer: hash[:customer],
        description: hash[:description],
        statement: hash[:statement_descriptor],
        card_last4: hash[:source][:last4],
        card_brand: hash[:source][:brand],
        card_exp_month: hash[:source][:exp_month],
        card_exp_year: hash[:source][:exp_year],
        card_country: hash[:source][:country],
        charged_at: Time.at(hash[:created]),
        stripe_event_id: hash[:id],
        status: type,
        account: Account.find_by_stripe_id!(hash[:customer])
      )
    else
      Charge.new
    end
  end

  def self.get_type_event(event)
    event.type.remove('charge.')
  end

  def send_notification_to_account!
    case status
    when SUCCEEDED
      AccountPaymentMailer.charge_succeeded(self).deliver_now
    when FAILED
      AccountPaymentMailer.charge_failed(self).deliver_now
    end
  end

  private

    def self.amount_with_decimals(amount)
      amount.to_s.insert(-3,'.').to_f
    end

end
