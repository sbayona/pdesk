class Event < ActiveRecord::Base
  attr_accessor :client_attributes, :resize, :until_date, :period, :frequency
  attr_reader :day_delta, :minute_delta

  CONFIRMATION_CODE = {CONFIRMED: '1', CANCELED: '5', CALLBACK: '9'}
  UPDATE_VIA = {WEB: 'web', EMAIL: 'email', SMS: 'sms', VOICE: 'voice'}

  STATES = %w(pending confirmed canceled show no_show callback).freeze

  has_soft_deletion :default_scope => true

  include Concerns::SubscriberNotifications
  include Concerns::ClientNotifications
  include Concerns::EmailScripts
  include Concerns::SmsScripts
  include Concerns::VoiceScripts
  include Concerns::EventTiming
  include Concerns::Versioning
  include Concerns::EventStateMachine
  include Concerns::Rescheduling
  include Concerns::Geolocation
  include Concerns::ClientResolution
  include Concerns::ScheduleTimingResolution
  include Concerns::EndTimeResolution
  include Concerns::ExpiredTime
  include Concerns::UntilDate
  include Concerns::ScheduleTimings
  include Concerns::ScriptGroupResolution
  include Concerns::Twilio
  include Concerns::Stats
  include Concerns::ManualTriggers

  belongs_to :schedule
  belongs_to :client
  belongs_to :user
  belongs_to :account
  belongs_to :script_group
  belongs_to :schedule_timing
  has_many :event_logs, dependent: :delete_all
  has_many :email_logs
  has_many :sms_logs
  has_many :voice_logs
  has_many :notes, dependent: :delete_all

  scope :with_sms_confirmation_request, -> { where("events.id IN (SELECT el.event_id FROM event_logs AS el WHERE el.type IN ('SmsLog') AND el.sent_at IS NOT NULL)") }
  scope :in_the_future, -> { where("start_time > '#{Time.now.utc.to_s(:db)}'") }
  scope :in_the_past, -> { where("start_time < '#{Time.now.utc.to_s(:db)}'") }
  scope :today, -> { where("date_trunc('day', start_time) = date_trunc('day', TIMESTAMP 'today')") }
  scope :tomorrow, ->  { where("date_trunc('day', start_time) = date_trunc('day', TIMESTAMP 'tomorrow')") }
  scope :this_week, -> { where("date_trunc('week', start_time) = date_trunc('week', TIMESTAMP 'today')") }
  scope :recently_updated, -> { order("updated_at DESC") }
  scope :recently_created, -> { order('created_at DESC') }

  validates :schedule, :account, :user, :start_time, presence: true
  validates :client, presence: true, unless: :unavailable_time?

  delegate :name, :email, :mobile_number, :human_mobile_number, :human_phone_number, to: :client, prefix: true, allow_nil: true
  delegate :caller_id_number, :notes_enabled?, to: :schedule, prefix: false
  delegate :event_name, :client_name, to: :account, prefix: true
  delegate :name, to: :account, prefix: true
  delegate :name, to: :user, prefix: true

  validate :ensure_schedule_timing_is_present

  before_create :generate_short_code

  def ensure_schedule_timing_is_present
    errors.add(:base, :missing_schedule_timing) if schedule_timing.blank?
  end

  def pending?
    state == 'pending'
  end

  def confirmed?
    state == 'confirmed'
  end

  def canceled?
    state == 'canceled'
  end

  def status
    EventStatus.new(self)
  end

  def day_delta=(day_delta)
    self.start_time = (day_delta.to_i).days.from_now(self.start_time) unless resize
    self.end_time   = (day_delta.to_i).days.from_now(self.end_time)
    @day_delta      = day_delta
  end

  def minute_delta=(minute_delta)
    self.start_time = (minute_delta.to_i).minutes.from_now(self.start_time) unless resize
    self.end_time   = (minute_delta.to_i).minutes.from_now(self.end_time)
    @minute_delta   = minute_delta
  end

  def creator
    user
  end

  def notify_via?(type)
    type = type.upcase.to_sym
    raise ArgumentError, I18n.t('models.event.errors.notify_msg', type: type) unless Event.can_do?(type)
    reminder_preferences.include?(Schedule::REMINDER_PREFERENCES[type].to_s)
  end

  def self.can_do?(type)
    Schedule::REMINDER_PREFERENCES.keys.include?(type)
  end

  def self.active_with_outstanding_sms_confirmation_request
    in_the_future.pending.with_sms_confirmation_request
  end

  def title
    title = read_attribute(:title)

    if unavailable_time?
      title.blank? ? "unavailable" : title
    else
      client && client.name
    end
  end

  def scheduling_phone_number
    account.phone
  end

  def has_no_contact_info
    client_mobile_number.blank? &&
    client_email.blank?
  end

  def client_logs
    versions.where(event: %w(create update client_notification))
  end

  def subscriber_logs
    versions.where(event: %w(create update subscriber_notification))
  end

  def unknown_reply
    respond_to_sms_unknown_reply!
  end

 protected

  def generate_short_code
    self.short_code = loop do
      code = SecureRandom.urlsafe_base64(nil, false)[0,4]
      break code unless Event.exists?(short_code: code)
    end
  end


end
