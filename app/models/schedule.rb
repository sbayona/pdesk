class Schedule < ActiveRecord::Base
  belongs_to :account
  has_one :caller_id, dependent: :destroy
  has_many :events, dependent: :destroy
  has_many :clients, through: :events
  has_many :schedule_subscriptions, dependent: :destroy
  has_many :schedule_timings, -> { order "created_at" }, dependent: :destroy
  has_many :script_groups, dependent: :destroy
  has_many :email_scripts, through: :script_groups
  has_many :sms_scripts, through: :script_groups
  has_many :voice_scripts, through: :script_groups
  has_many :subscribers, source: :user, through: :schedule_subscriptions, extend: Concerns::UserNotificationPreferences

  VALID_LENGTH = (5..150).to_a
  VALID_FREQUENCY = [5, 10, 15, 20, 30, 40, 50, 60, 70, 80]
  VALID_HOURS = (0..23).to_a

  DEFAULT_VALUES = { name: I18n.t('models.schedule.default_value') }

  REMINDER_PREFERENCES = {EMAIL: 1, SMS: 2, VOICE: 3}
  REMINDER_CODE_MAP = Hash[REMINDER_PREFERENCES.to_a.map(&:reverse)]

  validates_presence_of :name
  validates_presence_of :starts_at, :ends_at, :frequency, :length
  validates :starts_at, :ends_at, inclusion: { in: VALID_HOURS }, allow_nil: true
  validates :frequency, inclusion: { in: VALID_FREQUENCY }, allow_nil: true
  validates :length, inclusion: { in: VALID_LENGTH }, allow_nil: true
  validates :length, :frequency, :length, :ends_at, :starts_at, numericality: true, allow_nil: true

  before_create :build_default_associations!


  def default_timing
    schedule_timings.detect(&:default)
  end

  delegate :number, to: :caller_id, prefix: true, allow_nil: true

  # script group needs a default attribute
  def default_script_group
    script_groups.first
  end

  def build_default_associations!
    script_group = self.script_groups.build(ScriptGroup.default_values)
    schedule_timing = self.schedule_timings.build(ScheduleTiming.default_values)
    script_group.build_email_script(Script::Email.default_values(account))
    script_group.build_sms_script(Script::Sms.default_values(account))
    script_group.build_voice_script(Script::Voice.default_values(account))
  end

end
