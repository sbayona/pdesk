class Metric

  # defaults to last 30 days
  def initialize(dates={}, interval='month')
    @dates = dates
    @interval = interval
  end

  def created_users
    @created_users ||= User.where(created_at: start_date..end_date).group("DATE_TRUNC('#{@interval}', created_at)").count(:id)
  end

  def active_users
    @active_users ||= User.where(last_sign_in_at: start_date..end_date).group("DATE_TRUNC('#{@interval}', created_at)").count
  end

  def total_users
    @total_users ||= User.count
  end

  def invited_users
    @invited_users ||= User.where(invitation_sent_at: start_date..end_date).group("DATE_TRUNC('#{@interval}', created_at)").count
  end

  def total_users_locked_out
    @total_users_locked_out ||= User.where("locked_at IS NOT NULL").count
  end

  def failed_jobs
    @failed_jobs ||= Delayed::Job.where("failed_at IS NOT NULL").count
  end

  def users_accepted_invitation
    @users_accepted_invitation ||= User.where(invitation_accepted_at: start_date..end_date).group("DATE_TRUNC('#{@interval}', created_at)").count
  end

  def total_accounts
    @total_accounts ||= Account.count
  end

  def accounts_created
    @accounts_created ||= Account.find_by_sql(<<-SQL)
      SELECT *
      FROM (SELECT generate_series('#{start_date}'::date, '#{end_date}'::date,'1 month'::interval)::date) AS d(created_date)
      LEFT JOIN (
      SELECT date_trunc('month', created_at)::date AS created_date, count(*) AS event_count
      FROM accounts
      WHERE created_at >= '#{start_date}'::date
      AND created_at <= '#{end_date}'::date
      GROUP BY 1
      ) t USING (created_date)
      ORDER BY 1;
      SQL
  end

  def events_created
    @events_created ||= Event.find_by_sql(<<-SQL)
      SELECT *
      FROM (SELECT generate_series('#{start_date}'::date, '#{end_date}'::date,'1 month'::interval)::date) AS d(created_date)
      LEFT JOIN (
      SELECT date_trunc('month', created_at)::date AS created_date, count(*) AS event_count
      FROM events
      WHERE created_at >= '#{start_date}'::date
      AND created_at <= '#{end_date}'::date
      GROUP BY 1
      ) t USING (created_date)
      ORDER BY 1;
      SQL
  end

  def total_events
    @total_events ||= Event.count
  end

  private

  def start_date
    @start_date ||= @dates.fetch(:start_date, 30.days.ago).to_date.to_s(:db)
  end

  def end_date
    @end_date ||= @dates.fetch(:end_date, Time.zone.now).to_date.to_s(:db)
  end
end
