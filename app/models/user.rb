class User < ActiveRecord::Base
  include FullNameSplitter
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :, :timeoutable and :omniauthable

  devise :validatable, :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :lockable

  ROLES = {ADMIN: 'admin', REGULAR: 'regular'}

  has_soft_deletion :default_scope => true
  # delete all subscriptions after soft deleting the user
  after_soft_delete {|u| u.schedule_subscriptions.destroy_all }

  scope :invited, -> { unscoped.where.not(invitation_token: nil) }
  scope :sorted, -> { order('users.created_at DESC') }

  belongs_to :account
  has_many :events
  has_many :clients, foreign_key: "creator_id"
  has_many :schedule_subscriptions, dependent: :destroy
  has_many :schedules, through: :schedule_subscriptions
  has_many :demos

  accepts_nested_attributes_for :account

  validates :account, :first_name, :last_name, presence: true

  delegate :state, to: :account, prefix: false
  delegate :name, to: :account, prefix: true

  def name
    "#{first_name} #{last_name}"
  end

  def admin?
    role == 'admin'
  end

  def self.find_for_database_authentication(warden_conditions)
    joins(:account).where(warden_conditions.dup).
    where('accounts.state = ?', Account::ACTIVE).first
  end

end
