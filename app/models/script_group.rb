class ScriptGroup < ActiveRecord::Base
  belongs_to :schedule
  belongs_to :email_script, class_name: "Script::Email"
  belongs_to :sms_script, class_name: "Script::Sms"
  belongs_to :voice_script, class_name: "Script::Voice"


  def self.default_values
    { name: I18n.t('models.script_group.default_values') }
  end
end