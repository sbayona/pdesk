class DecoratedSubmitInput < SimpleForm::Inputs::Base

  def input(wrapper_options)
    cancel_link = input_html_options.delete(:cancel_link)
    "#{@builder.button :submit, input_html_options} &nbsp; or #{cancel_link}".html_safe
  end

  def label(wrapper_options)
    false
  end

  def input_html_classes
    super.delete('form-control')
    super
  end

end
