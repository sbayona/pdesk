class EventReportDecorator < Draper::Decorator
  delegate_all

  def each
    model.each { |event| yield(event.decorate) }
  end

  def filter_select(type)
    filter = filters.fetch(type)

    h.select_tag(
      "q[#{filter[:field]}]",
      h.options_for_select(filter[:options], selected: query[filter[:field]]),
      prompt: I18n.t("decorators.event_report.filter_label.#{type}"),
      class: 'form-control'
    )
  end

  private

  def filters
    {
      creators: {
        options: users.map { |s| [s.name, s.id] },
        field: :user_id
      },
      schedules: {
        options: schedules.map { |s| [s.name, s.id] },
        field: :schedule_id
      },
      clients: {
        options: clients.map { |s| [s.name, s.id] },
        field: :client_id
      },
      states: {
        options: states,
        field: :state
      }
    }
  end
end
