class AccountDecorator < Draper::Decorator
  delegate_all

  def trial_period_end_date
    trial_period_ends_on.strftime("%B %d, %Y")
  end

  def trial_period_end_date_on_days
    h.pluralize(trial_period_ends_on_days,'day')
  end

end
