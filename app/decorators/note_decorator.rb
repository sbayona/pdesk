class NoteDecorator < Draper::Decorator
  delegate_all

  def created_at_ago
    h.time_ago_in_words(created_at)
  end

end
