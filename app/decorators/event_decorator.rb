class EventDecorator < Draper::Decorator
  delegate_all

  def appointment_time
    h.l(start_time, format: :long)
  end

  def schedule_name
    schedule.name
  end

  def client_name
    client.name
  end

  def status
    confirmation.state
  end

  def created_by
    creator.name
  end

  def confirmed_via
    reminder_preferences
      .map(&:to_i)
      .map { |i| Schedule::REMINDER_CODE_MAP[i] }
      .join(', ')
  end
end
