class ChargeDecorator < Draper::Decorator
  delegate_all

  def card_info
    "#{card_brand} **** **** **** #{card_last4}"
  end

  def amount_with_currency
    "#{h.number_to_currency(amount)} #{currency.upcase}"
  end

  def charged_at
    object.charged_at.strftime('%F') if object.charged_at
  end

end
