class SubscriptionPlanDecorator < Draper::Decorator
  delegate_all

  def formatted_price
    h.money_without_cents_and_with_symbol(price)
  end

end
