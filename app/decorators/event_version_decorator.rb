class EventVersionDecorator < Draper::Decorator
  delegate_all

  def author(event)
    if object.whodunnit.to_i > 0
      if target_user
        target_user.name
      else
        "unknown"
      end
    elsif object.whodunnit == 'client'
      "#{event.client_name} (#{helpers.client_name})"
    elsif object.whodunnit == 'system'
      "Practicaldesk"
    end
  end

  def target_user
    @target_user ||= helpers.current_account.users.unscoped.where(id: object.whodunnit).first
  end

  def action
    action = action_to_key
    action << " #{object.info}" if object.info.present?
    action << formatted_reschedule_info.to_s
    action
  end

  def formatted_reschedule_info
    return unless event_rescheduled?
    return duration_change if duration_changed?
    return day_change if day_changed?
    return same_day_reschedule if same_day_reschedule?
  end

  def event_rescheduled?
    change_keys.include?('rescheduled_at')
  end

  def duration_change
    "<br/>#{formatted_version_start_date} <b>from</b> #{formatted_version_start_time} <b>to</b> <strike>#{formatted_old_end_time}</strike> #{formatted_new_end_time}"
  end

  def day_change
    "<br/><strike>#{old_datetime}</strike> &mdash; #{new_datetime}"
  end

  def same_day_reschedule
    "<br/>#{formatted_version_start_date} at <strike>#{formatted_old_time_range}</strike> &mdash; #{formatted_new_time_range}"
  end

  def formatted_old_time_range
    "#{changed_start_time.first.in_time_zone.strftime('%l:%M%p')} - #{changed_end_time.first.in_time_zone.strftime('%l:%M%p')}"
  end

  def formatted_new_time_range
    "#{changed_start_time.last.in_time_zone.strftime('%l:%M%p')} - #{changed_end_time.last.in_time_zone.strftime('%l:%M%p')}"
  end

  def formatted_version_start_date
    version_start_time.to_s(:short_date)
  end

  def formatted_version_start_time
    object.object["start_time"].in_time_zone.strftime("%l:%M%p")
  end

  def version_start_time
    object.object["start_time"].in_time_zone
  end

  def changed_end_time
    object.changeset['end_time']
  end

  def changed_start_time
    object.changeset['start_time']
  end

  def formatted_old_end_time
    changed_end_time.first.in_time_zone.strftime("%l:%M%p")
  end

  def formatted_new_end_time
    changed_end_time.last.in_time_zone.strftime("%l:%M%p")
  end

  def day_changed?
    changed_start_time.first.in_time_zone.day != changed_start_time.last.in_time_zone.day
  end

  def same_day_reschedule?
    changed_start_time.first.in_time_zone.day == changed_start_time.last.in_time_zone.day
  end

  def old_datetime
    changed_start_time.first.in_time_zone.to_s(:short_datetime)
  end

  def new_datetime
    changed_start_time.last.in_time_zone.to_s(:short_datetime)
  end

  def duration_changed?
    change_keys.include?('end_time') && !change_keys.include?('start_time')
  end

  def change_keys
    object.changeset.keys
  end

  def action_name
    return "created" if created?
    object.action
  end

  #converts the combination of "action" and "via" attributes into an I18n key
  def action_to_key
    key = ".actions.#{action_name}"
    key << "_#{object.via}" if object.via.present?
    h.t(key, event_name: h.event_name)
  end

  def date
    object.created_at.to_s(:short_datetime)
  end

  def created?
    object.event == 'create'
  end

end
