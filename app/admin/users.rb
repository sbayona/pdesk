ActiveAdmin.register User do
  config.sort_order = 'created_at_desc'
  permit_params :email, :password, :password_confirmation, :first_name, :last_name, :role

  actions :all, except: :new


  member_action :become, method: :get do
    sign_in(:user, resource)
    redirect_to "/", notice: "Now signed in as #{resource.name}"
  end

  action_item :view, only: :show do
    link_to 'Become', become_admin_user_path(resource)
  end

  index do
    selectable_column
    column :name do |user|
      link_to user.name, admin_user_path(user)
    end
    column :account
    column :email
    column :role
    column 'Last seen', :last_sign_in_at
    column :created_at
    actions
  end

  filter :email
  filter :last_sign_in_at
  filter :created_at

  show do
    columns do
      column do
        attributes_table do
          row :id
          row :name
          row :email
          row :role
          row :last_sign_in_at
          row :last_sign_in_ip
          row :sign_in_count
          row :created_at
          row :updated_at
          row :account_owner
          row :current_sign_in_at
          row :current_sign_in_ip
          row :deleted_at
          row :failed_attempts
          row :invitation_accepted_at
          row :invitation_created_at
          row :invitation_limit
          row :invitation_sent_at
          row :invitation_token
          row :invitations_count
          row :invited_by_id
          row :invited_by_type
          row :last_sign_in_at
          row :last_sign_in_ip
          row :locked_at
          row :remember_created_at
          row :reset_password_sent_at
          row :reset_password_token
          row :type
          row :unlock_token
        end
      end
      column do
        panel 'Account Details' do
          attributes_table_for user.account do
            row :name
            row :state
            row :created_at
            row :updated_at
            row :time_zone
            row :client_name
            row :event_name
            row :address
            row :last_4_digits
            row :stripe_id
            row :stripe_subscription_id
            row :subscription_plan_id
          end
          div do
            link_to 'More details', admin_account_path(user.account)
          end
        end
      end
    end
  end

  form do |f|
    f.inputs "User Details" do
      f.input :first_name
      f.input :last_name
      f.input :role, :as => :select, :collection => User::ROLES, include_blank: false
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

end
