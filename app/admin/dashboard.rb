ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do

    columns do
      column do
        panel "charts" do
          start_date = 1.year.ago.beginning_of_month.to_date
          end_date = Date.today.beginning_of_month
          metric = Metric.new(start_date: start_date.to_s(:db), end_date: end_date.to_s(:db))
          @monthly_events_created = metric.events_created
          @monthly_accounts_created = metric.accounts_created

          render partial: 'charts', 
            locals: {
              monthly_events_created: @monthly_events_created,
              monthly_accounts_created: @monthly_accounts_created
            }
        end
      end
    end

    columns do
      column do
        panel "Recent Charges" do
          table_for Charge.includes(:account).sorted.limit(5) do
            column :amount do |charge|
              link_to "#{number_to_currency(charge.amount)} #{charge.currency.upcase}", admin_charge_path(charge)
            end
            column :account
            column :charged_at
          end
          strong ( link_to 'View all charges', admin_charges_path )
        end
      end

      column do
        panel "Recent Signups" do
          table_for User.sorted.limit(5) do
            column :name do |user|
              link_to user.name, admin_user_path(user)
            end
            column :account_name
            column :email
            column :role
            column :created_at do |u|
              u.created_at.strftime('%D')
            end
          end
          strong ( link_to 'View all users', admin_users_path )
        end
      end
    end

    columns do
      column do
        panel "Recent events" do
          table_for Event.recently_created.limit(5) do
            column :user_name
            column :account_name
            column :created_at do |event|
              event.created_at.strftime('%D')
            end
          end
        end
      end

      column do
        panel "Expiring Accounts" do
          table_for Account.on_trial.take(5) do
            column :name
            column :state
            column :expire_on do |account|
              account.trial_period_ends_on_days
            end
          end
          strong ( link_to 'View all expiring accounts', admin_accounts_path(scope: :on_trial) )
        end
      end
    end
  end # content
end
