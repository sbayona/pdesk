ActiveAdmin.register Account do
  config.sort_order = 'created_at_desc'
  actions :all, except: [:new, :destroy]

  scope :on_trial

  index do
    selectable_column
    column :name do |account|
      link_to account.name, admin_account_path(account)
    end

    column :state
    column 'Users' do |account|
      account.users.count
    end
    column :subscription_plan
    column :created_at
    column :trial_expire_on do |account|
      account.trial_period_ends_on_days if account.trial_period_ends_on_days > 0
    end
    actions
  end

  filter :state, as: :select, collection: Account::PASIVE_STATES
  filter :subscription_plan
  filter :created_at

  show do
    columns do
      column do
        attributes_table do
          row :id
          row :name
          row :state
          row :created_at
          row :updated_at
          row :time_zone
          row :client_name
          row :event_name
          row :address
          row :last_4_digits
          row :stripe_id
          row :stripe_subscription_id
          row :subscription_plan_id
          row :account_owner
        end
      end
      column do
        panel 'Users asociated' do
          table_for account.users do
            column :name do |user|
              link_to user.name, admin_user_path(user)
            end
            column :email
            column :role
          end
        end
        panel 'Stripe Information' do
          pre do
            JSON.pretty_generate( Stripe::Customer.retrieve(account.stripe_id).to_hash)
          end if account.stripe_id
        end
      end
    end
    panel 'Charge details' do
      paginated_collection(account.charges.page(params[:page]).per(10), download_links: false) do
        table_for collection do
          column :amount
          column :currency
          column :description
          column :statement
          column :charged_at
          column :status
          column 'Actions' do |charge|
            link_to 'View details', admin_charge_path(charge)
          end
        end
      end
    end
  end

  action_item :activate, only: :show, if: proc{ !account.is_active? } do
    link_to('Activate account', activate_admin_account_path(account), :method => :put)
  end

  action_item :cancel, only: :show, if: proc{ !account.is_canceled? } do
    link_to('Cancel account', cancel_admin_account_path(account), :method => :put)
  end

  member_action :activate, :method => :put do
    account = Account.find(params[:id])
    account.activate!
    flash[:notice] = "Account #{account.name} has been activated"
    redirect_to :action => :show
  end

  member_action :cancel, :method => :put do
    account = Account.find(params[:id])
    account.cancel!
    flash[:notice] = "Account #{account.name} has been canceled"
    redirect_to :action => :show
  end

  controller do
    def scoped_collection
      Account.includes(:subscription_plan)
    end
  end

  batch_action :activate do |selection|
    count = Account.where(id: selection).each { |ac| ac.activate! }.size
    redirect_to collection_path, :notice => "#{count} accounts activated"
  end

  batch_action :cancel do |selection|
    count = Account.where(id: selection).each { |ac| ac.cancel! }.size
    redirect_to collection_path, :notice => "#{count} accounts deactivated"
  end

end
