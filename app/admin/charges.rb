ActiveAdmin.register Charge do
  decorate_with ChargeDecorator
  config.sort_order = 'charged_at_desc'
  actions :index, :show


  index do
    column :amount do |charge|
      charge.amount_with_currency
    end
    column :description
    column :statement
    column :charged_at
    column :status
    actions
  end

  filter :charged_at
  filter :status, as: :select, collection: Charge::STATUSES

  show do
    panel 'Charge Detail' do
      columns do
        column do
          attributes_table_for charge do
            row :id
            row :amount
            row :currency
            row :customer
            row :description
            row :statement
            row :card_last4
            row :card_brand
            row :card_exp_month
            row :card_exp_year
            row :card_country
            row :charged_at
            row :account_id
            row :created_at
            row :updated_at
            row :stripe_event_id
            row :sequential_id
            row :status
          end
        end
        column do
          attributes_table_for charge do
            row :raw_data do |charge|
              pre do
                JSON.pretty_generate(charge.raw_data)
              end if charge.raw_data
            end
          end
        end
      end
    end
  end

end
