class DashboardPresenter

  attr_reader :current_user, :params, :cookies

  def initialize(current_user, params, cookies)
    @current_user = current_user
    @params = params
    @cookies = cookies
  end

  # determine the current schedule by looking at params and cookies
  def current_schedule
    return @current_schedule if defined?(@current_schedule)

    schedule_id = cookies.permanent[:schedule] = params[:schedule]

    if schedule_id.to_i > 0
      @current_schedule = current_user.account.schedules.where(id: schedule_id).first
    else
      @current_schedule = nil
    end
  end

  def events_today
    current_schedule.events.today if current_schedule
  end

  def events_tomorrow
    current_schedule.events.tomorrow if current_schedule
  end

  def events_this_week
    current_schedule.events.this_week if current_schedule
  end

  def all_schedules
    current_user.account.schedules
  end

  def frequency
    if current_schedule
      current_schedule.frequency
    else
      all_schedules.map(&:frequency).min
    end
  end

  def events_path
    if current_schedule
      "/events.json?schedule_id=#{current_schedule.id}"
    else
      "/events.json"
    end
  end

  def min_time
    if current_schedule
      current_schedule.starts_at
    else
      all_schedules.map(&:starts_at).min
    end
  end

  def max_time
    if current_schedule
      current_schedule.ends_at
    else
      all_schedules.map(&:ends_at).max
    end
  end

  # find the union of all days shown by a list of schedules
  def shown_days
    if current_schedule
      days = current_schedule.business_days
    else
      days = all_schedules.inject([]) {|a, s| a | s.business_days }
    end

    days.collect {|day| day.to_i }
  end

  # find days not shown by any of the schedules
  def hidden_days
    # the inverse of shown_days
    (0..6).to_a - shown_days
  end

  def schedule_name
    current_schedule ? current_schedule.name : I18n.t('presenters.dashboard.all_schudules_name')
  end

  def schedule_id
    current_schedule.id if current_schedule
  end

end
