class AppointmentDrop < Liquid::Drop

  def initialize(event)
    @event = event
  end

  def date
    @event.start_time.to_s(:event_date)
  end

  def start_time
    @event.start_time.to_s(:event_time)
  end

  def weekday
    @event.start_time.strftime("%A")
  end

  def old_start_date
    @event.length_changed? ? @event.current_event_date : @event.old_start_time.to_s(:short_date)
  end

  def old_start_time
    @event.length_changed? ? @event.time_period : @event.old_start_time.to_s(:event_time)
  end

  def new_start_time
    @event.new_start_time
  end

  def changed_on_the_same_day?
    @event.on_the_same_day?
  end

  def reschedule_info
    if changed_on_the_same_day?
      @event.length_changed? ? @event.current_event_time_period : new_start_time.to_s(:event_time)
    else
      @event.length_changed? ? @event.current_event_time_period : new_start_time.to_s(:short_datetime)
    end
  end

  def confirmation_code
    Event::CONFIRMATION_CODE[:CONFIRMED]
  end

  def cancellation_code
    Event::CONFIRMATION_CODE[:CANCELED]
  end

  def callback_code
    Event::CONFIRMATION_CODE[:CALLBACK]
  end

  def scheduling_phone_number
    @event.scheduling_phone_number
  end

end
