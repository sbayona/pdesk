class ClientTemplateDrop < Liquid::Drop
  def initialize(account)
    @account = account
  end

  def name
    "{{#{@account.client_name}.name}}"
  end

  def first_name
    "{{#{@account.client_name}.first_name}}"
  end

  def last_name
    "{{#{@account.client_name}.last_name}}"
  end


end
