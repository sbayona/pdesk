class AppointmentTemplateDrop < Liquid::Drop
  def initialize(account)
    @account = account
  end

  def date
    "{{#{@account.event_name}.date}}"
  end

  def start_time
    "{{#{@account.event_name}.start_time}}"
  end

end
