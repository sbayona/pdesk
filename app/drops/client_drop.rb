class ClientDrop < Liquid::Drop

  def initialize(client)
    @client = client
  end

  def name
    @client.name
  end

  def first_name
    @client.first_name
  end

  def last_name
    @client.last_name
  end

end
