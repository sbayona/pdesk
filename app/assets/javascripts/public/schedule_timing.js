(function() {
  var ready;

  ready = function() {
    {
      $(document).on('change', 'select#schedule_timing_schedule_timing_options_attributes_1_value', function() {
        if($(this).val() == "0") {
          $('#schedule_timing_schedule_timing_options_attributes_1_days_before').addClass('hidden');
        } else {
          $('#schedule_timing_schedule_timing_options_attributes_1_days_before').removeClass('hidden');
        }
      })

      $(document).on('change', 'select#schedule_timing_schedule_timing_options_attributes_0_value', function() {
        if($(this).val() == "0") {
          $('#schedule_timing_schedule_timing_options_attributes_0_days_before').addClass('hidden');
        } else {
          $('#schedule_timing_schedule_timing_options_attributes_0_days_before').removeClass('hidden');
        }
      })
    }
  }

  $(document).on('turbolinks:load', ready);

}).call(this);

