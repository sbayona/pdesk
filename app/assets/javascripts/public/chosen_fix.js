// fixes issue where chosen doesn't load correctly when the dropdown is invisible.
// to be placed anywhere before the jquery.chosen initialization
    var getHiddenOffsetWidth = function (el) {
    // save a reference to a cloned element that can be measured
    var $hiddenElement = $(el).clone().appendTo('body');
    // calculate the width of the clone
    var width = $hiddenElement.outerWidth();
   // remove the clone from the DOM
   $hiddenElement.remove();
   return width;
};
