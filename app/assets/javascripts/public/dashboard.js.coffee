$(document).on 'turbolinks:load', ->
  calendar = $("#calendar")

  calendar.fullCalendar
    editable: true
    header:
      left: "prev,next today"
      center: "title"
      right: "month,agendaWeek,agendaDay"

    defaultView: "agendaWeek"
    slotMinutes: (calendar.data('frequency') || 15)
    minTime: (calendar.data('min-time') || 3)
    maxTime: (calendar.data('max-time') || 18)
    allDaySlot: false
    startParam: 'start_time'
    endParam: 'end_time'
    dragOpacity: '0.5'
    timeFormat: "h:mm t{ - h:mm t} "
    events: (calendar.data('events-path') || "/events.json")
    hiddenDays: (calendar.data('hidden-days') || [])
    dayClick: (date, allDay, jsEvent, view) ->
      schedule_id = calendar.data('schedule-id')
      calendarEvents.dayClick date, allDay, jsEvent, view, schedule_id

    eventDrop: (event, dayDelta, minuteDelta, allDay, revertFunc) ->
      calendarEvents.move event, dayDelta, minuteDelta, allDay

    eventResize: (event, dayDelta, minuteDelta, revertFunc) ->
      calendarEvents.resize event, dayDelta, minuteDelta

    eventClick: (event, jsEvent, view) ->
      calendarEvents.edit event, jsEvent, view

    loading: (isLoading) ->
      schedule_name = calendar.data('schedule-name')

      if !isLoading && $('.fc-header-title h2').text().indexOf(schedule_name) < 0
        $('.fc-header-title h2').prepend(schedule_name + "&nbsp;&nbsp;")

    eventRender: (event, element, agendaDay) ->
      element.attr('id', "event_" + event.id)
      if !event.annotation
        element.prepend($('<div class="color-band">&nbsp;</div>'))
