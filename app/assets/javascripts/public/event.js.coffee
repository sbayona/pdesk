ready = ->
  $('.subscribers').hide()

  $('#internal_communication').click ->
    if $(this).is(':checked')
      $('.subscribers').show()
    else
      $('.subscribers').hide()

$(document).on('turbolinks:load', ready)
