ready = ->
  $('#clearSearch').click ->
     $('#searchField').val('')
     $('#searchField').parents('form').submit()
     false

  $('#client_mobile_number').mask("(999) 999-9999")
  $('#client_phone_number').mask("(999) 999-9999")

$(document).on('turbolinks:load', ready)
