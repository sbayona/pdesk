(function ($) {
  $.fn.eventModal = function (url, data) {
    $("#event-modal")
      .one('show.bs.modal', function() {
        $(this).find('.modal-content').load(url, data)
      })
      .one('shown.bs.modal', function() {
        $('#clients-menu').focus();
      })
      .one('hidden.bs.modal', function() {
        $(this).removeData('bs.modal');
      })
      .modal()

  }
})(jQuery);
