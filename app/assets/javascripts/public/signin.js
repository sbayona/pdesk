(function ($) {
    "use strict";

    // Detecting IE
    var oldIE;
    if ($('html').is('.ie6, .ie7, .ie8')) {
        oldIE = true;
    }

    if (oldIE) {
        window.location = "/browser_not_supported";
    }

}(jQuery));
