// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require turbolinks
//= require fullcalendar
//= require jquery_ujs
//= require bootstrap
//= require twitter/typeahead
//= require hogan.js
//= require flashlight
//= require nprogress
//= require nprogress-turbolinks
//= require moment.min
//= require daterangepicker
//= require_tree ../public/



var ready = function() {

  $('body').on('hidden.bs.modal', '.modal', function () {
      $(this).removeData('bs.modal');
      $(this).find('.modal-content').empty();
  });

  // Disable submit buttons when are pressed
  // For cases when ajax calls are invoked and the button
  // is required as enabled again, you should use:
  //
  // $("form .btn[type=submit]").button('reset');
  //
  // For more customization, check http://getbootstrap.com/javascript/#buttons-stateful
  $("form .btn[type=submit]").click(function(){
    $(this).button('loading');
  });
};

$(document).on('turbolinks:load', ready);
