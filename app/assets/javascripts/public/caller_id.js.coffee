@ConfirmationPoller =
  poll: ->
    setTimeout @request, 1000

  request: ->
    $.ajax 
      url: $('#pollingUrl').data('url')
      success: (data) ->
        if data.verification_response == true
          $.get($('#verifiedUrl').data('url'))
        else 
          ConfirmationPoller.poll()
      dataType: "json"

jQuery ->
  if $('#validationCode').length > 0
    ConfirmationPoller.poll()
