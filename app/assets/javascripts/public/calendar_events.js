var calendarEvents = (function() {
  // defined within the local scope
  var move = function(event, dayDelta, minuteDelta, Allday) {
    data = {event: {day_delta: dayDelta, minute_delta: minuteDelta}};
    $.ajax({type: "PUT", url: "events/"+event.id+".json", data: data});
  }

  var dayClick = function(date, allDay, jsEvent, view, schedule_id) {
    params = "event[start_time]="+encodeURIComponent(date)+"&event[all_day]="+allDay+"&event[schedule_id]="+schedule_id;
    url = "/events/new?"+params;
    $(view.element).eventModal(url);
  }

  var resize = function(event, dayDelta, minuteDelta) {
    data = {event: {day_delta: dayDelta, minute_delta: minuteDelta, resize: true}};
   $.ajax({type: "PUT", url: "events/"+event.id+".json", data: data});
  }

  var edit = function(event, jsEvent, view, url) {
    url = "/events/" + event.id + "/edit.js";
    $(event.element).eventModal(url, null);
    return false;
  }

  var loadTypeAhead = function() {
    var clients = new Bloodhound({
      datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      remote: {
        url: '/clients/search/%QUERY.json',
        wildcard: '%QUERY'
      },
      prefetch: '/clients.json',
      dupDetector: function(remoteMatch, localMatch) { return remoteMatch.id === localMatch.id; }
    });

    clients.initialize();

    var compiledTemplate = Hogan.compile(
        ['<div class="contact">{{name}}</div>'].join('')
      )

    $('#clients-menu').typeahead(null, {
      displayKey: 'name',
      source: clients.ttAdapter(),
      templates: {
        suggestion: compiledTemplate.render.bind(compiledTemplate)
      }
    })
    .on("typeahead:selected", function(obj, datum) {
      $('#event_client_attributes_email').val(datum['email']);
      $('#event_client_attributes_mobile_number').val(datum['mobile_number']);
      $('#event_client_attributes_phone_number').val(datum['phone_number']);
      $('#event_client_attributes_id').val(datum['id']);
    })
  }

  return {
    move: move,
    resize: resize,
    edit: edit,
    dayClick: dayClick,
    utils: {
      loadTypeAhead: loadTypeAhead
    }
  }
})();

