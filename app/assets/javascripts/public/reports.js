(function() {
  var ready;

  ready = function() {
    var $left, $right;

    $left = $('#reportrange input[name="q[date_range_left]"]');
    $right = $('#reportrange input[name="q[date_range_right]"]');

    if ($left.val() && $right.val()) {
      $('#reportrange span').html(
        moment($left.val()).format('MMMM D, YYYY') + ' - ' + moment($right.val()).format('MMMM D, YYYY')
      );
    } else {
      $('#reportrange span').html('Select date range');
    }

    $('#reportrange').daterangepicker({
      format: 'MM/DD/YYYY',
      startDate: moment().subtract(29, 'days'),
      endDate: moment(),
      dateLimit: { days: 60 },
      showDropdowns: true,
      showWeekNumbers: true,
      timePicker: false,
      ranges: {
        'Today': [moment(), moment()],
        'Tomorrow': [moment().add(1, 'day'), moment().add(1, 'day')],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      opens: 'left',
      drops: 'down',
      buttonClasses: ['btn', 'btn-sm'],
      applyClass: 'btn-primary',
      cancelClass: 'btn-default',
      separator: ' to ',
      locale: {
        applyLabel: 'Select',
        cancelLabel: 'Cancel',
        fromLabel: 'From',
        toLabel: 'To',
        customRangeLabel: 'Custom',
        daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        firstDay: 1
      }
    }, function(start, end, label) {
      var left = start.format('YYYY-MM-D');
      var right = end.format('YYYY-MM-D');

      $left.val(left);
      $right.val(right);

      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    });

  }


  $(document).on('turbolinks:load', ready);

}).call(this);
