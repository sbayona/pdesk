ready = ->
  $('[data-toggle=ajaxModal]').on "click", (e) ->
    $("#ajaxModal").remove()
    e.preventDefault()
    $this = $(this)
    $remote = $this.data("remote") or $this.attr("href")
    $modal = $('<div class="modal" id="ajaxModal"><div class="modal-body"></div></div>')
    $("body").append $modal
    $modal.modal
      backdrop: "static"
      keyboard: false

    $modal.load $remote

$(document).on('turbolinks:load', ready);
