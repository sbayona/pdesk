class ScheduleSubscriptionsController < PublicController
  authorize_resource
  
  layout 'settings'

  respond_to :html, :js

  before_action :find_schedule

  def new
    @schedule_subscription = @schedule.schedule_subscriptions.build

    respond_with @schedule_subscription do |format|
      format.html { render :new, layout: false }
    end
  end

  def create
    @schedule_subscription = @schedule.schedule_subscriptions.create(permitted_params[:schedule_subscription])
    respond_with @schedule_subscription
  end

  def destroy
    @schedule_subscription = @schedule.schedule_subscriptions.find(params[:id])
    @schedule_subscription.destroy
    respond_with @schedule_subscription
  end

  private

  def permitted_params
    params.permit(:id, :schedule_id, schedule_subscription: [:user_id, :notification_preferences])
  end

  def find_schedule
    @schedule ||= current_account.schedules.find(params[:schedule_id])
  end

end
