class Script::SmsController < PublicController

  layout false
  respond_to :js, :html

  before_action :find_scripts

  def edit
    respond_with @sms_scripts
  end

  def update
    if @sms_scripts.update_attributes(permitted_params)
      flash[:notice] = t('.success')
    end
    respond_with @sms_scripts
  end

  private

  private

  def find_scripts
    @sms_scripts ||= schedule.sms_scripts.find(params[:id])
  end

  def schedule
    @schedule ||= current_account.schedules.find(params[:schedule_id])
  end

  def permitted_params
    params.require(:script_sms).permit(:message, :cancellation_message, :confirmation_message, :requested_contact_message)
  end
end
