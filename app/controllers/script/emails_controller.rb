class Script::EmailsController < PublicController
  
  layout false
  respond_to :js, :html
  before_action :find_scripts

  def edit
    respond_with @email_scripts
  end

  def update
    if @email_scripts.update_attributes(permitted_params)
      flash[:notice] = t('.success')
    end
    respond_with @email_scripts
  end

  private

  def find_scripts
    @email_scripts ||= schedule.email_scripts.find(params[:id])
  end

  def schedule
    @schedule ||= current_account.schedules.find(params[:schedule_id])
  end

  def permitted_params
    params.require(:script_email).permit(:user_id, :subject, :message, :cancellation_subject, :cancellation_message, :reschedule_subject, :reschedule_message)
  end
end
