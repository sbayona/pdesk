class Script::VoiceController < PublicController
  
  layout false
  respond_to :js, :html
  before_action :voice_scripts

  def edit
    respond_with voice_scripts
  end

  def update
    if voice_scripts.update_attributes(permitted_params)
      flash[:notice] = t('.success')
    end
    respond_with voice_scripts
  end

  private

  def voice_scripts
    @voice_scripts ||= schedule.voice_scripts.find(params[:id])
  end

  def schedule
    @schedule ||= current_account.schedules.find(params[:schedule_id])
  end

  def permitted_params
    params.require(:script_voice).permit(:user_id, :message, :confirmation_message, :cancellation_message, :reschedule_message)
  end
end
