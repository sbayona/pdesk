module Concerns
  module TimeZone

    def set_account_time_zone(&block)
      Time.use_zone(current_account.time_zone, &block)
    end

  end
end
