module Concerns
  module UnavailableMode
    extend ActiveSupport::Concern

    included do
      before_filter :set_unavailable_mode
      helper_method :unavailable_mode?
    end

    protected

    def set_unavailable_mode
      if params[:unavailable_mode] == "true"
        cookies[:unavailable_mode] = true
      elsif params[:unavailable_mode] == "false"
        cookies.delete(:unavailable_mode)
      end
    end

    def unavailable_mode?
      cookies[:unavailable_mode]
    end
  end
end
