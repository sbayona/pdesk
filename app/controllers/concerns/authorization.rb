module Concerns
  module Authorization

    def current_account
      @current_account ||= current_user && current_user.account && current_user.account.decorate
    end

    def redirect_trial_accounts
      redirect_to edit_expire_path
    end

    def needs_subscription?
      current_account && current_account.needs_subscription_info?
    end

  end
end
