module Concerns
  module ExceptionLogging
    extend ActiveSupport::Concern

    included do
      before_filter :log_additional_data
    end

    protected

    def log_additional_data
      request.env["exception_notifier.exception_data"] = {
        :user_name => current_user.try(:name),
        :user_id => current_user.try(:id),
        :user_email => current_user.try(:email)
      }
    end

  end
end
