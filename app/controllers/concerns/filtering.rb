module Concerns
  module Filtering

    def filtered_results(collection)
      @filtered_results ||= begin
        results = collection
        results = results.fuzzy_search(params[:search]) if params[:search].present?
        results = results.order(sort_statement) if sort_statement
        results = results.page(params[:page]).per(20)
        results
      end
    end

    private

    def sort_statement
      return unless params[:sort_by].present?
      "#{params[:sort_by]} ASC"
    end

  end
end
