module Concerns
  module ClientActions
    private

    def is_client_action?
      client_actions.include?(action_name) &&
      has_valid_token? &&
      request.method == 'GET'
    end

    def has_valid_token?
      return false unless client_token_param.present? && event_uuid_param.present?
      return false unless unscoped_event
      return false unless matches_client_token?
      true
    end

    def confirm_as_client
      PaperTrail.whodunnit = 'client'
      unscoped_event.updated_via = Event::UPDATE_VIA[:EMAIL]
      unscoped_event.confirm!
      render layout: 'client_action'
    end

    def cancel_as_client
      PaperTrail.whodunnit = 'client'
      unscoped_event.cancel!
      render layout: 'client_action'
    end

    def client_actions
      ['confirm', 'cancel']
    end

    def client_token_param
      params[:t]
    end

    def event_uuid_param
      params[:id]
    end

    def unscoped_event
      @unscoped_event ||= Event.find_by(uuid: params[:id])
    end

    def matches_client_token?
      unscoped_event.client && client_token == params[:t]
    end

    def client_token
      @client_token ||= unscoped_event.client.token
    end

  end
end
