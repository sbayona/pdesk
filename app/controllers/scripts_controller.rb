class ScriptsController < PublicController
  authorize_resource
  layout 'settings'

  before_action :find_schedule
  
  private

  def find_schedule
    @schedule ||= current_account.schedules.find(permitted_params[:schedule_id])
  end

  def permitted_params
    params.permit(:schedule_id)
  end
end
