class VoiceController < ApplicationController
  include EventsHelper

  before_filter :event, :set_timezone
  before_filter :set_whodunnit, only: [:confirmation, :cancelation, :callback]
  before_filter :set_updated_via
  layout false

  def reminder
  end

  def update
    if confirmed?
      redirect_to voice_confirmation_url(format: :xml, uid: params[:uid])
    elsif canceled?
      redirect_to voice_cancelation_url(format: :xml, uid: params[:uid])
    elsif callback?
      redirect_to voice_callback_url(format: :xml, uid: params[:uid])
    end
  end

  def confirmation
    event.confirm!
  end

  def cancelation
    event.cancel!
  end

  def callback
    event.callback!
  end

  private

  def set_whodunnit
    PaperTrail.whodunnit = 'client'
  end

  def event
    @event ||= Event.where(uuid: params[:uid]).first rescue nil
  end

  def set_updated_via
    event.updated_via = Event::UPDATE_VIA[:VOICE] if event
  end

  def set_timezone
    if event
      Time.zone = event.time_zone
      event.reload # we need to reload the record so its timestamps pick up the time zone
    end
  end

  def confirmed?
    params[:Digits] == Event::CONFIRMATION_CODE[:CONFIRMED]
  end

  def canceled?
    params[:Digits] == Event::CONFIRMATION_CODE[:CANCELED]
  end

  def callback?
    params[:Digits] == Event::CONFIRMATION_CODE[:CALLBACK]
  end

end
