class AccountsController < PublicController
  authorize_resource

  respond_to :html

  layout 'settings'

  def update
    if current_account.update_attributes(permitted_params)
      flash[:notice] = t('.success')
    end
    respond_with current_account, location: edit_account_path
  end

  def destroy
    current_account.cancel! if current_account.account_owner == current_user
    sign_out current_user
    redirect_to root_path
  end

  private

  def permitted_params
    params.require(:account).permit(:name, :address, :phone, :client_name, :event_name, :time_zone)
  end

end
