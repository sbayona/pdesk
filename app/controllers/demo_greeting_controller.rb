class DemoGreetingController < ApplicationController

  def show
    demo = Demo.find_by_uuid(params[:id])
    send_file(demo.greeting.path, 
      type: 'audio/mpeg', 
      disposition: 'inline',
      filename: "#{demo.uuid}.mp3")
  end
end
