class ProfilesController < PublicController
  respond_to :html

  layout 'settings'

  def update  
    if current_user.update_attributes(permitted_params)
      flash[:success] = t('.success')
    end
    respond_with current_user, location: edit_profile_path
  end 

  private

  def permitted_params
    params[:user].delete(:password) if params[:user][:password].blank?
    params[:user].delete(:password_confirmation) if params[:user][:password].blank?
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
  end

end
