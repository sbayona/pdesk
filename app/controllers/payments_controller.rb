class PaymentsController < PublicController
  layout 'settings'
  load_and_authorize_resource :current_account

  def edit
    unless current_account.subscription_plan
      flash[:notice] = 'You must to select a subscription plan first'
      redirect_to action: :show, controller: :billings
    end
  end

  def update
    if current_account.update_attributes(account_params)
      redirect_to billing_path
    else
      render :edit
    end
  end

 private

  def account_params
    params.require(:account).permit(:stripe_token, :last_4_digits)
  end
end
