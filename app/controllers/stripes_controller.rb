class StripesController < ActionController::Base

  respond_to :xml

  def charge
    # Retrieving the event from the Stripe API guarantees its authenticity
    event = Stripe::Event.retrieve(params[:id])
    if Charge::STATUSES.include? Charge.get_type_event(event)
      charge = Charge.new_from_stripe_event(event)
      charge.raw_data = params
      if charge.save
        charge.send_notification_to_account!
        head 200
      else
        logger.info "Event #{params[:id]} failed to save: #{charge.errors.full_messages}"
        head 422
      end
    else
      logger.info "Unknown event type #{event.type} for event #{params[:id]}"
      head 422
    end

  end

end
