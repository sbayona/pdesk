class ReportsController < PublicController
  def index
    @events = EventReportDecorator.new(
      EventReport.new(current_account, params)
    )

    render :index
  end
end
