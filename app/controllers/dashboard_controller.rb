class DashboardController < PublicController
  before_filter :redirect_if_schedule_present, only: :index


  def index
    @dashboard = DashboardPresenter.new(current_user, params, cookies)
  end

  def onboarding
  end

  private

  def redirect_if_schedule_present
    if redirect_to_preferred_schedule?
      redirect_to root_path(schedule: current_schedule) and return
    end
    # Redirect to the first schedule if there's only one
    if current_account.schedules.one? && params[:schedule].nil?
      single_schedule = current_account.schedules.first
      redirect_to(root_path(schedule: single_schedule.id))
    end
  end

  def redirect_to_preferred_schedule?
    params[:schedule].nil? &&
    current_account.schedules.exists?(current_schedule)
  end

  def current_schedule
    cookies.permanent[:schedule]
  end
end
