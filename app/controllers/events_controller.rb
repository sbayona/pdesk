class EventsController < PublicController
  authorize_resource

  include Concerns::ClientActions

  before_action :halt_if_no_schedule_selected, only: :new
  before_action :initialize_event, only: [:new, :create]
  skip_before_filter :authenticate_user!, if: -> { is_client_action? }

  layout 'events', only: :show

  respond_to :js, :json, :html

  def index
    events
  end

  def show
    @event_versions = EventVersionDecorator.decorate_collection(event.versions)
    @note = Note.new
    @notes = @event.notes.sorted.decorate

    respond_with event
  end

  def edit
    respond_with event
  end

  def new
    respond_with @event do |format|
      format.html { render layout: false }
    end
  end

  def create
    @event.save
    respond_with(@event)
  end

  def update
    event.update_attributes(permitted_params)
    respond_with(event)
  end

  def confirm
    if is_client_action?
      confirm_as_client
    else
      update_event('confirm!')
    end
  end

  def cancel
    if is_client_action?
      cancel_as_client
    else
      update_event('cancel!')
    end
  end

  def showed
    update_event('show!')
  end

  def no_show
    update_event('no_show!')
  end

  def destroy
    update_event('soft_delete!')
  end

  private

  def permitted_params
    params.require(:event).permit(:unavailable_time, :schedule_timing_id, :resize, :day_delta, :minute_delta, :title, :client_name, :start_time, :end_time, :all_day, :description, :schedule_id, :script_group_id, client_attributes: [:id, :name, :phone_number, :mobile_number, :email], reminder_preferences: [])
  end

  def end_time
    Time.at(params[:end_time].to_i)
  end

  def start_time
    Time.at(params[:start_time].to_i)
  end

  def events
    @events ||= begin
      events = current_account.events.includes(:client)
      events = events.where(schedule_id: params[:schedule_id]) if params[:schedule_id].present?
      events = events.where("start_time >= ? and end_time <= ?", start_time, end_time) if params[:start_time].present?
      events
    end
  end

  def event
    @event ||= current_account.events.find(params[:id])
  end

  def initialize_event
    @event = current_account.events.build(permitted_params)
    @event.user = current_user
  end

  def update_event(type)
    event.updated_via = Event::UPDATE_VIA[:WEB]
    event.send(type)
    respond_to do |format|
      format.json { head :no_content }
      format.js { render 'update_event' }
    end
  end

  def halt_if_no_schedule_selected
    schedule_id = cookies.permanent[:schedule] || params[:event][:schedule]

    return if current_account.schedules.exists?(schedule_id)

    respond_to do |format|
      format.js { render partial: 'please_select_schedule' }
    end
    false
  end

end
