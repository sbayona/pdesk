class ExpiresController < PublicController
  # layout 'settings'
  load_and_authorize_resource :current_account
  before_action :plans
  skip_before_filter :redirect_trial_accounts
  before_filter :reject_trial_accounts

  def edit
  end

  def update
    if current_account.update_attributes(account_params)
      redirect_to root_path
    else
      render :edit
    end
  end

 private

  def account_params
    params.require(:account).permit(:stripe_token, :last_4_digits, :subscription_plan_id)
  end

  def plans
    @plans = SubscriptionPlan.all
  end

  def reject_trial_accounts
    redirect_to root_path unless current_account.needs_subscription_info?
  end

end
