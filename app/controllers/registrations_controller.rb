class RegistrationsController < Devise::RegistrationsController
  layout 'application'

  def new
    @account_owner = AccountOwner.new
    @account_owner.build_account
    respond_with @account_owner
  end

  def create
    @account_owner = AccountOwner.new(sign_up_params)
    @account_owner.role = User::ROLES[:ADMIN]

    if @account_owner.save
      if @account_owner.active_for_authentication?
        UsersMailer.sign_up_notification(@account_owner).deliver_now
        AdministrativeMailer.signup_notification(@account_owner).deliver_now

        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_up(resource_name, @account_owner)
        respond_with @account_owner, :location => after_sign_up_path_for(@account_owner)
      else
        set_flash_message :notice, :"signed_up_but_#{@account_owner.inactive_message}" if is_navigational_format?
        expire_session_data_after_sign_in!
        respond_with @account_owner, :location => after_inactive_sign_up_path_for(@account_owner)
      end
    else
      clean_up_passwords @account_owner
      respond_with @account_owner
    end
  end

  private

  def sign_up_params
    params.require(:user).permit(:full_name, :email, :password, account_attributes: [:name, :stripe_token, :last_4_digits, :subscription_plan_id])
  end

end
