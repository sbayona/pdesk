class CallerIdsController < PublicController
  before_action :find_schedule
  before_action :initialize_caller_id, only: :show
  before_action :delete_resource!, only: :show, if: :resource_has_expired

  authorize_resource

  respond_to :html, :js
  
  layout 'settings'

  def new
    @caller_id = @schedule.build_caller_id
    respond_with(@caller_id)
  end

  def create
    @caller_id = @schedule.build_caller_id(permitted_params)
    if @caller_id.save
      flash[:notice] = t('.success')
    end
    respond_with(@caller_id)
  end

  def destroy
    @caller_id = @schedule.caller_id.destroy
    respond_with @caller_id, location: schedule_caller_id_path
  end

  def verification_check
    verification_response = @schedule.caller_id.has_verification_answer?
    respond_to do |format|
      format.js do 
        render json: { :verification_response => verification_response }.to_json 
      end
    end
  end

  def verified
  end

  private

  def find_schedule
    @schedule ||= current_account.schedules.find(params[:schedule_id])
  end

  def permitted_params
    params.require(:caller_id).permit(:number, :friendly_name)
  end

  def initialize_caller_id
    @caller_id = CallerId.new
  end

  def delete_resource!
    @schedule.caller_id.destroy
  end

  def resource_has_expired
    return false if @schedule.caller_id.nil?
    @schedule.caller_id.expired?
  end

end
