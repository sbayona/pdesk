class UsersController < PublicController
  authorize_resource

  layout 'settings'

  def index
    @users = current_account.users
  end

  def show
    @user = current_account.users.find(params[:id])
  end

  def update
    if resource.update_attributes(user_params)
      flash[:notice] = t('.notice')
      redirect_to action: :show
    end
  end

  def destroy
    @user = current_account.users.find(params[:id])
    @user.soft_delete!
    flash[:alert] = t('.alert')
    redirect_to users_path
  end

  def resend_invitation
    @user = current_account.users.invited.find(params[:id])
    @user.deliver_invitation
    flash[:alert] = t('.alert')
    redirect_to users_path
  end

  private

  def user_params
    params.require(:user).permit(:role)
  end

  def resource
    @user ||= current_account.users.find(params[:id])
  end

end
