class BillingsController < PublicController
  layout 'settings'

  def show
    @subscription_plan = current_account.subscription_plan
    @charges = current_account.charges.decorate
  end

  def edit
  end

  def update
  end

end
