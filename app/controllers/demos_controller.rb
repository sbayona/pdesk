class DemosController < ApplicationController
  before_filter :find_demo
  skip_before_action :verify_authenticity_token

  def reminder
    user = User.where(mobile_number: params["To"]).first
    @demo = user.demos.last
  end

  def options
    if confirmed?
      redirect_to voice_confirmation_url(format: :xml)
    elsif canceled?
      redirect_to voice_cancelation_url(format: :xml)
    end
  end

  private
  
  def confirmed?
    params[:Digits] == Event::CONFIRMATION_CODE[:CONFIRMED]
  end

  def canceled?
    params[:Digits] == Event::CONFIRMATION_CODE[:CANCELED]
  end

  def find_demo
    @demo ||= Demo.where(uuid: params[:uuid]).first
  end

end
