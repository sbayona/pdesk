class SchedulesController < PublicController
  authorize_resource

  respond_to :html

  layout 'settings'

  def new
    @schedule = current_account.schedules.build
    respond_with(@schedule)
  end

  def create
    @schedule = current_account.schedules.build(permitted_params)
    respond_with @schedule do |format|
      if @schedule.save
        flash[:notice] = t('.success')
        format.html { redirect_to edit_schedule_path(@schedule) }
      else
        format.html { render action: :new }
      end
    end
  end

  def index
    schedules
  end

  def edit
    schedule
  end

  def show
    schedule
  end

  def update
    if schedule.update_attributes(permitted_params)
      flash[:notice] = t('.success')
    end
    respond_with(schedule, location: edit_schedule_path(schedule))
  end

  def destroy
    flash[:notice] = t('.success') if schedule.destroy
    respond_with(schedule)
  end

  private

  def permitted_params
    params.require(:schedule).permit(:name, :starts_at, :ends_at, :frequency, :length, :notes_enabled, :business_days => [], :reminder_preferences => [])
  end

  def schedules
    @schedules ||= current_account.schedules.page(params[:page]).per(10)
  end

  def schedule
    @schedule ||= current_account.schedules.find(params[:id])
  end

end
