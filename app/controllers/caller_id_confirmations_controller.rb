#used by Twilio for caller ID confirmation callbacks

class CallerIdConfirmationsController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: :create

  def create
    verified = params['VerificationStatus'] == "success" ? true : false
    twilio_sid = params['OutgoingCallerIdSid']
    schedule = Schedule.find(params[:schedule_id])
    schedule.caller_id.update_attributes(verified: verified, twilio_sid: twilio_sid, verified_at: Time.zone.now)
    render nothing: true, status: :ok
  end

end
