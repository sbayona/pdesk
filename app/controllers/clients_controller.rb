class ClientsController < PublicController

  skip_before_filter :authenticate_user!, only: :unsubscribe
  before_action :find_client, only: [:edit, :update, :show, :destroy, :contact_fields]
  before_filter :get_client_events, only: [:show, :contact_fields]

  layout 'clients'

  authorize_resource

  respond_to :html, :json

  include Concerns::Filtering

  def index
    @clients ||= begin
      if request.format.json?
        scoped_clients.limit(1000).order('created_at DESC')
      else
        filtered_results(scoped_clients)
      end
    end
  end

  def new
    @client = Client.new
    respond_with @client
  end

  def create
    @client = current_account.clients.build(permitted_params)
    @client.creator = current_user
    if @client.save
      flash[:notice] = t('.success')
    end
    respond_with @client
  end

  def edit
    respond_with @client
  end

  def update
    if @client.update_attributes(permitted_params)
      flash[:notice] = t('.success')
    end
    respond_with @client
  end

  def destroy
    @client.destroy
    respond_with @client
  end

  def unsubscribe
    @client = Client.where(token: params[:token]).first
    @client.update_attributes(receive_email: false)
    render layout: 'client_action'
  end

  def search
    respond_to do |format|
      format.json do
        @clients = scoped_clients.fuzzy_search(name: params[:name])
        render :index
      end
    end
  end

  private

  def permitted_params
    params.require(:client).permit(:name, :phone_number, :mobile_number, :email)
  end

  def find_client
    @client ||= scoped_clients.find(params[:id])
  end

  def scoped_clients
    @scoped_clients ||= current_account.clients
  end

  def get_client_events
    @client_events = if params[:upcoming]
      @client.upcoming_events.page params[:page]
    else
      @client.past_events.page params[:page]
    end
  end

end
