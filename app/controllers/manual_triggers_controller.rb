class ManualTriggersController < PublicController
  before_action :find_event

  def update
    @event.manually_trigger(params[:event_trigger])
    redirect_to @event
  end

  private

  def find_event
    @event = Event.find(params[:event_id])
  end
end
