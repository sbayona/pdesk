class SubscriptionPlansController < PublicController
  layout false
  respond_to :js
  before_action :plans

  def update
    current_account.validate_subscription_plan = true
    current_account.update_attributes(subscription_plan_id: plan_id)
    respond_with current_account
  end

  private 

  def plan_id
    params[:account] && params[:account][:subscription_plan_id]
  end

  def plans
    @plans = SubscriptionPlan.all
  end

end
