class SmsController < ApplicationController
  before_action :confirm_client_exists, only: :reply
  include Concerns::Twilio
  layout false

  def reply
    PaperTrail.whodunnit = 'client'
    if likely_client.nil?
      raise ArgumentError, "No likely client for #{params['From']}"
    elsif event
      event.updated_via = Event::UPDATE_VIA[:SMS]
      if confirmed?
        event.confirm!
      elsif canceled?
        event.cancel!
      elsif callback?
        event.callback!
      else
        event.unknown_reply
      end
    end
    render status: :ok, nothing: true
  end

  def test
    send_message(to: "3233164935", body: "test", status_callback: "http://6182367e.ngrok.com/sms/callback?monkey=joe")
    render nothing: true
  end

  def callback
    event_log = EventLog.find(params[:log_id])
    event_log.update_attributes(callback_attrs)
    render status: :ok, nothing: true
  end

  private

  def callback_attrs
    callback_attrs = {callback_payload: sms_status}
    callback_attrs.merge!({delivered_at: now}) if message_delivered?
    callback_attrs.merge!({sent_at: now}) if message_sent?
    callback_attrs.merge!({queued_at: now}) if message_queued?
    callback_attrs.merge!({failed_at: now}) if message_failed?
    callback_attrs.merge!({undelivered_at: now}) if message_undelivered?
    callback_attrs
  end

  def now
    Time.zone.now.utc
  end

  def message_delivered?
    sms_status == 'delivered'
  end

  def message_sent?
    sms_status == 'sent'
  end

  def message_queued?
    sms_status == 'queued'
  end

  def message_failed?
    sms_status == 'failed'
  end

  def message_undelivered?
    sms_status == 'undelivered'
  end

  def sms_status
    params["SmsStatus"]
  end

  def confirmed?
    sms_response == Event::CONFIRMATION_CODE[:CONFIRMED]
  end

  def canceled?
    sms_response == Event::CONFIRMATION_CODE[:CANCELED]
  end

  def callback?
    sms_response == Event::CONFIRMATION_CODE[:CALLBACK]
  end

  def sms_response
    params["Body"].strip
  end

  def likely_client
    @likely_client ||= Client.with_outstanding_sms_notification.find_by(mobile_number: params["From"])
  end

  def client_with_confirmed_notifications
    @confirmed_client ||= Client.with_confirmed_sms_notifications.find_by(mobile_number: params["From"])
  end

  def client_data(clients)
    clients.collect {|c| {id: c.id, email: c.email, mobile: c.mobile_number} }
  end

  def event
    @event ||= likely_client.events.active_with_outstanding_sms_confirmation_request.first
  end

  def confirm_client_exists
    if non_existing_client?
      raise ArgumentError, "no client with number #{params['From']}" and return false 
    end
  end

  def non_existing_client?
    !Client.exists?(mobile_number: params['From'])
  end

  def client_from_number
    @client_from_number ||= Client.find_by(mobile_number: params["From"])
  end

end
