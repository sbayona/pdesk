class ApplicationController < ActionController::Base
  include Concerns::ExceptionLogging
  include Concerns::Authorization
  include Concerns::TimeZone
  include Concerns::Twilio
  include Concerns::UnavailableMode

  helper_method :current_account, :needs_subscription?

  private

  def after_sign_in_path_for(resource)
    stored_location_for(resource) ||
    if resource.is_a?(Admin)
      admin_root_url
    else
      super
    end
  end

end
