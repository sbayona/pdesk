class ScheduleTimingsController < PublicController
  authorize_resource

  layout false
  respond_to :html, :js

  before_action :find_schedule_timing, except: [:new, :create, :index]

  def index
    schedule
    respond_with @schedule do |format|
      format.html { render :index, layout: 'settings' }
    end
  end

  def new
    @schedule_timing = schedule.schedule_timings.build

    respond_with @schedule_timing do |format|
      format.html { render :new, layout: false}
    end
  end

  def edit
    respond_with @schedule_timing
  end

  def update
    if @schedule_timing.update_attributes(permitted_params)
      flash[:notice] = t('.success')
    end

    respond_with @schedule_timing
  end

  def create
    @schedule_timing = schedule.schedule_timings.build(permitted_params)
    if @schedule_timing.save
      flash[:notice] = t('.success')
    end

    respond_with @schedule_timing
  end

  def destroy
    if @schedule_timing.events.any?
      flash[:warning] = t('.cannot_delete_error')
      render nothing: true and return
    else
      flash[:notice] = t('.success') if @schedule_timing.destroy
    end

    respond_with @schedule_timing
  end

  private

  def permitted_params
    params.require(:schedule_timing).permit(:name, :default, options: [:days_before, :value], schedule_timing_options_attributes: [:id, :days_before, :value])
  end

  def find_schedule_timing
    @schedule_timing ||= schedule.schedule_timings.find(params[:id])
  end

  def schedule
    @schedule ||= current_account.schedules.find(params[:schedule_id])
  end

end
