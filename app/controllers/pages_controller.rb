class PagesController < ApplicationController

  def browser_not_supported
    render layout: false
  end
end
