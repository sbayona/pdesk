class NotesController < ApplicationController

  authorize_resource

  before_filter :get_event
  before_filter :get_note, only: [:edit, :update, :destroy]

  def create
    @note = current_account.notes.build(event: @event, description: description_param)
    @note.save
  end

  def edit
  end

  def update
    @note.update_attribute(:description, description_param)
  end

  def destroy
    @note.destroy
  end

  private

    def get_note
      @note = @event.notes.find(params[:id])
    end

    def get_event
      @event = current_account.events.find(params[:event_id])
    end

    def permitted_params
      params.require(:note).permit(:description)
    end

    def description_param
      params[:note][:description]
    end

end
