class PublicController < ApplicationController
  #filters
  around_filter :set_account_time_zone, if: :current_account
  before_filter :authenticate_user!
  before_filter :detect_devise_type
  before_filter :redirect_trial_accounts, if: :needs_subscription?

  protect_from_forgery with: :exception

  layout :layout_by_resource

  protected

  def layout_by_resource
    if devise_controller?
      "devise"
    else
      "application"
    end
  end

  def detect_devise_type
    request.variant = :tablet if request.user_agent =~ /iPad/
    request.variant = :iphone if request.user_agent =~ /iPhone/
  end
end
