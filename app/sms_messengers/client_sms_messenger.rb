class ClientSmsMessenger
  include Concerns::Twilio

  def initialize(event)
    @event = event
    Time.zone = @event.time_zone
    @event.reload # we need to reload the record so its timestamps pick up the time zone
  end

  def request_confirmation(log_id)
    result = send_message to: @event.sms_recipient_number, body: @event.sms_confirmation_request_message, log_id: log_id
    create_event_version('reminder_msg')
    result
  end

  def respond_to_cancelation_request
    return if @event.sms_recipient_number.blank?
    result = send_message to: @event.sms_recipient_number, body: @event.sms_cancellation_message
    create_event_version('cancelation_reply')
    result
  end

  def respond_to_confirmation_request
    return if @event.sms_recipient_number.blank?
    result = send_message to: @event.sms_recipient_number, body: @event.sms_confirmation_message
    create_event_version('confirmation_reply')
    result
  end

  def respond_to_callback_request
    return if @event.sms_recipient_number.blank?
    result = send_message to: @event.sms_recipient_number, body: @event.sms_callback_response_message
    create_event_version('callback_reply')
    result
  end

  def respond_to_unknown_reply
    return if @event.sms_recipient_number.blank?
    result = send_message to: @event.sms_recipient_number, body: I18n.t('models.event.unknown_reply_code')
    create_event_version('unknown_reply')
    result
  end

  private

  def create_event_version(action)
    @event.create_event_notification_log(action, 'sms', 'client', @event.sms_recipient_number)
  end

end
