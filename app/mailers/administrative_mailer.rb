class AdministrativeMailer < ActionMailer::Base

  def signup_notification(account_owner)
    @account_owner = account_owner
    mail to: ENV['NEW_ACCOUNTS_EMAIL'], subject: "New Account at Practicaldesk.com"
  end
end
