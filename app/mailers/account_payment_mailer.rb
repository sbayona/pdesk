class AccountPaymentMailer < ActionMailer::Base

  def charge_succeeded(charge)
    @charge = charge.decorate
    @account = charge.account
    @subscription_plan = @account.subscription_plan
    pdf_options = {
      :pdf => "receipt",
      :template => 'pdf/charge_succeeded.pdf.haml',
      :layout   => 'pdf/body',
      :margin => { :bottom => 40, :top => 30 },
      :footer => {
        :html => { :template => 'layouts/pdf/footer'},
        :line => true}
    }

    attachments["receipt.pdf"] = render_to_string(pdf_options)
    self.instance_variable_set(:@lookup_context, nil)
    mail to: @account.email
  end

  def charge_failed(charge)
    @account = charge.account
    mail to: @account.email
  end

  def trial_expiring_soon(account)
    @account = account.decorate
    @subscription_plan = account.subscription_plan.decorate
    mail to: account.email
  end
end
