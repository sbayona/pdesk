# Notifies interested users of schedule changes

class SubscriberNotificationsMailer < ActionMailer::Base
  include ::EventsHelper

  add_template_helper(Mailers::UserTransactionMailHelper)
  add_template_helper(::EventsHelper)

  layout 'user_emails'

  def notify_created(id)
    with_event(id) do
      mail to: emails_to_notify,
           subject: default_i18n_subject(event_name: event_name)
      create_event_version('creation_notice')      
    end
  end

  def notify_confirmed(id)
    with_event(id) do
      mail to: emails_to_notify,
           subject: default_i18n_subject(event_name: indefinite_articlerize(event_name).capitalize)
      create_event_version('confirmation_notice')
    end
  end

  def notify_canceled(id)
    with_event(id) do
      mail to: emails_to_notify,
           subject: default_i18n_subject(event_name: indefinite_articlerize(event_name).capitalize)
      create_event_version('cancelation_notice')
    end
  end

  def notify_updated(id)
    with_event(id) do
      mail to: emails_to_notify,
           subject: default_i18n_subject(event_name: indefinite_articlerize(event_name).capitalize)
      create_event_version('reschedule_notice')
    end
  end

  def client_callback_request(id)
    with_event(id) do
      mail to: emails_to_notify,
           subject: default_i18n_subject(client_name: client_name)
      create_event_version('callback_notice')
    end
  end

  private

  def with_event(event_id)
    @event = Event.find(event_id)
    return if emails_to_notify.blank?
    Time.zone = @event.time_zone
    @event.reload
    yield
  end

  def create_event_version(action)
    @event.create_event_notification_log(action, 'email', 'subscribers', emails_to_notify)
  end

  def emails_to_notify
    @emails_to_notify ||= @event.notification_subscribers.collect {|a| a.email }
  end

  def event_name
    @event.account_event_name
  end

  def client_name
    @event.account_client_name
  end

end

