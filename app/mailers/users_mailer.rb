class UsersMailer < ActionMailer::Base

  def sign_up_notification(user)
    @user = user
    @account = user.account

    mail(to: @user.email)
  end

end
