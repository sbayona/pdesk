class ClientNotificationsMailer < ActionMailer::Base

  layout 'client_email'

  add_template_helper(Mailers::ClientTransactionMailHelper)

  def reminder_message(id)
    with_event(id) do
      mail to: @event.email_recipient, subject: @event.email_confirmation_request_subject, reply_to: @event.email_reply_to
      create_event_version('reminder_msg')
    end
  end

  def notify_cancellation(id)
    with_event(id) do
      mail to: @event.email_recipient, subject: @event.email_cancellation_notification_subject, reply_to: @event.email_reply_to
      create_event_version('cancelation_notice')
    end
  end

  def notify_reschedule(id)
    with_event(id) do
      mail to: @event.email_recipient, subject: @event.email_reschedule_notification_subject, reply_to: @event.email_reply_to
      create_event_version('reschedule_notice')
    end
  end

  private

  def create_event_version(action)
    @event.create_event_notification_log(action, 'email', 'client', @event.email_recipient)
  end

  def with_event(id)
    @event = Event.find(id)
    return if @event.unavailable_time?
    Time.zone = @event.time_zone
    @event.reload # we need to reload the record so its timestamps pick up the time zone
    return if @event.email_recipient.blank?
    yield
  end


end
