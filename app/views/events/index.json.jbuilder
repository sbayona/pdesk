json.array!(@events) do |event|
  json.id event.id
  json.title event.title
  json.state t(".statuses.#{event.state}")
  json.start event.start_time.to_i
  json.end event.end_time.to_i
  json.allDay event.all_day
  json.className event.state
  json.url event_url(event, format: :json)
  json.annotation event.unavailable_time
  json.editable event_editable?(event)
  json.notes_count event.notes.count
end
