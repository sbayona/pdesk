xml.instruct!
xml.Response do
  xml.Gather(action: voice_update_url(format: 'xml', uid: @event.uuid), :numDigits => 1) do
      xml.Say @event.voice_confirmation_request_message
  end
end
