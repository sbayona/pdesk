json.array!(@schedules) do |schedule|
  json.extract! schedule, :name, :starts_at, :ends_at, :frequency, :length, :business_days
  json.url schedule_url(schedule, format: :json)
end
