xml.instruct!
xml.Response do
  xml.Gather(action: options_demo_url(format: 'xml', uuid: @demo.uuid), :numDigits => 1) do
      xml.Play(demo_greeting_url(host: ENV['HOST'], id: @demo.uuid, format: 'mp3'))
  end
end
