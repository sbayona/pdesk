json.array!(@clients) do |client|
  json.extract! client, :id, :name, :mobile_number, :email
end
