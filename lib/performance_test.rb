class PerformanceTest
  def self.run
    event_records = 10
    %w(events notification_logs delayed_jobs).each do |table_name|
      ActiveRecord::Base.connection.execute("TRUNCATE TABLE #{table_name}")
    end

    account ||= Account.first
    schedule ||= account.schedules.first
    user ||= account.users.first
    script_group ||= schedule.script_groups.first


    event_records.times do
      client = Client.order("random()").first
      start_time = (Time.now + 1.day) + (Time.now - (Time.now + 1.day)) * rand
      event = account.events.create!(start_time: start_time, schedule: schedule, client_attributes: {name: client.name}, user: user, script_group: script_group)
      puts "event id: #{event.id} created!"
    end
    puts "**** finished creating events! ****"

    puts "running jobs..."
    e = EnqueuClientEmails.new
    e.perform
  end
end
