require_relative "../config/environment.rb"

loop do
  sleep(5)
  QueueClientConfirmationRequest.run!
  QueueRescheduleNotification.run!
end
