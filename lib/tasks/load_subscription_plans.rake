namespace :admin do

  desc "loads subscription plans"
  task :load_subscription_plans => :environment do
    subscription_plans = YAML.load(File.read('config/subscription_plans.yml'))

    subscription_plans.each do |subscription|
      SubscriptionPlan.find_or_create_by(code: subscription["code"]) do |sp|
        sp.name = subscription['name']
        sp.price_in_cents = subscription['price_in_cents']
        sp.max_events_per_month = subscription['max_events_per_month']
        sp.max_users = subscription['max_users']
        sp.country_code = subscription['country_code']
        sp.currency = subscription['currency']
        sp.active = subscription['active']
      end
    end
  end
end
