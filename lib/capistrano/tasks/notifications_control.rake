namespace :notifications_control do
  desc "start the notifications control"
  task :start do
    on roles fetch(:notifications_control_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, :'bin/notifications_control', :start
        end
      end
    end
  end

  task :stop do
    on roles fetch(:notifications_control_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, :'bin/notifications_control', :stop, fetch(:notifications_control_flags)
        end
      end
    end
  end

  task :restart do
    on roles fetch(:notifications_control_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, :'bin/notifications_control', :restart, fetch(:notifications_control_flags)
        end
      end
    end
  end

  after 'deploy:updated', 'notifications_control:restart'
  after 'deploy:reverted', 'notifications_control:restart'
end

namespace :load do
  task :defaults do
    set :notifications_control_roles,       ->{ :app }
    set :notifications_control_options,     ->{ {:roles => fetch(:notifications_control_roles)} }
  end
end
