# Practicaldesk

### First Time Setup

```
bundle install
```

Setup vars on:
```
database.yml & application.yml
```

```
rake db:create
```

```
rake db:schema:load
```

```
rake db:seed
```

Run server:
```
rails s
```

Run DelayedJob daemon
```
rake jobs:work
```

Run Notifications script
```
./bin/notifications_control run
```
