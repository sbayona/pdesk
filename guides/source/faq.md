<h2> Frequently Asked Questions</h2>

<h3 class="no-index"> How can I reset my password, or help someone else reset theirs?</h3>

You can send yourself or someone else a password reset from the login page. Click on the "I forgot my username/password" under the login prompt. Enter your email address to send yourself a password reset email, or their email address to have one sent to them.

[Go directly to that page](https://app.practicaldesk.com/users/password/new)


<h3 class="no-index"> How much does Practicaldesk cost? Is there a free plan?</h3>

Practicaldesk starts at $44 a month. You can find [plans and pricing here](https://practicaldesk.com/#pricing).

There's no free plan for Practicaldesk. However, Practicaldesk is free during your trial period.

<h3 class="no-index">How can I pay for Practicaldesk?</h3>

We accept all major credit cards. We collect your payment information once you have an account with us and your trial period has expired.

<h3 class="no-index">How do I access Practicaldesk from my mobile device?</h3>

We have a mobile version of the web app. Simply visit https://app.practicaldesk.com on your mobile devise.


<h3 class="no-index">What happens when my trial ends?</h3>

If your trial ends before you choose a plan, your account will freeze. Your data will remain intact, though inaccessible, until you choose a plan and add a credit card.

<h3 class="no-index">What are the technical requirements for using Practicaldesk?</h3>

Practicaldesk is available anywhere you have an internet connection. There's nothing to install, and all you need is a modern web browser. 

<h3 class="no-index">Can I customize Practicaldesk in any way?</h3>

You can customize all the templates used for communicating with your customers. The SMS messages or emails your customers receives can be completely tailored to your preference. So, for example, you could add directions to your office or special instructions for calling.

