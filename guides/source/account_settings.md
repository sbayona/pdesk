#Account Settings

Once logged in, click on your name located on the top right corner of the app. A menu will appear, select "Account Settings"

![Accessing account setting](images/animations/go_to_settings.gif)

##The Account settings form

Below is an example of the account settings form.

![Account settings form](images/ilustrations/account_settings_form.gif)


<dl>
  <dt>The phone field</dt>
  <dd>Enter the phone number where clients call to schedule appointments.</dd>

  <dt>Did you know you can customize the way you refer to clients?</dt>
  <dd>The "client name" menu gives you options to change the way to call your clients. Most in the health care industry prefer the word "patient", others prefer "client" or "customer".</dd>

  <dt>You can also change how you refer to the events that are scheduled</dt>
  <dd>We generally call them "appointments" but you can call them "meetings", "jobs", "interviews", etc.
  </dd>
  <dt>The time zone menu</dt>
  <dd>It is critical that you select the time zone where you are located. Practicaldesk uses this to calculate when to deliver your client communication and to properly display the times and dates.</dd>
</dl>
