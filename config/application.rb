require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module PracticalDesk
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.autoload_paths += %W(
      #{config.root}/app/presenters
      #{config.root}/app/models/event_logs
      #{config.root}/app/models/demo)

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = "UTC"

    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    config.i18n.default_locale = :en
    config.encoding = "utf-8"
    config.assets.precompile += %w( admin/admin.js public/print-reports.css mobile/application.css )
    config.active_record.raise_in_transactional_callbacks = true

    # config.generators do |g|
    #     g.test_framework :rspec, fixture: true
    #     g.fixture_replacement :factory_girl, dir: "spec/factories"
    # end

    config.action_mailer.default_options = { from: ENV['DEFAULT_SENDER_EMAIL'] }

    config.generators do |g|
     g.view_specs false
     g.helper_specs false
     g.test_framework :rspec
    end
  end
end
