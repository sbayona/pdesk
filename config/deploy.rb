lock '3.10.1'

set :application, 'practicaldesk'
set :repo_url, 'git@github.com:martianinteractive/practicaldesk.git'

set :linked_files, %w{config/application.yml config/database.yml}
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}
set :deploy_to, '/var/www/practicaldesk'
set :bundle_binstubs, false

# the server role where delayed jobs run
set :delayed_job_server_role, :app

# number of workers to start
set :delayed_job_args, "-n 2"

namespace :deploy do

  desc 'Restart delayed job and application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
      invoke "delayed_job:restart"
      invoke "notifications_control:restart"
    end
  end

  after :publishing, :restart
end

namespace :db do
  task :backup_name do
    now = Time.now
    backup_time = [now.year,now.month,now.day,now.hour,now.min,now.sec].join('-')
    set :backup_file, "#{shared_path}/db_backups/#{fetch(:stage).to_s}-snapshot-#{backup_time}.sql"

    on roles(:db) do
      execute "mkdir -p #{shared_path}/db_backups"
    end
  end

  desc "dump db"
  task :dump do
    on roles(:db) do
      invoke 'db:backup_name'
      db_conf_string = capture("cat #{shared_path}/config/database.yml")
      db_info = YAML.load(db_conf_string)[fetch(:stage).to_s]
      dbuser = db_info['username']
      environment_database = db_info['database']
      execute "pg_dump --no-owner -U #{dbuser} #{environment_database} | bzip2 -c > #{fetch(:backup_file)}.bz2"
    end
  end

  task :backup do
    now = Time.now
    backup_time = [now.year,now.month,now.day,now.hour,now.min,now.sec].join('-')

    on roles(:db) do
      invoke 'db:backup_name'
      invoke 'db:dump'
      download! "#{fetch(:backup_file)}.bz2", "/Users/sergiobayona/Documents/backups/practicaldesk/#{fetch(:stage).to_s}-snapshot-#{backup_time}.sql.bz2"
    end
  end


  desc "Sync your production database to your local dev environment"
  task :clone_to_local do
    on roles(:db) do
      invoke 'db:backup_name'
      invoke 'db:dump'
      download! "#{fetch(:backup_file)}.bz2", "/tmp/#{fetch(:application)}.sql.bz2"
      development_info = YAML.load_file("config/database.yml")['development']
      run_str = "PGPASSWORD=#{development_info['password']} bzcat /tmp/#{fetch(:application)}.sql.bz2 | psql -U #{development_info['username']} #{development_info['database']}"
      %x!#{run_str}!
    end
  end
end
SSHKit.config.command_map[:whenever] = "bundle exec whenever"
