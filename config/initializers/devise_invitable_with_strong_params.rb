require 'devise/invitations_controller'

Devise::InvitationsController.class_eval do
  def update_resource_params
    params.require(resource_name).permit(
      :invitation_token,
      :first_name,
      :last_name,
      :email,
      :password,
      :password_confirmation
    )
  end

  def invite_params
    params.require(resource_name).permit(
      :first_name,
      :last_name,
      :email
    )
  end

  def invite_resource
    current_account.users.invite!(invite_params, current_inviter)
  end
end
