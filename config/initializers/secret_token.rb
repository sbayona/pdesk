# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
PracticalDesk::Application.config.secret_key_base = '8458b92563bee94f858056b9f2ff6404432a3016685061791736ab7176ed6cd0739e14690a908491aeead31dc3439668681e9c3deae2d387a45727fa78c82053'
