Rails.application.routes.draw do

  resource :support, only: [:show]
  resource :help, only: [:show]
  resources :demo_greeting, only: [:show]
  resource :demo, only: [:show] do
    member do
      post :options, :reminder
      get :confirmation, :cancelation
    end
  end

  # mount SmsConfirm::Engine => "/r"

  root :to => "dashboard#index"

  get "onboarding.js" => "dashboard#onboarding"

  get "browser_not_supported" => "pages#browser_not_supported"

  devise_for :users, skip: [:sessions, :registrations]
  devise_scope :user do
    post 'sign_in', to: "sessions#create", as: :user_session
    get 'sign_in', to: "sessions#new", as: :new_user_session
    delete 'sign_out', to: "sessions#destroy", as: :destroy_user_session
    post 'register', to: "registrations#create", as: :user_registration
    get 'register', to: "registrations#new", as: :new_user_registration
    get "register/:package_code" => "registrations#new"
  end

  devise_for :admin

  authenticate :admin do
    ActiveAdmin.routes(self)
  end

  post "sms/reply" => "sms#reply", as: :sms_reply
  get "sms/test" => "sms#test"
  post "sms/callback" => "sms#callback", as: :sms_callback
  post "voice/reminder" => "voice#reminder", as: :voice_reminder
  post "voice/update" => "voice#update", as: :voice_update
  get "voice/confirmation" => "voice#confirmation", as: :voice_confirmation
  get "voice/cancelation" => "voice#cancelation", as: :voice_cancelation
  get "voice/callback" => "voice#callback", as: :voice_callback

  resource :subscription_plans, only: [:edit, :update]
  resource :payments, only: [:edit, :update]

  resources :users, only: [:index, :update, :show, :destroy] do
    member do
      put :toggle_status
      post :resend_invitation
    end
  end

  resources :schedules do
    resources :schedule_subscriptions
    resources :schedule_timings
    resources :scripts, only: [:index]
    scope module: 'schedules' do
      resource :options
    end
    namespace :script do
      resources :emails, only: [:update, :edit]
      resources :sms, only: [:update, :edit]
      resources :voice, only: [:update, :edit]
    end
    resource :caller_id do
      member do
        get :verification_check
        get :verified
      end
    end
  end

  post "caller_id_confirmations/:schedule_id" => "caller_id_confirmations#create", as: :caller_id_confirmations

  resources :clients
  get "clients/search/:name" => "clients#search", as: :search
  get "unsubscribe/:token" => "clients#unsubscribe", as: :unsubscribe
  get "clients/contact_fields/:id" => "clients#contact_fields", as: :contact_fields

  resources :events do
    resources :notes, only: [:create, :edit, :update, :destroy]
    member do
      get :confirm
      get :cancel
      get :showed
      get :no_show
    end

    resources :manual_triggers, only: [:index, :update], param: :action
  end

  resources :event_series

  post 'events/new' => 'events#new'

  resource :profile, only: [:edit, :update]
  resource :account, only: [:edit, :update, :destroy]
  resource :expire, only: [:edit, :update]

  resource :billing

  resource :stripe, only: [], format: true, :constraints => { :format => /xml/ } do
    post :charge
  end

  get "dashboard/index"

  get '/secret/exception/raiser' => 'test_exceptions#show'

  get '/reports' => 'reports#index'

end
