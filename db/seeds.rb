require 'faker'
client_count = 1000
user_count = 20
Rake::Task['admin:load_subscription_plans'].invoke
subscription_id = SubscriptionPlan.first.id

account ||= Account.where(name: "Martian Interactive", last_4_digits: "1234", stripe_id: "cus_4hNY8eY84v184v", subscription_plan_id: subscription_id).first_or_create

Admin.create(email: 'sbayona@martianinteractive.com', password: "mjuribe4", password_confirmation: "mjuribe4")

user ||= account.users.create!(first_name: "Sergio", last_name: "Bayona", email: "sbayona@martianinteractive.com", password: "mjuribe4", password_confirmation: "mjuribe4", account_owner: true, role: 'admin')

user_count.times do
  account.users.create! do |a|
    pass = "abcde12345"
    a.first_name = Faker::Name.first_name
    a.last_name = Faker::Name.last_name
    a.email = Faker::Internet.email
    a.password = pass
    a.password_confirmation = pass
  end
end

client_count.times do
  client = account.clients.build do |a|
    a.name = Faker::Name.name
    a.phone_number = Faker::PhoneNumber.phone_number
    a.mobile_number = Faker::PhoneNumber.cell_phone
    a.email = Faker::Internet.email
    a.creator = user
  end
  # Be aware that not all the clients will be saved, because some of them has invalid phone numbers
  client.save
end

puts "done"
