class AddReminderPreferencesToEvents < ActiveRecord::Migration
  def change
    add_column :events, :reminder_preferences, :string, array: true, default: "{}"
  end
end
