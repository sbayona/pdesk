class AddUpdatedViaToEvent < ActiveRecord::Migration
  def change
    add_column :events, :updated_via, :string
  end
end
