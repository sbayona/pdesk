class AddConfirmationStateToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :confirmation_state, :string
    Account.where(confirmation_state: nil).update_all(confirmation_state: Account::ACTIVE)
  end
end
