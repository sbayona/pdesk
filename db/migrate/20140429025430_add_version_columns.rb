class AddVersionColumns < ActiveRecord::Migration
  def change
    add_column :versions, :action, :string
    add_column :versions, :target, :string
    add_column :versions, :via, :string
  end
end
