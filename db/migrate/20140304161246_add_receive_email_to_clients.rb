class AddReceiveEmailToClients < ActiveRecord::Migration
  def change
    add_column :clients, :receive_email, :boolean, default: true
  end
end
