class DefaultToNullOnAccountTrialPeriod < ActiveRecord::Migration
  def change
    change_column :accounts, :trial_period_in_days, :integer, default: nil, null: true
  end
end
