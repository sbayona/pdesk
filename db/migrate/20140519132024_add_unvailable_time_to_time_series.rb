class AddUnvailableTimeToTimeSeries < ActiveRecord::Migration
  def change
    add_column :event_series, :unavailable_time, :boolean, default: false
  end
end
