class CreateBlockedDays < ActiveRecord::Migration
  def change
    create_table :blocked_days do |t|
      t.string :name
      t.date :date
      t.string :country

      t.timestamps
    end
  end
end
