class AddSourceToEventLogs < ActiveRecord::Migration
  def change
    add_column :event_logs, :source, :string
    add_column :event_logs, :user_type, :string
  end
end
