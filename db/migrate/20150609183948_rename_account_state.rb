class RenameAccountState < ActiveRecord::Migration
  def change
    rename_column :accounts, :confirmation_state, :state
  end
end
