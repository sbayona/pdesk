class ChangePapertrailTextColumnsToJson < ActiveRecord::Migration
  def change
    add_column :event_versions, :object_json, :json
    execute "ALTER TABLE event_versions ALTER COLUMN object_json SET DEFAULT '{}'::JSON"
    add_column :event_versions, :object_changes_json, :json
    execute "ALTER TABLE event_versions ALTER COLUMN object_changes_json SET DEFAULT '{}'::JSON"
    EventVersion.all.each do |ev|
      serialized_object = PaperTrail.serializer.load(ev.object.to_s)
      serialized_object_changes = PaperTrail.serializer.load(ev.object_changes.to_s)
      ev.update_attributes(object_changes_json: serialized_object_changes) if serialized_object_changes
      ev.update_attributes(object_json: serialized_object) if serialized_object
    end
    remove_column :event_versions, :object
    remove_column :event_versions, :object_changes
    rename_column :event_versions, :object_json, :object
    rename_column :event_versions, :object_changes_json, :object_changes
  end
end
