class AddCallbackAtToEvent < ActiveRecord::Migration
  def change
    add_column :events, :callback_at, :datetime
  end
end
