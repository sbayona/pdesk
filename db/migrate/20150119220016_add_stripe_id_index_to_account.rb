class AddStripeIdIndexToAccount < ActiveRecord::Migration
  def change
    add_index "accounts", "stripe_id"
  end
end
