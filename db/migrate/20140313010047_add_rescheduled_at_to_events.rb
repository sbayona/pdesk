class AddRescheduledAtToEvents < ActiveRecord::Migration
  def change
    add_column :events, :rescheduled_at, :datetime
  end
end
