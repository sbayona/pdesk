class AllowNullOnDaysBefore < ActiveRecord::Migration
  def change
    change_column :event_timings, :days_before, :integer, null: true
    change_column :schedule_timing_options, :days_before, :integer, null: true
  end
end
