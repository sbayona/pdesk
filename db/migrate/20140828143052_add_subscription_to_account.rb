class AddSubscriptionToAccount < ActiveRecord::Migration
  def change
    add_reference :accounts, :subscription_plan, index: true
  end
end
