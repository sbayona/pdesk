class CreateEventVersions < ActiveRecord::Migration
  def change
    create_table :event_versions do |t|
      t.string   :item_type, :null => false
      t.integer  :item_id,   :null => false
      t.string   :event,     :null => false
      t.string   :whodunnit
      t.text     :object
      t.text     :object_changes
      t.string   :action #custom
      t.string   :target #custom
      t.string   :via    #custom
      t.string   :info   #custom
      t.text   :message  #custom
      t.datetime :created_at
    end
    add_index :event_versions, [:item_type, :item_id]
    add_index :event_versions, [:item_type, :item_id, :event]
  end
end
