class CreateSubscriptionPlans < ActiveRecord::Migration
  def change
    create_table :subscription_plans do |t|
      t.string :code
      t.integer :price_in_cents
      t.string :currency, default: "USD"
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
