class AddSequentialIdToCharges < ActiveRecord::Migration
  def up
    add_column :charges, :sequential_id, :integer
    add_index :charges, :sequential_id
    Charge.all.each do |charge|
      charge.set_sequential_id
      charge.save
    end
  end

  def down
    remove_index :charges, :sequential_id
    remove_column :charges, :sequential_id
  end
end
