class AddVoiceScriptIdToScriptGroups < ActiveRecord::Migration
  def change
    add_column :script_groups, :voice_script_id, :integer
  end
end
