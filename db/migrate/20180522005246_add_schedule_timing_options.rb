class AddScheduleTimingOptions < ActiveRecord::Migration
  def change
    add_column :schedule_timings, :options, :jsonb, default: []
  end
end
