class CreateCharges < ActiveRecord::Migration
  def change
    create_table :charges do |t|
      t.float :amount
      t.string :currency
      t.string :customer
      t.string :description
      t.string :statement
      t.string :card_last4
      t.string :card_brand
      t.integer :card_exp_month
      t.integer :card_exp_year
      t.string :card_country
      t.datetime :charged_at
      t.references :account, index: true

      t.timestamps
    end
  end
end
