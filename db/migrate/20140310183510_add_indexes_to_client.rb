class AddIndexesToClient < ActiveRecord::Migration
  def change
    execute "
    create index on clients using gin(to_tsvector('english', name));
    create index on clients using gin(to_tsvector('english', phone_number));
    create index on clients using gin(to_tsvector('english', mobile_number));
    create index on clients using gin(to_tsvector('english', email));"
  end
end
