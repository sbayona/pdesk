class AddSmsLogTimestamps < ActiveRecord::Migration
  def change
    add_column :event_logs, :sent_at, :timestamp
    add_column :event_logs, :queued_at, :timestamp
    add_column :event_logs, :failed_at, :timestamp
    add_column :event_logs, :undelivered_at, :timestamp
  end
end
