class RenameCancelationFields < ActiveRecord::Migration
  def change
    rename_column :script_emails, :cancelation_subject, :cancellation_subject
    rename_column :script_emails, :cancelation_message, :cancellation_message
  end
end
