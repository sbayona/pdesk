class AddAccountTrialPeriodDays < ActiveRecord::Migration
  def change
    add_column :subscription_plans, :trial_period_days, :integer, default: 0, null: false
  end
end
