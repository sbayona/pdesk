class AddEventTriggerTime < ActiveRecord::Migration
  def change
    add_column :events, :trigger_times, :jsonb, default: []
  end
end
