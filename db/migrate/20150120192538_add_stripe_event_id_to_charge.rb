class AddStripeEventIdToCharge < ActiveRecord::Migration
  def change
    add_column :charges, :stripe_event_id, :string
  end
end
