class UpgradeVoiceScripts < ActiveRecord::Migration
  def change
    add_column :script_voices, :message, :text
    add_column :script_voices, :confirmation_message, :text
    add_column :script_voices, :cancellation_message, :text
    add_column :script_voices, :requested_contact_message, :text
    add_column :script_voices, :created_at, :datetime
    add_column :script_voices, :updated_at, :datetime
  end
end
