class RenameEventState < ActiveRecord::Migration
  def change
    rename_column :events, :confirmation_state, :state
  end
end
