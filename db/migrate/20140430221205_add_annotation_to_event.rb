class AddAnnotationToEvent < ActiveRecord::Migration
  def change
    add_column :events, :unavailable_time, :boolean, default: false
  end
end
