class AddUuidToEvent < ActiveRecord::Migration
  def change
    add_column :events, :uuid, :uuid, :default => "uuid_generate_v4()"
    add_index :events, :uuid, unique: true
  end
end
