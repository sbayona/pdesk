class AddNotesEnabledToSchedules < ActiveRecord::Migration
  def change
    add_column :schedules, :notes_enabled, :boolean, default: true
  end
end
