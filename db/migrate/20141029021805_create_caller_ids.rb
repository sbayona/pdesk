class CreateCallerIds < ActiveRecord::Migration
  def change
    create_table :caller_ids do |t|
      t.string :number
      t.string :friendly_name
      t.datetime :verified_at
      t.boolean :verified
      t.string :twilio_sid
      t.references :schedule, index: true

      t.timestamps
    end
  end
end
