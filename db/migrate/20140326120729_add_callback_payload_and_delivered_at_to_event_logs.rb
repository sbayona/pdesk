class AddCallbackPayloadAndDeliveredAtToEventLogs < ActiveRecord::Migration
  def change
    add_column :event_logs, :callback_payload, :text
    add_column :event_logs, :delivered_at, :datetime
  end
end
