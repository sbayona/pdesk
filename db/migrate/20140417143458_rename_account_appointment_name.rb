class RenameAccountAppointmentName < ActiveRecord::Migration
  def change
    rename_column :accounts, :appointment_name, :event_name
  end
end
