class CreateEventSeries < ActiveRecord::Migration
  def change
    create_table :event_series do |t|
      t.integer :frequency, default: 1
      t.string :period, default: 'monthly'
      t.datetime :start_time
      t.datetime :end_time
      t.datetime :until_date
      t.boolean :all_day, default: false
      t.references :account, index: true
      t.references :schedule, index: true
      t.references :client, index: true
      t.references :user, index: true
      t.references :script_group, index: true
      t.references :schedule_timing, index: true

      t.timestamps
    end
  end
end
