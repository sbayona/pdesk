class RemoveNotificationPreferencesFromScheduleSubscriptons < ActiveRecord::Migration
  def change
    remove_column :schedule_subscriptions, :notification_preferences
  end
end
