class AddAccountIdAndNameIndex < ActiveRecord::Migration
  def change
    add_index :clients, [:account_id, :name], unique: true
  end
end
