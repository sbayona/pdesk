class RemoveTrialPeriodDaysFromSubscriptionPlans < ActiveRecord::Migration
  def change
    remove_column :subscription_plans, :trial_period_days
  end
end
