class AddTextMessageScriptToScriptGroup < ActiveRecord::Migration
  def change
    add_reference :script_groups, :sms_script, index: true
  end
end
