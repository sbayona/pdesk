class AddMaxUsersAndMaxEventsToSubscriptionPlan < ActiveRecord::Migration
  def change
    add_column :subscription_plans, :max_events_per_month, :integer
    add_column :subscription_plans, :max_users, :integer
    add_column :subscription_plans, :country_code, :string, default: 'US'
    add_column :subscription_plans, :name, :string
  end
end
