class AddIndexesToQueries < ActiveRecord::Migration
  def change
    add_index :event_logs, [:event_id, :event_timing_id, :type], unique: true
    add_index :event_logs, :event_id
    add_index :event_logs, :event_timing_id
    add_index :script_groups, :voice_script_id
    
  end
end
