class RenameNotificationLogsTable < ActiveRecord::Migration
  def change
    rename_table :notification_logs, :event_logs
  end
end
