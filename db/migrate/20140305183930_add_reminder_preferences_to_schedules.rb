class AddReminderPreferencesToSchedules < ActiveRecord::Migration
  def change
    add_column :schedules, :reminder_preferences, :string, array: true, default: '{1,2,3}'
  end
end
