class CreateDemos < ActiveRecord::Migration
  def change
    create_table :demos do |t|
      t.string :greeting
      t.references :user, index: true
      t.uuid :uuid, default: "uuid_generate_v4()"

      t.timestamps null: false
    end
  end
end
