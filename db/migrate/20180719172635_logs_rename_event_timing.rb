class LogsRenameEventTiming < ActiveRecord::Migration
  def change
    add_column :event_logs, :trigger_time_id, :integer
    add_index :event_logs, [:event_id, :trigger_time_id, :type]
  end
end
