class ChangeReminderPreferences < ActiveRecord::Migration
  def change
    remove_column :events, :reminder_preferences
    add_column :events, :message_via, :jsonb, default: {}
  end
end
