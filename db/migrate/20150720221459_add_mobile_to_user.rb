class AddMobileToUser < ActiveRecord::Migration
  def change
    add_column :users, :mobile_number, :string, default: "", null: false 
  end
end
