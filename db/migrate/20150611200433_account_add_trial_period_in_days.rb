class AccountAddTrialPeriodInDays < ActiveRecord::Migration
  def change
    add_column :accounts, :trial_period_in_days, :integer, default: 0, null: false
  end
end
