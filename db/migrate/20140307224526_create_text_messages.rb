class CreateTextMessages < ActiveRecord::Migration
  def change
    create_table :script_sms do |t|
      t.text :message
      t.text :confirmation_message
      t.text :cancellation_message
      t.text :requested_contact_message

      t.timestamps
    end
  end
end
