class AddStripeAttributesToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :last_4_digits, :string
    add_column :accounts, :stripe_id, :string
  end
end
