class AddRawDataAndStatusToCharges < ActiveRecord::Migration
  def change
    add_column :charges, :raw_data, :json
    add_column :charges, :status, :string
  end
end
