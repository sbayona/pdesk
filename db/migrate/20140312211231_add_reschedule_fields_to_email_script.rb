class AddRescheduleFieldsToEmailScript < ActiveRecord::Migration
  def change
    add_column :script_emails, :reschedule_subject, :string
    add_column :script_emails, :reschedule_message, :text
  end
end
