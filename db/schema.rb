# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180719172635) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"
  enable_extension "pg_trgm"
  enable_extension "uuid-ossp"

  create_table "accounts", force: :cascade do |t|
    t.string   "name",                   limit: 255,                                        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "time_zone",              limit: 255, default: "Eastern Time (US & Canada)", null: false
    t.string   "client_name",            limit: 255, default: "client",                     null: false
    t.string   "event_name",             limit: 255, default: "appointment",                null: false
    t.text     "address"
    t.string   "last_4_digits",          limit: 255
    t.string   "stripe_id",              limit: 255
    t.integer  "subscription_plan_id"
    t.string   "state"
    t.string   "phone"
    t.integer  "trial_period_in_days"
    t.string   "stripe_subscription_id"
  end

  add_index "accounts", ["stripe_id"], name: "index_accounts_on_stripe_id", using: :btree
  add_index "accounts", ["subscription_plan_id"], name: "index_accounts_on_subscription_plan_id", using: :btree

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.integer  "failed_attempts",                    default: 0,  null: false
    t.string   "unlock_token",           limit: 255
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "blocked_days", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.date     "date"
    t.string   "country",    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "caller_ids", force: :cascade do |t|
    t.string   "number",        limit: 255
    t.string   "friendly_name", limit: 255
    t.datetime "verified_at"
    t.boolean  "verified"
    t.string   "twilio_sid",    limit: 255
    t.integer  "schedule_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "caller_ids", ["schedule_id"], name: "index_caller_ids_on_schedule_id", using: :btree

  create_table "charges", force: :cascade do |t|
    t.float    "amount"
    t.string   "currency",        limit: 255
    t.string   "customer",        limit: 255
    t.string   "description",     limit: 255
    t.string   "statement",       limit: 255
    t.string   "card_last4",      limit: 255
    t.string   "card_brand",      limit: 255
    t.integer  "card_exp_month"
    t.integer  "card_exp_year"
    t.string   "card_country",    limit: 255
    t.datetime "charged_at"
    t.integer  "account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "stripe_event_id", limit: 255
    t.integer  "sequential_id"
    t.json     "raw_data"
    t.string   "status",          limit: 255
  end

  add_index "charges", ["account_id"], name: "index_charges_on_account_id", using: :btree
  add_index "charges", ["sequential_id"], name: "index_charges_on_sequential_id", using: :btree

  create_table "clients", force: :cascade do |t|
    t.string   "name",          limit: 255,                null: false
    t.string   "phone_number",  limit: 255
    t.string   "mobile_number", limit: 255
    t.string   "email",         limit: 255
    t.integer  "account_id"
    t.integer  "creator_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "scheduled_at"
    t.string   "token",         limit: 255
    t.boolean  "receive_email",             default: true
  end

  add_index "clients", ["account_id", "name"], name: "index_clients_on_account_id_and_name", unique: true, using: :btree
  add_index "clients", ["account_id"], name: "index_clients_on_account_id", using: :btree
  add_index "clients", ["creator_id"], name: "index_clients_on_creator_id", using: :btree
  add_index "clients", ["name"], name: "index_clients_on_name", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",               default: 0, null: false
    t.integer  "attempts",               default: 0, null: false
    t.text     "handler",                            null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "demos", force: :cascade do |t|
    t.string   "greeting"
    t.integer  "user_id"
    t.uuid     "uuid",       default: "uuid_generate_v4()"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "demos", ["user_id"], name: "index_demos_on_user_id", using: :btree

  create_table "event_logs", force: :cascade do |t|
    t.text     "data"
    t.text     "error"
    t.datetime "completed_at"
    t.integer  "event_id"
    t.string   "type",             limit: 255
    t.integer  "event_timing_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "source",           limit: 255
    t.string   "user_type",        limit: 255
    t.text     "callback_payload"
    t.datetime "delivered_at"
    t.datetime "sent_at"
    t.datetime "queued_at"
    t.datetime "failed_at"
    t.datetime "undelivered_at"
    t.integer  "trigger_time_id"
  end

  add_index "event_logs", ["event_id", "event_timing_id", "type"], name: "index_event_logs_on_event_id_and_event_timing_id_and_type", unique: true, using: :btree
  add_index "event_logs", ["event_id", "trigger_time_id", "type"], name: "index_event_logs_on_event_id_and_trigger_time_id_and_type", using: :btree
  add_index "event_logs", ["event_id", "type"], name: "index_event_logs_on_event_id_and_type", using: :btree
  add_index "event_logs", ["event_id"], name: "index_event_logs_on_event_id", using: :btree
  add_index "event_logs", ["event_timing_id"], name: "index_event_logs_on_event_timing_id", using: :btree

  create_table "event_series", force: :cascade do |t|
    t.integer  "frequency",                      default: 1
    t.string   "period",             limit: 255, default: "monthly"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "until_date"
    t.boolean  "all_day",                        default: false
    t.integer  "account_id"
    t.integer  "schedule_id"
    t.integer  "client_id"
    t.integer  "user_id"
    t.integer  "script_group_id"
    t.integer  "schedule_timing_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "unavailable_time",               default: false
  end

  add_index "event_series", ["account_id"], name: "index_event_series_on_account_id", using: :btree
  add_index "event_series", ["client_id"], name: "index_event_series_on_client_id", using: :btree
  add_index "event_series", ["schedule_id"], name: "index_event_series_on_schedule_id", using: :btree
  add_index "event_series", ["schedule_timing_id"], name: "index_event_series_on_schedule_timing_id", using: :btree
  add_index "event_series", ["script_group_id"], name: "index_event_series_on_script_group_id", using: :btree
  add_index "event_series", ["user_id"], name: "index_event_series_on_user_id", using: :btree

  create_table "event_timings", force: :cascade do |t|
    t.integer  "days_before"
    t.integer  "offset_in_seconds", null: false
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "event_timings", ["event_id"], name: "index_event_timings_on_event_id", using: :btree

  create_table "event_versions", force: :cascade do |t|
    t.string   "item_type",                   null: false
    t.integer  "item_id",                     null: false
    t.string   "event",                       null: false
    t.string   "whodunnit"
    t.string   "action"
    t.string   "target"
    t.string   "via"
    t.string   "info"
    t.text     "message"
    t.datetime "created_at"
    t.json     "object",         default: {}
    t.json     "object_changes", default: {}
  end

  add_index "event_versions", ["item_type", "item_id", "event"], name: "index_event_versions_on_item_type_and_item_id_and_event", using: :btree
  add_index "event_versions", ["item_type", "item_id"], name: "index_event_versions_on_item_type_and_item_id", using: :btree

  create_table "events", force: :cascade do |t|
    t.string   "title",                limit: 255
    t.datetime "start_time"
    t.datetime "end_time"
    t.boolean  "all_day",                          default: false
    t.text     "description"
    t.integer  "event_series_id"
    t.integer  "schedule_id"
    t.integer  "client_id"
    t.integer  "user_id"
    t.integer  "account_id"
    t.integer  "script_group_id"
    t.integer  "schedule_timing_id"
    t.string   "state",                limit: 255
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
    t.datetime "confirmed_at"
    t.datetime "canceled_at"
    t.datetime "showed_at"
    t.datetime "no_showed_at"
    t.uuid     "uuid",                             default: "uuid_generate_v4()"
    t.string   "reminder_preferences",             default: [],                   array: true
    t.datetime "rescheduled_at"
    t.datetime "callback_at"
    t.string   "updated_via",          limit: 255
    t.boolean  "unavailable_time",                 default: false
    t.string   "short_code"
    t.jsonb    "trigger_times",                    default: []
  end

  add_index "events", ["account_id"], name: "index_events_on_account_id", using: :btree
  add_index "events", ["client_id"], name: "index_events_on_client_id", using: :btree
  add_index "events", ["event_series_id"], name: "index_events_on_event_series_id", using: :btree
  add_index "events", ["schedule_id"], name: "index_events_on_schedule_id", using: :btree
  add_index "events", ["schedule_timing_id"], name: "index_events_on_schedule_timing_id", using: :btree
  add_index "events", ["script_group_id"], name: "index_events_on_script_group_id", using: :btree
  add_index "events", ["short_code"], name: "index_events_on_short_code", unique: true, using: :btree
  add_index "events", ["start_time"], name: "index_events_on_start_time", using: :btree
  add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree
  add_index "events", ["uuid"], name: "index_events_on_uuid", unique: true, using: :btree

  create_table "notes", force: :cascade do |t|
    t.string   "description"
    t.integer  "account_id"
    t.integer  "event_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "notes", ["account_id"], name: "index_notes_on_account_id", using: :btree
  add_index "notes", ["event_id"], name: "index_notes_on_event_id", using: :btree

  create_table "reminder_timing_options", force: :cascade do |t|
    t.integer "reminder_timing_id"
    t.string  "value",              limit: 255,              null: false
    t.integer "days_before",                                 null: false
    t.string  "timing_type",        limit: 255, default: "", null: false
  end

  add_index "reminder_timing_options", ["reminder_timing_id"], name: "index_reminder_timing_options_on_reminder_timing_id", using: :btree

  create_table "reminder_timings", force: :cascade do |t|
    t.string   "name",        limit: 255,                 null: false
    t.integer  "schedule_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "default",                 default: false
  end

  add_index "reminder_timings", ["schedule_id"], name: "index_reminder_timings_on_schedule_id", using: :btree

  create_table "schedule_notifications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "schedule_id"
    t.integer  "notification_preferences"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "schedule_notifications", ["schedule_id"], name: "index_schedule_notifications_on_schedule_id", using: :btree
  add_index "schedule_notifications", ["user_id"], name: "index_schedule_notifications_on_user_id", using: :btree

  create_table "schedule_subscriptions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "schedule_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "schedule_subscriptions", ["schedule_id"], name: "index_schedule_subscriptions_on_schedule_id", using: :btree
  add_index "schedule_subscriptions", ["user_id"], name: "index_schedule_subscriptions_on_user_id", using: :btree

  create_table "schedule_timing_options", force: :cascade do |t|
    t.integer "schedule_timing_id"
    t.string  "value",              limit: 255,              null: false
    t.integer "days_before"
    t.string  "timing_type",        limit: 255, default: "", null: false
  end

  add_index "schedule_timing_options", ["schedule_timing_id"], name: "index_schedule_timing_options_on_schedule_timing_id", using: :btree

  create_table "schedule_timings", force: :cascade do |t|
    t.string   "name",        limit: 255,                 null: false
    t.integer  "schedule_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "default",                 default: false
    t.jsonb    "options",                 default: []
  end

  add_index "schedule_timings", ["schedule_id"], name: "index_schedule_timings_on_schedule_id", using: :btree

  create_table "schedules", force: :cascade do |t|
    t.string   "name",                 limit: 255,                                     null: false
    t.integer  "starts_at",                        default: 8
    t.integer  "ends_at",                          default: 18
    t.integer  "frequency",                        default: 15
    t.integer  "length",                           default: 60
    t.text     "business_days",                    default: ["1", "2", "3", "4", "5"], null: false, array: true
    t.integer  "account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "reminder_preferences",             default: ["1", "2", "3"],                        array: true
    t.boolean  "notes_enabled",                    default: true
  end

  add_index "schedules", ["account_id"], name: "index_schedules_on_account_id", using: :btree

  create_table "script_emails", force: :cascade do |t|
    t.string   "from_name",            limit: 255
    t.string   "from_email",           limit: 255
    t.string   "reply_to_name",        limit: 255
    t.string   "reply_to_email",       limit: 255
    t.string   "subject",              limit: 255
    t.text     "message"
    t.string   "cancellation_subject", limit: 255
    t.text     "cancellation_message"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "reschedule_subject",   limit: 255
    t.text     "reschedule_message"
  end

  add_index "script_emails", ["user_id"], name: "index_script_emails_on_user_id", using: :btree

  create_table "script_groups", force: :cascade do |t|
    t.string   "name",            limit: 255, null: false
    t.integer  "email_script_id"
    t.integer  "schedule_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sms_script_id"
    t.integer  "voice_script_id"
  end

  add_index "script_groups", ["email_script_id"], name: "index_script_groups_on_email_script_id", using: :btree
  add_index "script_groups", ["schedule_id"], name: "index_script_groups_on_schedule_id", using: :btree
  add_index "script_groups", ["sms_script_id"], name: "index_script_groups_on_sms_script_id", using: :btree
  add_index "script_groups", ["voice_script_id"], name: "index_script_groups_on_voice_script_id", using: :btree

  create_table "script_sms", force: :cascade do |t|
    t.text     "message"
    t.text     "confirmation_message"
    t.text     "cancellation_message"
    t.text     "requested_contact_message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "script_voices", force: :cascade do |t|
    t.integer  "option",                    default: 0
    t.text     "message"
    t.text     "confirmation_message"
    t.text     "cancellation_message"
    t.text     "requested_contact_message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subscription_plans", force: :cascade do |t|
    t.string   "code",                 limit: 255
    t.integer  "price_in_cents"
    t.string   "currency",             limit: 255, default: "USD"
    t.boolean  "active",                           default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "max_events_per_month"
    t.integer  "max_users"
    t.string   "country_code",         limit: 255, default: "US"
    t.string   "name",                 limit: 255
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name",             limit: 255, default: "",    null: false
    t.string   "last_name",              limit: 255, default: "",    null: false
    t.integer  "account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  limit: 255, default: "",    null: false
    t.string   "encrypted_password",     limit: 255, default: ""
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.integer  "failed_attempts",                    default: 0
    t.string   "unlock_token",           limit: 255
    t.datetime "locked_at"
    t.boolean  "account_owner",                      default: false
    t.string   "invitation_token",       limit: 255
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type",        limit: 255
    t.datetime "deleted_at"
    t.string   "role",                   limit: 255
    t.integer  "invitations_count",                  default: 0
    t.string   "type",                   limit: 255
    t.string   "mobile_number",                      default: "",    null: false
  end

  add_index "users", ["account_id"], name: "index_users_on_account_id", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "version_associations", force: :cascade do |t|
    t.integer "version_id"
    t.string  "foreign_key_name", null: false
    t.integer "foreign_key_id"
  end

  add_index "version_associations", ["foreign_key_name", "foreign_key_id"], name: "index_version_associations_on_foreign_key", using: :btree
  add_index "version_associations", ["version_id"], name: "index_version_associations_on_version_id", using: :btree

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",      limit: 255, null: false
    t.integer  "item_id",                    null: false
    t.string   "event",          limit: 255, null: false
    t.string   "whodunnit",      limit: 255
    t.text     "object"
    t.datetime "created_at"
    t.text     "object_changes"
    t.string   "action",         limit: 255
    t.string   "target",         limit: 255
    t.string   "via",            limit: 255
    t.integer  "transaction_id"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
  add_index "versions", ["transaction_id"], name: "index_versions_on_transaction_id", using: :btree

  add_foreign_key "notes", "accounts"
  add_foreign_key "notes", "events"
end
