require 'spec_helper'

shared_examples "an invalid client request" do
  before { event.reload } # reload to pick the uuid attribute

  it "redirects to login page" do
    expect(req).to redirect_to new_user_session_path
  end

  it "does not change the confirmation state" do
    expect { req }.to_not change { event.reload.state }
  end
end

describe EventsController,'client actions' do

  let!(:event) { create(:event, :with_associations) }
  let(:client) { event.client }

  describe "GET confirm" do

    describe "with valid params" do
      before { event.reload }

      let(:req) {  get :confirm, id: event.uuid, t: event.client.token }

      it "updates the cofirmation state to confirmed" do
        expect { req }.to change { event.reload.state }.from('pending').to('confirmed')
      end

      it "renders the confirmed template" do
        expect(req).to render_template 'confirm'
      end

      it "sets the updater to the client" do
        req
        expect(assigns(:unscoped_event).reload.updater.name).to eq client.name
      end

      it "sets the updated mechanism" do
        expect do
          req
        end.to change { event.reload.updated_via }.from(nil).to(Event::UPDATE_VIA[:EMAIL])
      end
    end

    describe "with invalid params" do

      it_behaves_like "an invalid client request" do
        let(:req) { get :confirm, id: 'c2d29867-3d0b-d497-9191-18a9d8ee7830', t: event.client.token }
      end

      it_behaves_like "an invalid client request" do
        let(:req) { get :confirm, id: event.uuid, t: 'invalidtoken' }
      end

      it_behaves_like "an invalid client request" do
        let(:req) { get :confirm, id: event.uuid, t: '' }
      end

      it_behaves_like "an invalid client request" do
        let(:req) { get :confirm, id: '', t: event.client.token }
      end
    end
  end

  describe "GET cancel" do
    describe "with valid params" do
      before { event.reload } # reload to pick the uuid attribute

      let(:req) {  get :cancel, id: event.uuid, t: event.client.token }

      it "updates the cofirmation state to confirmed" do
        expect { req }.to change { event.reload.state }.from('pending').to('canceled')
      end

      it " renders the confirmed template" do
        expect(req).to render_template 'cancel'
      end

      it "sets the updater to the client" do
        req
        expect(assigns(:unscoped_event).reload.updater.name).to eq client.name
      end
    end

    describe "with invalid params" do
      it_behaves_like "an invalid client request" do
        let(:req) { get :cancel, id: 'c2d29867-3d0b-d497-9191-18a9d8ee7830', t: event.client.token }
      end

      it_behaves_like "an invalid client request" do
        let(:req) { get :cancel, id: event.uuid, t: 'invalidtoken' }
      end

      it_behaves_like "an invalid client request" do
        let(:req) { get :cancel, id: event.uuid, t: '' }
      end

      it_behaves_like "an invalid client request" do
        let(:req) { get :cancel, id: '', t: event.client.token }
      end
    end
  end

end
