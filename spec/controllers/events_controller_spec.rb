require 'spec_helper'

describe EventsController do

  let!(:account) { create :account, :with_account_owner }
  let!(:owner) { account.account_owner }
  let!(:client) { create(:client, creator: owner, account: account)}

  let!(:dr_lee_schedule) { create :schedule, :with_events, number_of_events: 2, account: account }
  let(:event) { dr_lee_schedule.events.first }

  let(:valid_attributes) { {client_attributes: { name: "Peter Schey", mobile_number: "404-555-5555"}, start_time: Time.now.to_s, schedule_id: dr_lee_schedule.id } }

  before { sign_in owner }

  describe "GET index" do
    let!(:dr_james_schedule) { create :schedule, :with_events, number_of_events: 3, account: account }

    it "returns all account events" do
      get :index, format: :json
      expect(assigns(:events).count).to eq 5
    end

    it "Dr. Lee's schedule has 2 appointments" do
      get :index, schedule_id: dr_lee_schedule.id, format: :json
      expect(assigns(:events).count).to eq 2
    end

    it "Dr. James' schedule has 3 appointments" do
      get :index, schedule_id: dr_james_schedule.id, format: :json
      expect(assigns(:events).count).to eq 3
    end
  end

  describe "GET new" do
    context "with a valid schedule param" do
      before { xhr :get, :new, event: {schedule: dr_lee_schedule.id} }

      it "assigns a new event as @event" do
        expect(assigns(:event)).to be_a_new(Event)
      end

      it "render the new template" do
        expect(response).to render_template('events/new')
      end
    end

    context "with an invalid schedule param" do
      before { xhr :get, :new, event: {schedule: 9000000} }

      it "does not create an event instance variable" do
        expect(assigns(:event)).to be_nil
      end

      it "render the 'please_select_schedule' template" do
        expect(response).to render_template('events/_please_select_schedule')
      end
    end
  end

  describe "GET show" do
    before { get :show, id: event.id }

    it { expect(assigns(:event)).to be_present }

    it { expect(response).to render_template('events/show') }
  end

  describe "GET edit" do
    it "assigns the requested event as @event" do
      xhr :get, :edit, {:id => event.to_param}
      expect(assigns(:event)).to eq(event)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Event" do
        expect {
          xhr :post, :create, {:event => valid_attributes}
        }.to change(Event, :count).by(1)
      end

      it "assigns a newly created event as @event" do
        xhr :post, :create, {:event => valid_attributes}
        expect(assigns(:event)).to be_a(Event)
        expect(assigns(:event)).to be_persisted
      end

      it "redirects to the created event" do
        xhr :post, :create, {:event => valid_attributes}
        expect(response).to render_template('events/create')
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved event as @event" do
        # Trigger the behavior that occurs when invalid params are submitted
        allow_any_instance_of(Event).to receive(:save).and_return(false)
        allow_any_instance_of(Event).to receive(:errors).and_return(['error'])

        xhr :post, :create, {:event => { "title" => "invalid value" }}
        expect(assigns(:event)).to be_a_new(Event)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        allow_any_instance_of(Event).to receive(:save).and_return(false)
        allow_any_instance_of(Event).to receive(:errors).and_return(['error'])
        xhr :post, :create, {:event => { "title" => "invalid value" }}
        expect(response).to render_template("create")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested event" do
        expect_any_instance_of(Event).to receive(:update_attributes)
        xhr :put, :update, {:id => event.to_param, :event => valid_attributes}
      end

      it "assigns the requested event as @event" do
        xhr :put, :update, {:id => event.to_param, :event => valid_attributes}
        expect(assigns(:event)).to eq(event)
      end

      it "redirects to the event" do
        xhr :put, :update, {:id => event.to_param, :event => valid_attributes}
        expect(response).to render_template('update')
      end
    end

    describe "with invalid params" do
      it "assigns the event as @event" do
        allow_any_instance_of(Event).to receive(:save).and_return(false)
        allow_any_instance_of(Event).to receive(:errors).and_return(['error'])
        xhr :put, :update, {:id => event.to_param, :event => { "title" => "invalid value" }}
        expect(assigns(:event)).to eq(event)
      end

      it "re-renders the 'update' template" do
        allow_any_instance_of(Event).to receive(:save).and_return(false)
        allow_any_instance_of(Event).to receive(:errors).and_return(['error'])
        xhr :put, :update, {:id => event.to_param, :event => { "title" => "invalid value" }}
        expect(response).to render_template("update")
      end
    end
  end

  describe "DELETE destroy" do
    it "soft-deletes the event" do
      expect_any_instance_of(Event).to receive(:soft_delete!)
      xhr :delete, :destroy, {:id => event.to_param}
    end

    it "reduces the event count" do
      expect {
        xhr :delete, :destroy, {:id => event.to_param}
      }.to change(Event, :count).by(-1)
    end

    it "redirects to the events list" do
      xhr :delete, :destroy, {:id => event.to_param}
      expect(response).to render_template("update_event")
    end
  end

  describe "PUT confirm" do
    it "confirms the event" do
      expect do
        xhr :get, :confirm, { id: event.to_param }
      end.to change {event.reload.state }.from('pending').to('confirmed')
    end

    it "sets the updated mechanism" do
      expect do
        xhr :get, :confirm, { id: event.to_param }
      end.to change {event.reload.updated_via }.from(nil).to(Event::UPDATE_VIA[:WEB])
    end

    it "renders the right template" do
      xhr :get, :confirm, { id: event.to_param }
      expect(response).to render_template("update_event")
    end
  end

  describe "PUT cancel" do
    it "cancels the event" do
      expect do
        xhr :get, :cancel, { id: event.to_param }
      end.to change {event.reload.state }.from('pending').to('canceled')
    end

    it "renders the right template" do
      xhr :get, :cancel, { id: event.to_param }
      expect(response).to render_template("update_event")
    end
  end

  describe "PUT no_show" do
    it "no_show the event" do
      expect do
        xhr :get, :no_show, { id: event.to_param }
      end.to change {event.reload.state }.from('pending').to('no_show')
    end

    it "renders the right template" do
      xhr :get, :no_show, { id: event.to_param }
      expect(response).to render_template("update_event")
    end
  end

  describe "PUT showed" do
    it "no_show the event" do
      expect do
        xhr :get, :showed, { id: event.to_param }
      end.to change {event.reload.state }.from('pending').to('show')
    end

    it "renders the right template" do
      xhr :get, :showed, { id: event.to_param }
      expect(response).to render_template("update_event")
    end
  end

end
