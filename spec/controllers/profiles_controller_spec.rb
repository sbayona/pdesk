require 'spec_helper'

describe ProfilesController do

  let!(:user) { create(:user, :with_account) }

  before { sign_in user }

  describe "GET edit" do
    it "should render" do
      get :edit
      expect(response).to render_template("edit")
    end
  end

  describe "changing password" do
    it "should update the current user" do
      put :update, {user: {password: 'qwer1234', password_confirmation: 'qwer1234'}}
      user.valid_password?('qwer1234')
    end

    it "should require a password confirmation" do
      put :update, {user: {password: 'qwer1234', password_confirmation: ''}}
      expect(assigns(:current_user).errors.full_messages).to include(%(Password confirmation doesn't match Password))
      expect(response).to render_template('edit')
    end
  end

  describe "PUT update" do
    describe "changing name" do
      before { put :update, {user: {first_name: 'John', last_name: 'Doe'}} }

      it "should update the current user" do
        expect(user.reload.name).to eq('John Doe')
      end

      it "renders the form" do
        expect(response).to redirect_to action: :edit
      end

      it "should not change the password" do
        user.valid_password?('abcd1234')
      end
    end
  end

end
