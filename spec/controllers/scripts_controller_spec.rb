require 'spec_helper'

describe ScriptsController do

  let(:admin) { create(:admin, :with_account) }
  let(:account) { admin.account }
  let!(:schedule) { create(:schedule, account: account) }
  before { sign_in admin }

  describe "GET 'index'" do
    it "returns http success" do
      get :index, schedule_id: schedule.id
      expect(response).to be_success
    end
  end

end
