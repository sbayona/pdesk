require 'spec_helper'

RSpec.describe DemosController, type: :controller do

  let(:admin) { create(:admin, :with_account, :with_demos) }

  before do
    sign_in admin
  end

  it "" do
    get :reminder, format: :xml, :"To" => admin.mobile_number
  end

end
