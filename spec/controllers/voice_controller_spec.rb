require 'spec_helper'

describe VoiceController do
  render_views 
  let!(:event) { create(:event, :with_associations) }
  let!(:account) { event.account }
  let(:base_params) { {"format" => :xml, "uid" => event.uuid} }

  before { event.reload } # we need to reload to make the uuid available

  describe "GET reminder" do
    render_views

    describe "with valid params" do
      before { post :reminder, base_params }

      it "creates a new Client" do
        expect(response).to render_template('reminder')
      end

      it "renders the voice text" do
        expect(response.body).to contain_xml(<<-XML)
<?xml version="1.0" encoding="UTF-8"?>
<Response>
 <Gather action="http://test.host/voice/update.xml?uid=#{event.uuid}" numDigits="1">
   <Say>Hello. This is #{account.name}. 
We want to remind you about your appointment on #{event.start_time.to_s(:event_date)} at #{event.start_time.to_s(:event_time)}.
Please press 1 to confirm. 5 to cancel. Or 9 for us to call you back.
</Say>
 </Gather>
</Response>
        XML
      end
    end
  end

  describe "updating" do
    let(:make_request) { post :update, params }

    describe "confirmation" do
      let(:params) { base_params.merge("Digits" => Event::CONFIRMATION_CODE[:CONFIRMED]) }

      it "redirects to confirmation" do
        make_request
        expect(response).to redirect_to(voice_confirmation_path(uid: event.uuid, format: :xml))
      end
    end

    describe "cancelation" do
      let(:params) { base_params.merge("Digits" => Event::CONFIRMATION_CODE[:CANCELED]) }

      it "redirects to cancel" do
        make_request
        expect(response).to redirect_to(voice_cancelation_path(uid: event.uuid, format: :xml))
      end
    end

    describe "callback" do
      let(:params) { base_params.merge("Digits" => Event::CONFIRMATION_CODE[:CALLBACK]) }

      it "redirects to cancel" do
        make_request
        expect(response).to redirect_to(voice_callback_path(uid: event.uuid, format: :xml))
      end
    end
  end

  describe "confirmation" do
    let(:make_request) { get :confirmation, base_params }

    it { expect(make_request).to render_template('confirmation') }

    it { expect {make_request}.to change { event.reload.state }.from('pending').to('confirmed') }

    # it "reponds with the right text" do
    #   make_request
    #   expect(response.body).to match /Thank you! We look forward to serving you. Good bye./
    # end
  end

  describe "cancelation" do
    let(:make_request) { get :cancelation, base_params }

    it { expect(make_request).to render_template('cancelation') }

    it { expect {make_request}.to change { event.reload.state }.from('pending').to('canceled') }

    # it "reponds with the right text" do
    #   make_request
    #   expect(response.body).to match /We've received your cancelation request. Thank you. Good bye/
    # end
  end

  describe "callback" do
    let(:make_request) { get :callback, base_params }

    it { expect(make_request).to render_template('callback') }

    it { expect {make_request }.to change { event.reload.state }.from('pending').to('callback') }

    # it "reponds with the right text" do
    #   make_request
    #   expect(response.body).to match /Thank you. We will be in touch with you soon. Good bye./
    # end
  end
end
