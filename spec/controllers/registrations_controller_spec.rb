require 'spec_helper'

describe RegistrationsController do

  let(:subscription_plan) { create(:subscription_plan) }
  let(:account_attributes) { { name: "ACME, Inc", subscription_plan_id: subscription_plan.id, stripe_token: "12345", last_4_digits: "1234"} }
  let(:valid_attributes) { {account_attributes: account_attributes, full_name: "James Doe", email: "test@test.com", password: "abcd1234g"} }

  before { @request.env["devise.mapping"] = Devise.mappings[:user] }

  describe "POST create" do

    it "creates a user record" do
      expect {
        post :create, {user: valid_attributes}
      }.to change { AccountOwner.count }.by(1)
    end

    it "creates an account record" do
      expect {
        post :create, { user: valid_attributes }
      }.to change { Account.count }.by(1)
    end

    it "creates an admin" do
      post :create, { :user => valid_attributes }
      expect(AccountOwner.last.admin?).to eq true
    end

    it "creates a default schedule" do
      expect {
        post :create, { user: valid_attributes }
      }.to change { Schedule.count }.by(1)
    end

    it "redirects" do
      post :create, { user: valid_attributes }
      expect(response).to redirect_to root_path
    end

    it "delivers administrative email" do
      expect(AdministrativeMailer).to receive(:signup_notification).and_return(double("Mailer", deliver_now: true))
      post :create, { user: valid_attributes }
    end

    it "delivers the User's signup welcome email" do
      expect(UsersMailer).to receive(:sign_up_notification).and_return(double('Mailer', deliver_now: true))
      post :create, { user: valid_attributes }
    end

    it "re-renders the new template when there's an error" do
      post :create, { user: {email: "test@test.com"} }
      expect(response).to render_template :new
    end
  end

  describe "get NEW" do
    it "creates an instance of account owner" do
      get :new
      expect(assigns(:account_owner)).to be_an_instance_of(AccountOwner)
    end

    it "account owner build an account" do
      get :new
      expect(assigns(:account_owner).account).to be_an_instance_of(Account)
    end
  end
end
