require 'spec_helper'

describe UsersController do
  let(:account) {create(:account) }
  let(:admin) { create(:admin, account: account) }
  let(:user) { create(:user, account: account) }

  before { sign_in admin }

  describe "GET index" do
    it "assigns all users as @users" do
      get :index
      expect(assigns(:users)).to include(admin)
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
  end

  describe "GET show" do
    it "assigns the requested admin as @admin" do
      get :show, { id: admin.to_param }
      expect(assigns(:user)).to eq(admin)
    end

    it "renders the show template" do
      get :show, { id: admin.to_param }
      expect(response).to render_template("show")
    end
  end

  describe "POST #resend_invitation" do

    context "users who are actively invited" do
      let!(:invited_user) { create(:user, invited_by: admin, invitation_token: 'abcd1234', email: "peter555@test.com", account: account, invitation_sent_at: (DateTime.now - 2.hours), invitation_created_at: (DateTime.now - 2.hours))}

      it "sends the invitation email" do
        expect do
          post :resend_invitation, {id: invited_user.to_param }
        end.to change { ActionMailer::Base.deliveries.count }
      end

      it "sets a flash message" do
        post :resend_invitation, {id: invited_user.to_param }
        expect(flash[:alert]).to eq("We've resent the invitation email!")
      end
    end

    context "users who have already joined" do
      let!(:invited_but_joined_user) { create(:user, invited_by: admin, email: "tome@test.com", account: account)}

      it "does not send the invitation email" do
        expect do
          post :resend_invitation, {id: invited_but_joined_user.to_param }
        end.to raise_error ActiveRecord::RecordNotFound
      end

    end
  end

  describe "DELETE #destroy" do
    let!(:user_for_deletion) { create(:user, email: "tom@test.com", account: account) }

    it "soft-deletes the admin" do
      expect do
        delete :destroy, {id: user_for_deletion.to_param }
      end.to change { User.count }.by(-1)
    end
  end

  describe "UPDATE" do
    it "updates the role" do
      expect do
        put :update, {id: user.id, user: {role: 'admin'} }
      end.to change{ user.reload.role }.from(nil).to('admin')
    end

    it "redirects to the show action" do
      put :update, {id: user.id, user: {role: 'admin'}}
      expect(response).to redirect_to(action: :show)
    end
  end
end

describe UsersController, "as regular user" do
  let(:admin) { create(:admin, :with_account) }
  let(:another_account) { create(:account) }
  let(:user) { create(:user, account: another_account) }

  before { sign_in user }

  context "as a regular user, updating another user" do
    it "does raises an exception" do
      expect do
        put :update, {id: admin.id, user: {role: 'regular'} }
      end.to raise_error(CanCan::AccessDenied)
    end
  end
end
