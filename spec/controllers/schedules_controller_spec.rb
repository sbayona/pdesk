require 'spec_helper'

describe SchedulesController do

  let(:admin) { create(:admin, :with_account) }

  before { sign_in admin }

  let!(:schedule) { create(:schedule, account: admin.account) }

  let(:valid_attributes) { { "name" => "my schedule", "starts_at" => 1, "ends_at" => 2, "frequency" => 5, "length" =>11, "business_days" => ["1", "2"] } }


  describe "GET index" do
    it "assigns all schedules as @schedules" do
      get :index
      expect(assigns(:schedules)).to include(schedule)
    end
  end

  describe "GET new" do
    it "assigns a new schedule as @schedule" do
      get :new
      expect(assigns(:schedule)).to be_a_new(Schedule)
    end
  end

  describe "GET edit" do
    it "assigns the requested schedule as @schedule" do
      get :edit, {:id => schedule.to_param}
      expect(assigns(:schedule)).to eq(schedule)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Schedule" do
        expect {
          post :create, {:schedule => valid_attributes}
        }.to change(Schedule, :count).by(1)
      end

      it "assigns a newly created schedule as @schedule" do
        post :create, {:schedule => valid_attributes}
        expect(assigns(:schedule)).to be_a(Schedule)
        expect(assigns(:schedule)).to be_persisted
      end

      it "redirects to the created schedule" do
        post :create, {:schedule => valid_attributes}
        schedule = Schedule.last
        expect(response).to redirect_to(edit_schedule_path(schedule))
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved schedule as @schedule" do
        # Trigger the behavior that occurs when invalid params are submitted
        allow_any_instance_of(Schedule).to receive(:save).and_return(false)
        allow_any_instance_of(Schedule).to receive(:errors).and_return(['error'])
        post :create, {:schedule => { "name" => "invalid value" }}
        expect(assigns(:schedule)).to be_a_new(Schedule)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        allow_any_instance_of(Schedule).to receive(:save).and_return(false)
        allow_any_instance_of(Schedule).to receive(:errors).and_return(['error'])
        post :create, {:schedule => { "name" => "invalid value" }}
        expect(response).to render_template(:new)
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested schedule" do
        expect_any_instance_of(Schedule).to receive(:update_attributes).with({ "name" => "MyString" }).and_return(true)
        xhr :put, :update, {:id => schedule.to_param, :schedule => { "name" => "MyString" }}
      end

      it "assigns the requested schedule as @schedule" do
        put :update, {:id => schedule.to_param, :schedule => valid_attributes}
        expect(assigns(:schedule)).to eq(schedule)
      end

      it "reloads the DOM portion of the form" do
        put :update, {:id => schedule.to_param, :schedule => valid_attributes}
        expect(response).to redirect_to edit_schedule_path(schedule)
      end
    end

    describe "with invalid params" do
      it "assigns the schedule as @schedule" do
        allow_any_instance_of(Schedule).to receive(:save).and_return(false)
        allow_any_instance_of(Schedule).to receive(:errors).and_return(['error'])
        put :update, {:id => schedule.to_param, :schedule => { "name" => "invalid value" }}
        expect(assigns(:schedule)).to eq(schedule)
      end

      it "renders the edit page" do
        allow_any_instance_of(Schedule).to receive(:save).and_return(false)
        allow_any_instance_of(Schedule).to receive(:errors).and_return(['error'])
        put :update, {:id => schedule.to_param, :schedule => { "name" => "invalid value" }}
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do

    it "destroys the requested schedule" do
      expect {
        delete :destroy, {:id => schedule.to_param}
      }.to change{Schedule.count}.by(-1)
    end

    it "redirects to the schedules list" do
      delete :destroy, {:id => schedule.to_param}
      expect(response).to redirect_to(schedules_url)
    end
  end

end
