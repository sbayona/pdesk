require 'spec_helper'

describe ClientsController do
  let(:account) { create(:account, :expired_trial, :with_account_owner) }
  let(:admin) { account.account_owner }

  before do
    sign_in admin
  end

  it "redirects to the expired trial payment page" do
    get :index
    expect(response).to redirect_to(edit_expire_path)
  end

end
