require 'spec_helper'

describe Script::SmsController do

  let(:admin) { create(:admin, :with_account) }

  before { sign_in admin }

  let!(:schedule) { create(:schedule, account: admin.account) }
  let(:sms_script) { schedule.sms_scripts.first }

  let(:valid_attributes) { { "message" => "Confirmation!" } }

  describe "GET edit" do
    it "assigns the requested script_sms as @script_sms" do
      xhr :get, :edit, schedule_id: schedule.to_param, id: sms_script.to_param
      expect(assigns(:sms_scripts)).to eq(sms_script)
    end

    it "" do
      xhr :get, :edit, schedule_id: schedule.to_param, id: sms_script.to_param
      expect(response).to render_template('edit')
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested script_sms" do
        expect_any_instance_of(Script::Sms).to receive(:update_attributes).with(valid_attributes).and_return(true)
        xhr :put, :update, id: sms_script.to_param, schedule_id: schedule.to_param, script_sms: valid_attributes
      end

      it "assigns the requested script_sms as @script_sms" do
        xhr :put, :update, id: sms_script.to_param, schedule_id: schedule.to_param, script_sms: valid_attributes
        expect(assigns(:sms_scripts)).to eq(sms_script)
      end

      it "redirects to the script_sms" do
        xhr :put, :update, id: sms_script.to_param, schedule_id: schedule.to_param, script_sms: valid_attributes
        expect(response).to render_template 'update'
      end
    end

    describe "with invalid params" do
      it "assigns the script_sms as @script_sms" do
        allow_any_instance_of(Script::Sms).to receive(:save).and_return(false)
        allow_any_instance_of(Script::Sms).to receive(:errors).and_return(['error'])

        xhr :put, :update, id: sms_script.to_param, schedule_id: schedule.to_param, script_sms: {some: 'test'}
        expect(assigns(:sms_scripts)).to eq(sms_script)
      end

      it "re-renders the 'edit' template" do
        allow_any_instance_of(Script::Sms).to receive(:save).and_return(false)
        allow_any_instance_of(Script::Sms).to receive(:errors).and_return(['error'])

        xhr :put, :update, id: sms_script.to_param, schedule_id: schedule.to_param, script_sms: {some: 'test'}
        expect(response).to render_template 'update'
      end
    end
  end
end
