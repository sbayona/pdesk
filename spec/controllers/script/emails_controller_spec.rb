require 'spec_helper'

describe Script::EmailsController do

  let(:account) { create(:account) }
  let(:admin) { create(:admin, account: account) }

  before { sign_in admin }

  let!(:schedule) { create(:schedule, account: account) }
  let(:email_script) { schedule.email_scripts.first }

  let(:valid_attributes) { { "subject" => "Confirmation!" } }

  describe "GET edit" do
    it "assigns the requested script_email as @script_email" do
      xhr :get, :edit, schedule_id: schedule.to_param, id: email_script.to_param
      expect(assigns(:email_scripts)).to eq(email_script)
    end

    it "" do
      xhr :get, :edit, schedule_id: schedule.to_param, id: email_script.to_param
      expect(response).to render_template('edit')
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested script_email" do
        expect_any_instance_of(Script::Email).to receive(:update_attributes).with(valid_attributes).and_return(true)
        xhr :put, :update, id: email_script.to_param, schedule_id: schedule.to_param, script_email: valid_attributes
      end

      it "assigns the requested script_email as @script_email" do
        xhr :put, :update, id: email_script.to_param, schedule_id: schedule.to_param, script_email: valid_attributes
        expect(assigns(:email_scripts)).to eq(email_script)
      end

      it "redirects to the script_email" do
        xhr :put, :update, id: email_script.to_param, schedule_id: schedule.to_param, script_email: valid_attributes
        expect(response).to render_template 'update'
      end
    end

    describe "with invalid params" do
      it "assigns the script_email as @script_email" do
        allow_any_instance_of(Script::Email).to receive(:save).and_return(false)
        allow_any_instance_of(Script::Email).to receive(:errors).and_return(['error'])

        xhr :put, :update, id: email_script.to_param, schedule_id: schedule.to_param, script_email: {some: 'test'}
        expect(assigns(:email_scripts)).to eq(email_script)
      end

      it "re-renders the 'edit' template" do
        allow_any_instance_of(Script::Email).to receive(:save).and_return(false)
        allow_any_instance_of(Script::Email).to receive(:errors).and_return(['error'])

        xhr :put, :update, id: email_script.to_param, schedule_id: schedule.to_param, script_email: {some: 'test'}
        expect(response).to render_template 'update'
      end
    end
  end
end
