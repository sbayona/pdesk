require 'spec_helper'

describe SubscriptionPlansController do

  let!(:account) { create :account, :with_account_owner }
  let!(:owner) { account.account_owner }
  let!(:subscription_plan) { create(:subscription_plan) }

  before { sign_in owner }

  describe "GET 'index'" do
    it "returns http success" do
      xhr :get, :edit
      expect(response).to be_success
    end
  end

  describe "PUT 'update'" do
    it "returns http success" do
      xhr :put, :update, {subscription_plan: { id: subscription_plan.id }}
      expect(response).to render_template(:update)
    end
  end

end
