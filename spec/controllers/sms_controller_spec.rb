require 'spec_helper'

describe SmsController do

  describe "reply" do
    let!(:event) { create(:event, :with_outstanding_sms_confirmation_request, :in_the_future, :with_schedule_subscriber, :with_associations) }
    let(:client) { event.client }
    let(:state) { Event::CONFIRMATION_CODE[:CONFIRMED] }
    let(:valid_params) { { Body: state, To: "+14049751432", MessageSid: "SM51bd9f03b0866590c0f617c4ec878564", AccountSid: "AC9bcc5a64fff4e4a2e1f3e014d84f4a39", From: client.mobile_number } }

    describe "with confirmed response" do
      it "confirms the event" do
        expect { post :reply, valid_params }.to change { event.reload.state }.from('pending').to('confirmed')
      end
    end

    describe "with multiple unconfirmed events in the future" do

      let(:new_event) { event.dup }

      before do
        new_event.start_time = Chronic.parse("2 days from now")
        new_event.save!
      end

      it "confirms the closest (upcoming) appointment" do
        expect {
          post :reply, valid_params 
        }.to change { event.reload.state }.from('pending').to('confirmed')
      end

      it "does not affect the later" do
        expect {
          post :reply, valid_params
        }.to_not change { new_event.reload.state }
      end

    end

    describe "with confirmed events in the future" do
      before do
        event.confirm!
      end

      it "raises an argument error" do
        expect {
          post :reply, valid_params
        }.to raise_error(ArgumentError, "No likely client for #{client.mobile_number}")
      end
    end

    context "with no events in the future (one event in the past)" do
      let!(:event) { create(:event, :with_outstanding_sms_confirmation_request, :in_the_past, :with_schedule_subscriber, :with_associations) }
      let(:client) { event.client }
      let(:status_code) { Event::CONFIRMATION_CODE[:CONFIRMED] }
      let(:valid_params) { { Body: status_code, To: "+14049751432", MessageSid: "SM51bd9f03b0866590c0f617c4ec878564", AccountSid: "AC9bcc5a64fff4e4a2e1f3e014d84f4a39", From: client.mobile_number } }
      let(:reply) { post :reply, valid_params }

      it "raises an error" do
        expect { reply }.to raise_error(ArgumentError, "No likely client for #{client.mobile_number}")
      end
    end

    describe "with callback request" do
      let(:state) { Event::CONFIRMATION_CODE[:CALLBACK] }

      it "sets it to callback status" do
        expect { post :reply, valid_params }.to change { event.reload.state }.from('pending').to('callback')
      end

      it "sends the request callback email to the subscribers" do
         expect {post :reply, valid_params }.to change { ActionMailer::Base.deliveries.size }.by(1)
      end

      it "sets the update mechanism" do
        expect {post :reply, valid_params }.to change { event.reload.updated_via }.from(nil).to(Event::UPDATE_VIA[:SMS])
      end
    end
  end

  describe "unknown code" do
    let!(:event) { create(:event, :with_outstanding_sms_confirmation_request, :in_the_future, :with_schedule_subscriber, :with_associations) }
    let(:client) { event.client }
    let(:status_code) { "8" }
    let(:valid_params) { { Body: status_code, To: "+14049751432", MessageSid: "SM51bd9f03b0866590c0f617c4ec878564", AccountSid: "AC9bcc5a64fff4e4a2e1f3e014d84f4a39", From: client.mobile_number } }
    let(:reply) { post :reply, valid_params }

    it "does not change the event state" do
      expect { reply }.to_not change { event.reload.state }
    end

    it "response body is blank" do
      reply
      expect(response.body).to be_blank
    end

    it "receives unknown reply method" do
      event = stub_model(Event)
      allow(controller).to receive(:event).and_return(event)
      expect(event).to receive(:unknown_reply)
      reply
    end
  end

  describe "delivered callback" do
    let!(:event) { create(:event, :with_sms_log, :with_associations) }
    let(:sms_log) { event.sms_logs.first }
    let(:callback) { post :callback, {SmsSid: "SMaed1e83dcb584066b8d061bbaaf1bc4d", SmsStatus: "delivered", MessageStatus: "delivered", To: "+13233164935", MessageSid: "SMaed1e83dcb584066b8d061bbaaf1bc4d", AccountSid: "AC9bcc5a64fff4e4a2e1f3e014d84f4a39", From: "+14049751432", ApiVersion: "2010-04-01", log_id: sms_log.id}}

    it "set the delivered at timestamp" do
      Timecop.freeze do
        expect {callback}.to change { sms_log.reload.delivered_at.to_i }.from(0).to(Time.now.utc.to_i)
      end
    end

    it "sets the payload" do
      expect { callback }.to change { sms_log.reload.callback_payload }.from(nil).to('delivered')
    end
  end

end
