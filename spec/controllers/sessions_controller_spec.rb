require 'spec_helper'

describe SessionsController do

  before { @request.env["devise.mapping"] = Devise.mappings[:user] }

  describe "POST 'create'" do

    let!(:account) { create(:account) }
    let!(:user) { create(:user, account: account) }

    it "logs in an user with an active account" do
      post :create, user: {
        email: user.email,
        password: user.password
      }
      expect(response.status).to eq(302)
      expect(response).to redirect_to(root_path)
    end
  end

  describe "with canceled account" do

    let!(:account) { create(:account, :canceled) }
    let!(:user) { create(:user, account: account) }

    it "rejects an user with an canceled account" do
      post :create, user: {
        email: user.email,
        password: user.password
      }
      expect(response.status).to eq(200)
      expect(response).to render_template(:new)
    end
  end
end
