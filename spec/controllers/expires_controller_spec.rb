require 'spec_helper'

describe ExpiresController do

  let(:account) { create(:account, :with_account_owner) }
  let(:admin) { account.account_owner }
  let(:subscription_plan) { create(:subscription_plan) }

  before do
    sign_in admin
  end

  describe "GET 'edit'" do

    it "returns http success" do
      allow(controller.current_account).to receive(:needs_subscription_info?) {true}

      get 'edit'
      expect(response).to be_success
    end

    it "redirects to root when subscription info is not required yet" do
      allow(controller.current_account).to receive(:needs_subscription_info?) {false}
      
      get 'edit'
      expect(response).to redirect_to root_path
    end

  end

  describe "GET 'update'" do
    it "returns http success" do
      put :update, account: {
        stripe_token: "tok_14l0UMLFpj4nm9zCzrqBtxiw",
        last_4_digits: "9424",
        subscription_plan: { id: subscription_plan.id }
      }
      expect(response).to redirect_to root_path
    end
  end

end
