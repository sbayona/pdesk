require 'spec_helper'
require 'stripe_mock'

describe StripesController, "As admin" do

  describe "POST charge" do

    before { StripeMock.start }
    after { StripeMock.stop }
    let(:stripe_helper) { StripeMock.create_test_helper }
    let!(:customer) {
      Stripe::Customer.create({
        email: 'homer@martianinteractive.com',
        card: stripe_helper.generate_card_token
      })
    }
    let!(:account) { create(:account, :with_account_owner, :with_subscription, stripe_id: customer.id) }

    let(:valid_attributes) {
      StripeMock.mock_webhook_event('charge.succeeded', {
        :customer => customer.id
      }).to_h.merge({format: :xml})
    }

    let(:invalid_event_attributes) {
      StripeMock.mock_webhook_event('account.updated', {
        :customer => customer.id
      }).to_h.merge({format: :xml})
    }

    let(:non_existing_customer_attributes) {
      StripeMock.mock_webhook_event('charge.succeeded', {
        :customer => 'asdf'
      }).to_h.merge({format: :xml})
    }


    describe "with valid params" do
      let(:make_request) { post :charge, valid_attributes }

      it "creates a new Charge" do
        expect {
          make_request
        }.to change(Charge, :count).by(1)
      end

      it "returns a 200 status code" do
        make_request
        assert_response :success
      end

    end

    describe "with invalid params" do

      context "invalid type" do
        it 'returns "unprocessable entity" code' do
          post :charge, invalid_event_attributes
          assert_response 422
        end
      end

      context "non-existing customer" do
        it 'raises RecordNotFound error ' do
          expect {
            post :charge, non_existing_customer_attributes
          }.to raise_error ActiveRecord::RecordNotFound
        end
      end
    end
  end

end
