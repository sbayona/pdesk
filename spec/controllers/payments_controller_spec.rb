require 'spec_helper'

describe PaymentsController do
  describe "with subscription plan" do
  let(:account) { create(:account, :with_account_owner, :with_subscription) }
  let(:admin) { account.account_owner }
  
  before { sign_in admin }

    it "returns http success" do
      get :edit
      expect(response).to be_success
    end

    it "returns http success" do
      put :update, account: { stripe_token: "tok_14l0UMLFpj4nm9zCzrqBtxiw", last_4_digits: "9424"}
      expect(response).to be_redirect
    end
    
  end

  describe "without subscription plan" do
  let(:account) { create(:account, :with_account_owner) }
  let(:admin) { account.account_owner }
  
  before { sign_in admin }
  
    it "redirects to billing when account has no subscriptions" do
      get 'edit'
      expect(response).to be_redirect
    end

  end

end
