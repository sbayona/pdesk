require 'spec_helper'

describe AccountsController do

  let!(:admin) { create(:account_owner, :with_account) }

  before { sign_in admin }

  describe "GET edit" do
    it "renders edit template" do
      xhr :get, :edit
      expect(response).to render_template 'edit'
    end
  end

  describe "PUT update" do
    let(:account_values) { {"name" => "Monkey Joes"} }

    describe "with valid params" do
      let(:make_request) { put :update, account: account_values}

      it "updates the requested schedule_timing" do
        expect_any_instance_of(Account).to receive(:update_attributes).with(account_values)
        make_request
      end

      it "render the update template" do
        make_request
        expect(response).to redirect_to edit_account_path
      end
    end

    describe "with invalid params" do
      let(:make_invalid_request) { put :update, account: { bla: 'test' } }

      before do
        allow_any_instance_of(Account).to receive(:update_attributes).and_return(false)
        allow_any_instance_of(Account).to receive(:errors).and_return(['error'])
      end

      it "re-renders the 'edit' template" do
        make_invalid_request
        expect(response).to render_template "edit"
      end
    end
  end

  describe "DELETE destroy" do
    before do
      allow_any_instance_of(Account).to receive(:delete_stripe_subscriptions).and_return(true)
    end

    it "changes account state to 'canceled'" do
      expect {
        xhr :delete, :destroy
      }.to change {Account.last.state}.from(Account::ACTIVE).to(Account::CANCELED)
    end

    it "redirects to the root url" do
      xhr :delete, :destroy
      expect(response).to redirect_to(root_url)
    end

    it "signs out the current user" do
      expect {
        xhr :delete, :destroy
      }.to change {subject.current_user}.from(admin.account.users.first).to(nil)
    end

    context 'user that is not an account owner' do

      let!(:admin) { create(:admin, :with_account) }

      it "keeps account as active" do
        expect {
          xhr :delete, :destroy
        }.not_to change { Account.last.state }.from(Account::ACTIVE)
      end
    end

  end

end
