require 'spec_helper'

shared_examples "an authenticated controller" do

  context "make an action request" do
    it "should redirect to new session page" do
      do_request
      expect(response).to redirect_to(new_user_session_path)
    end
  end
end

describe DashboardController do
  let!(:user) { create(:user, :with_account) }
  let(:account) { user.account }
  let(:schedule) { account.schedules.first }

  context "index" do
    let(:get_index) { get :index }

    it_behaves_like "an authenticated controller" do
      let(:do_request) { get_index }
    end

    describe "dashboard" do
      before do
        sign_in user
      end

      context "with one schedule" do
        before { get_index }

        it "redirects to the first schedule" do
          expect(response).to redirect_to root_path(schedule: schedule.id)
        end
      end

      context "with more than one schedule" do
        before do
          account.schedules.create(name: "schedule 2")
          get_index
        end

        it { expect(response).to be_ok }
        it { expect(response).to render_template("dashboard/index")}
        it { expect(response).to render_template("layouts/application") }
      end
    end
  end
end
