require 'spec_helper'

describe ScheduleSubscriptionsController do
  let(:admin) { create(:admin, :with_account) }

  before { sign_in admin }

  let!(:schedule) { create(:schedule, account: admin.account) }

  let(:valid_attributes) { {user_id: admin.id } }

  describe "POST create" do
    describe "with valid params" do
      let(:make_request) { xhr :post, :create, schedule_id: schedule.id, schedule_subscription: valid_attributes }

      it "creates a new Schedule Notification" do
        expect {
          make_request
        }.to change(ScheduleSubscription, :count).by(1)
      end

      it "assigns a newly created schedule as @schedule" do
        make_request
        expect(assigns(:schedule_subscription)).to be_a(ScheduleSubscription)
        expect(assigns(:schedule_subscription)).to be_persisted
      end

      it "redirects to the created schedule" do
        make_request
        expect(response).to render_template "create"
      end
    end

    describe "with invalid params" do
      let(:make_invalid_request) { xhr :post, :create, schedule_id: schedule.id, schedule_subscription: {} }

      it "assigns a newly created but unsaved schedule as @schedule" do
        # Trigger the behavior that occurs when invalid params are submitted
        allow_any_instance_of(ScheduleSubscription).to receive(:save).and_return(false)
        allow_any_instance_of(ScheduleSubscription).to receive(:errors).and_return(['error'])
        make_invalid_request
        expect(assigns(:schedule_subscription)).to be_a_new(ScheduleSubscription)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        allow_any_instance_of(ScheduleSubscription).to receive(:save).and_return(false)
        allow_any_instance_of(ScheduleSubscription).to receive(:errors).and_return(['error'])
        make_invalid_request
        expect(response).to render_template "create"
      end
    end
  end

end
