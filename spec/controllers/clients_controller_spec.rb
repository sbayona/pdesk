require 'spec_helper'

describe ClientsController, "As admin" do

  let(:account) { create(:account, :with_account_owner) }
  let(:admin) { account.account_owner }

  let(:valid_attributes) { { "name" => "Jhon Petersen" } }

  before { sign_in admin }

  describe "GET index" do
    let(:client) { admin.clients.create! valid_attributes }

    it "assigns all clients as @clients" do
      get :index
      expect(assigns(:clients)).to eq([client])
    end

    context "searching" do
      let(:clients) { double('clients').as_null_object }
      let(:search_param) { 'Petersen' }

      before do
        allow(Client).to receive(:search).and_return(clients)
      end

      it "invokes the search method" do
        expect(Client).to receive(:fuzzy_search).with(search_param).and_return(clients)
        get :index, search: search_param
      end
    end
  end

  describe "GET show" do
    let!(:client) { admin.clients.create! valid_attributes }

    context 'client without events' do

      before { get :show, {:id => client.to_param} }

      it "assigns the requested client as @client" do
        expect(assigns(:client)).to eq(client)
      end

      it "client events are empty" do
        expect(assigns(:client_events)).to be_empty
      end

    end

    context 'client with events' do

    let(:past_event) {create(:event, :without_client_with_user, :in_the_past, client: client)}
    let(:future_event) {create(:event, :without_client_with_user, :in_the_future, client: client)}

      it "returns upcoming events" do
        get :show, {:id => client.to_param, upcoming: true}
        expect(assigns(:client_events)).to eq([future_event])
      end

      it "returns recent events" do
        get :show, {:id => client.to_param}
        expect(assigns(:client_events)).to eq([past_event])
      end

    end

  end

  describe "GET new" do
    it "assigns a new client as @client" do
      get :new
      expect(assigns(:client)).to be_a_new(Client)
    end
  end

  describe "GET edit" do
    it "assigns the requested client as @client" do
      client = admin.clients.create! valid_attributes
      get :edit, {:id => client.to_param}
      expect(assigns(:client)).to eq(client)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      let(:make_request) { post :create, {:client => valid_attributes} }

      it "creates a new Client" do
        expect {
          make_request
        }.to change(Client, :count).by(1)
      end

      it "assigns a newly created client as @client" do
        make_request
        expect(assigns(:client)).to be_a(Client)
        expect(assigns(:client)).to be_persisted
      end

      it "redirects to the created client" do
        make_request
        expect(response).to redirect_to(Client.last)
      end
    end

    describe "with invalid params" do
      let(:make_invalid_request) { post :create, {:client => { "name" => "invalid value" }} }

      before do
        allow_any_instance_of(Client).to receive(:save).and_return(false)
        allow_any_instance_of(Client).to receive(:errors).and_return(['error'])
      end

      it "assigns a newly created but unsaved client as @client" do
        make_invalid_request
        expect(assigns(:client)).to be_a_new(Client)
      end

      it "re-renders the 'new' template" do
        make_invalid_request
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      let!(:client) { admin.clients.create! valid_attributes }
      let(:make_request) { put :update, {:id => client.to_param, :client => { "name" => "MyString" }} }

      it "updates the requested client" do

        expect_any_instance_of(Client).to receive(:update_attributes).with({ "name" => "MyString" }).and_return(true)
        make_request
      end

      it "assigns the requested client as @client" do
        make_request
        expect(assigns(:client)).to eq(client)
      end

      it "redirects to the client" do
        make_request
        expect(response).to redirect_to(client)
      end
    end

    describe "with invalid params" do
      let!(:client) { admin.clients.create! valid_attributes }
      let(:make_invalid_request) { put :update, {:id => client.to_param, :client => { "name" => "invalid value" }} }

      before do
        allow_any_instance_of(Client).to receive(:save).and_return(false)
        allow_any_instance_of(Client).to receive(:errors).and_return(['error'])
      end

      it "assigns the client as @client" do
        make_invalid_request
        expect(assigns(:client)).to eq(client)
      end

      it "re-renders the 'edit' template" do
        make_invalid_request
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    let(:make_request) { delete :destroy, {:id => client.to_param} }
    let!(:client) { admin.clients.create! valid_attributes }

    it "destroys the requested client" do
      expect {
        make_request
      }.to change(Client, :count).by(-1)
    end

    it "redirects to the clients list" do
      make_request
      expect(response).to redirect_to(clients_url)
    end
  end

  describe "GET unsubscribe" do
    let!(:client) { admin.clients.create! valid_attributes }
    let(:request) { get :unsubscribe, token: client.token }

    it "changes the email preferences" do
      expect { request }.to change { client.reload.receive_email }.from(true).to(false)
    end

    it "renders the unsubscribe template" do
      expect(request).to render_template 'unsubscribe'
    end
  end

end

describe ClientsController, "As regular user" do
  let!(:user) { create(:user, :with_account) }
  let!(:client) { create(:client, creator: user) }

  before { sign_in user }

  it { expect(get :index).to render_template 'index' }
  it { expect(get :new).to render_template 'new' }

  it "" do
    get :edit, {:id => client.to_param}
    expect(response.status).to be 200
  end

  it " " do
    get :unsubscribe, {token: client.token}
    expect(response.status).to be 200
  end


end
