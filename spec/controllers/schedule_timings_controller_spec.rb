require 'spec_helper'

describe ScheduleTimingsController do

  let(:admin) { create(:admin, :with_account) }
  let(:account) { admin.account }

  before { sign_in admin }

  let!(:schedule) { create(:schedule, account: account) }
  let(:valid_timing_value) { ScheduleTiming::OPTIONS.last["KEY"] }

  let(:valid_attributes) { { name: "some name", schedule: schedule, options: [{"days_before"=>"0", "value"=> valid_timing_value}] } }

  describe "GET new" do
    it "assigns a new schedule_timing as @schedule_timing" do
      xhr :get, :new, schedule_id: schedule.id
      expect(assigns(:schedule_timing)).to be_a_new(ScheduleTiming)
    end
  end

  describe "GET edit" do
    it "assigns the requested schedule_timing as @schedule_timing" do
      schedule_timing = ScheduleTiming.create! valid_attributes
      xhr :get, :edit, id: schedule_timing.to_param, schedule_id: schedule.to_param
      expect(assigns(:schedule_timing)).to eq(schedule_timing)
    end
  end

  describe "POST create" do
    let(:make_request) { xhr :post, :create, schedule_id: schedule.to_param, schedule_timing: valid_attributes }

    describe "with valid params" do
      it "creates a new ScheduleTiming" do
        expect {
          make_request
        }.to change(ScheduleTiming, :count).by(1)
      end

      it "assigns a newly created schedule_timing as @schedule_timing" do
        make_request
        expect(assigns(:schedule_timing)).to be_a(ScheduleTiming)
        expect(assigns(:schedule_timing)).to be_persisted
      end

      it "redirects to the created schedule_timing" do
        make_request
        expect(response).to render_template 'create'
      end
    end

    describe "with invalid params" do
      let(:make_invalid_request) { xhr :post, :create, schedule_id: schedule.to_param, schedule_timing: {test: 'bla'} }

      before do
        allow_any_instance_of(ScheduleTiming).to receive(:save).and_return(false)
        allow_any_instance_of(ScheduleTiming).to receive(:errors).and_return(['error'])
      end

      it "assigns a newly created but unsaved schedule_timing as @schedule_timing" do
        make_invalid_request
        expect(assigns(:schedule_timing)).to be_a_new(ScheduleTiming)
      end

      it "re-renders the 'new' template" do
        make_invalid_request
        expect(response).to render_template "create"
      end
    end
  end

  describe "PUT update" do
    let!(:schedule_timing) { ScheduleTiming.create! valid_attributes }

    describe "with valid params" do
      let(:make_request) { xhr :put, :update, schedule_id: schedule.to_param, id: schedule_timing.to_param, schedule_timing: {name: "myname", options: [] } }

      it "updates the requested schedule_timing" do
        expect_any_instance_of(ScheduleTiming).to receive(:update_attributes).with({ "name" => "myname", "options" => [] })
        make_request
      end

      it "assigns the requested schedule_timing as @schedule_timing" do
        make_request
        expect(assigns(:schedule_timing)).to eq(schedule_timing)
      end

      it "redirects to the schedule_timing" do
        make_request
        expect(response).to render_template 'update'
      end
    end

    describe "with invalid params" do
      let(:make_invalid_request) { xhr :put, :update, schedule_id: schedule.to_param, id: schedule_timing.to_param, schedule_timing: {test: 'bla'} }

      before do
        allow_any_instance_of(ScheduleTiming).to receive(:save).and_return(false)
        allow_any_instance_of(ScheduleTiming).to receive(:errors).and_return(['error'])
      end

      it "assigns the schedule_timing as @schedule_timing" do
        make_invalid_request
        expect(assigns(:schedule_timing)).to eq(schedule_timing)
      end

      it "re-renders the 'edit' template" do
        make_invalid_request
        expect(response).to render_template "update"
      end
    end
  end

  describe "DELETE destroy" do
    let!(:schedule_timing) { ScheduleTiming.create! valid_attributes }
    let(:make_request) { xhr :delete, :destroy, schedule_id: schedule.to_param, id: schedule_timing.to_param }

    it "destroys the requested schedule_timing" do
      expect {
        make_request
      }.to change(ScheduleTiming, :count).by(-1)
    end

    it "redirects to the schedule_timings list" do
      make_request
      expect(response).to render_template "destroy"
    end
  end

end
