require 'spec_helper'

describe NotesController do

  let!(:account) { create :account, :with_account_owner }
  let!(:owner) { account.account_owner }
  let!(:client) { create(:client, creator: owner, account: account)}

  let!(:dr_lee_schedule) { create :schedule, :with_events, account: account }
  let(:event) { dr_lee_schedule.events.first }

  let(:valid_attributes) { attributes_for(:note) }
  let(:invalid_attributes) { { description: ''} }

  let!(:note) { create :note, account: account, event: event }

  before { sign_in owner }

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Note" do
        expect {
          xhr :post, :create, {event_id: event.id, note: valid_attributes}
        }.to change(Note, :count).by(1)
      end

      it "assigns a newly created note as @note" do
        xhr :post, :create, {event_id: event.id, note: valid_attributes}
        expect(assigns(:note)).to be_a(Note)
        expect(assigns(:note)).to be_persisted
      end

    end

    context "with invalid params" do
      it "doesn't creates a new note for empty descriptions" do
        expect {
          xhr :post, :create, {event_id: event.id, note: invalid_attributes}
        }.to change(Note, :count).by(0)
      end

      it "doesn't creates a new note for events not belonging to the current account" do
        another_account = create :account, :with_account_owner
        another_schedule = create :schedule, :with_events, account: another_account
        another_event = another_schedule.events.first

        expect {
          xhr :post, :create, {event_id: another_event.id, note: valid_attributes}
        }.to raise_error ActiveRecord::RecordNotFound
      end

    end
  end

  describe "PUT #update" do

    it "update a note" do
      expect { 
        xhr :put, :update, { event_id: event, id: note, note: { description: "Updated Note" } }
       }.to change { note.reload.description }.from('test note').to('Updated Note')
    end

    it "should render update template" do
      xhr :put, :update, { event_id: event, id: note, note: { description: "Updated Note" } }
      expect(response).to render_template(:update)
    end

    it "should render js content_type" do
      xhr :put, :update, { event_id: event, id: note, note: { description: "Updated Note" } }
      expect(response.content_type).to eq("text/javascript")
    end
  end

  describe "DELETE #destroy" do

    it "removes a note from event" do
      expect {
        xhr :delete, :destroy, { event_id: event.id, id: note.id }
      }.to change(Note, :count).by(-1)
    end

    it "should render destroy template" do
      xhr :delete, :destroy, { event_id: event.id, id: note.id }
      expect(response).to render_template(:destroy)
    end

    it "should render js content_type" do
      xhr :delete, :destroy, { event_id: event.id, id: note.id }
      expect(response.content_type).to eq("text/javascript")
    end
  end

end
