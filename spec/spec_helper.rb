require 'simplecov'
SimpleCov.start 'rails'
# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'test_xml/spec'
require 'capybara/rails'
require 'capybara/rspec'
require 'vcr'
require 'shoulda/matchers'
require 'byebug'

Capybara.javascript_driver = :webkit

Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }


ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|

  config.infer_spec_type_from_file_location!

  database_cleaner(config)

  config.order = "random"

  config.include FeatureHelpers, type: :feature

  config.include FactoryGirl::Syntax::Methods

  config.before(:each) { Time.zone = 'UTC' }
end


VCR.configure do |c|
  c.cassette_library_dir = 'spec/cassettes'
  c.hook_into :webmock
  c.configure_rspec_metadata!
  c.allow_http_connections_when_no_cassette = true
end


Capybara::Webkit.configure do |config|
  config.block_unknown_urls
  config.allow_url("api.stripe.com")
  config.allow_url('js.stripe.com')
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end

