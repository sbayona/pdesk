require 'spec_helper'

describe Demo do

end

describe DemoPhoneCall do
  let(:user) { create(:user, :with_account) }
  let(:account) { user.account }

  it "returns the Text-to-Speech copy" do
    Timecop.freeze do
      demo_datetime = DemoPhoneCall.new(user).demo_datetime
      expect(DemoPhoneCall.new(user).demo_speech).to eq(<<-EOS)
This is a practical desk demonstration.
Hello! This is #{account.name}. You have an appointment on #{demo_datetime}. Please click one to confirm or five to cancel.
EOS
    end
  end
end
