require 'spec_helper'

describe EventReport do
  describe '#to_a' do
    let!(:user) { create(:user, :with_account) }
    let!(:schedule) { create(:schedule, account: user.account) }

    before do
      create(:event, :without_user, user: user)
      create(:event, :without_user, user: user)
    end

    it "filters events by schedule" do
      event = create(:event, :without_schedule, user: user, schedule: schedule)

      report = EventReport.new(user.account, q: { schedule_id: schedule.id })

      expect(report.to_enum).to match_array([event])
    end

    it "filters events by client" do
      client = create(:client, creator: user)
      event = create(:event, :without_client, user: user, client: client)

      report = EventReport.new(user.account, q: { client_id: client.id })

      expect(report.to_enum).to match_array([event])
    end

    it "filters events by state" do
      event = create(:event, :without_user, user: user)

      event.confirm!

      report = EventReport.new(user.account, q: { state: 'confirmed' })

      expect(report.to_enum).to match_array([event])
    end

    it "filters events by creator" do
      creator = create(:user, :with_account)
      event = create(:event, :without_user, user: creator)

      report = EventReport.new(creator.account, q: { user_id: creator.id })

      expect(report.to_enum).to match_array([event])
    end

    it "filters events by start_time" do
      today = Date.today
      range = (today - 2.days)..today

      event_1 = create(:event, :without_user, user: user, start_time: today - 2.days)
      event_2 = create(:event, :without_user, user: user, start_time: today - 1.day)
      event_3 = create(:event, :without_user, user: user, start_time: today)

      report = EventReport.new(user.account, q: {
        date_range_left: range.first, date_range_right: range.last
      })

      expect(report.to_enum).to match_array([event_1, event_2, event_3])

      report = EventReport.new(user.account, q: {
        date_range_left: today, date_range_right: today
      })

      expect(report.to_enum).to match_array([event_3])
    end

    it "paginates events" do
      report = EventReport.new(user.account)

      expect(report.current_page).to be(1)
      expect(report.total_pages).to be(1)
    end
  end
end
