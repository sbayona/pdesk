require 'spec_helper'

describe Charge do
  it { is_expected.to belong_to(:account) }

  it { is_expected.to validate_uniqueness_of(:stripe_event_id) }

end
