require 'spec_helper'

describe ScheduleSubscription do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:schedule) }

  it { is_expected.to validate_presence_of(:user).with_message('select a user') }
  it { is_expected.to validate_presence_of(:schedule).with_message('select a schedule') }

  it "" do
    account = create(:account)
    user = create(:user, account: account)
    schedule = create(:schedule, account: account)
    create(:schedule_subscription, user: user, schedule: schedule)
    is_expected.to validate_uniqueness_of(:user_id).scoped_to(:schedule_id)
  end


end
