require 'spec_helper'

describe Event do
  include Concerns::Twilio

  let!(:event) { create(:event, :with_associations) }
  let(:send_sms) { event.sms_client_event_confirmation_request!(999) }

  describe "requesting sms confirmation" do
    describe "with a valid message", vcr: { :cassette_name => "send_valid_sms" } do
      before { send_sms }

      it { expect(send_sms.has_key?(:data)).to eq true}
      it { expect(send_sms[:data][:to]).to eq event.client.mobile_number }
      it { expect(send_sms[:data][:body]).to be_present}
    end

    describe "with an invalid message", vcr: { :cassette_name => "send_sms_with_bad_number" } do
      before do
        event.client.update_attributes(mobile_number: nil)
        send_sms
      end

      it { expect(send_sms.has_key?(:error)).to eq true}
      it { expect(send_sms.has_key?(:data)).to eq false}
    end
  end


end
