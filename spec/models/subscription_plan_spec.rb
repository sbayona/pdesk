require 'spec_helper'

describe SubscriptionPlan do
  it { is_expected.to have_many(:accounts) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:code) }
  it { is_expected.to validate_presence_of(:price_in_cents) }
  it { is_expected.to validate_presence_of(:currency) }
  it { is_expected.to validate_presence_of(:max_events_per_month) }
  it { is_expected.to validate_presence_of(:max_users) }
  it { is_expected.to validate_presence_of(:country_code) }
  it { is_expected.to validate_uniqueness_of(:code) }
  it { is_expected.to validate_uniqueness_of(:name) }
end
