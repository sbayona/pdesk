describe ClientNotificationCaller do


  let!(:schedule) { create(:schedule, :with_account, length: 30) }
  let!(:creator) { create(:user, account: schedule.account) }
  let!(:client) {  create(:client, name: "Laura Bush", creator: creator) }
  let(:account) { schedule.account }
  let(:schedule_timing) { schedule.schedule_timings.first }
  let(:valid_event_attributes) { {start_time: Time.zone.now, schedule: schedule, schedule_timing: schedule_timing, account: account, user: creator, client: client } }
  let(:event) { Event.create!(valid_event_attributes) }
  let(:caller) { ClientNotificationCaller.new(event) }

  before { allow(event).to receive(:caller_id_number) {'4444444444'} }

  it "changes the time zone" do
    expect { caller }.to change { Time.zone.name }.from('UTC').to('Eastern Time (US & Canada)')
  end

  it "creates an event version" do
    expect { caller.request_confirmation }.to change { event.versions.count }.by(1)
  end

  describe "" do
    before { caller.request_confirmation }

    let(:version) { event.versions.last }

    it { expect(version.event).to eq 'update' }

    it { expect(version.action).to eq 'reminder_msg' }

    it { expect(version.via).to eq 'voice' }

    it { expect(version.target).to eq 'client' }
  end


end
