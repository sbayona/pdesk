require 'spec_helper'

describe QueueRescheduleNotification do


  describe "creating the event" do
    let!(:event) { create(:event, :with_associations) }

    it "does not find event as rescheduled" do
      expect(QueueRescheduleNotification.pending_for_client).to be_empty
    end

    it "does not find event as rescheduled" do
      expect(QueueRescheduleNotification.pending_for_users).to be_empty
    end
  end

  describe "re-scheduling the event to a future time" do
    let(:client_query) { QueueRescheduleNotification.pending_for_client }

    context "confirmed events" do
      let(:confirmed_event) { create(:event, :with_associations, :confirmed, :rescheduled) }

      it { expect(client_query).to include(confirmed_event) }
    end

    context "pending events" do
      let(:pending_event) { create(:event, :with_associations, :pending, :rescheduled) }

      it { expect(client_query).to include(pending_event) }
    end

    context "canceled events" do
      let(:canceled_event) { create(:event, :with_associations, :canceled, :rescheduled) }

      it { expect(client_query).not_to include(canceled_event) }
    end

    context "show events" do
      let(:showed_event) { create(:event, :with_associations, :showed, :rescheduled) }

      it { expect(client_query).not_to include(showed_event) }
    end

    context "no show events" do
      let(:no_showed_event) { create(:event, :with_associations, :no_showed, :rescheduled) }

      it { expect(client_query).not_to include(no_showed_event) }
    end

    describe "3 minutes later..." do
      let!(:event) { create(:event, :with_associations, :rescheduled) }

      it "picks up the event" do
        expect(QueueRescheduleNotification.pending_for_client).to eq([event])
      end

      describe "run the queue" do
        let(:run_queue) { QueueRescheduleNotification.run! }

        it "creates an entry on the event logs table for client notifications" do
          expect{run_queue}.to change { EmailLog.reschedules.for_clients.count }.by(1)
        end

        it "creates an entry on the event logs table for user notifications" do
          expect{run_queue}.to change { EmailLog.reschedules.for_users.count }.by(1)
        end

        it "sets the completed_at timestamp at the end" do
          run_queue
          expect(event.email_logs.reschedules.last.completed_at.to_i).to eq(Time.now.to_i)
        end

        it "sets the completed_at timestamp at the end" do
          Timecop.freeze do
            run_queue
            expect(event.email_logs.last.completed_at.to_i).to eq(Time.now.to_i)
          end
        end

        describe "then run it again" do
          before { run_queue }

          it { expect(QueueRescheduleNotification.pending_for_client).to be_empty }
          it { expect(QueueRescheduleNotification.pending_for_users).to be_empty }

          describe "with a different event" do
            let!(:another_event) { create(:event, :with_associations) }

            it "does not change the event log" do
              expect{ run_queue }.to_not change { EmailLog.count }
            end

            it "picks up the next event" do
              another_event.update_attributes(start_time: Time.now + 3.days)

              expect(QueueRescheduleNotification.pending_for_client).to eq([another_event])
            end
          end

          describe "updating the same event again" do
            before { event.update_attributes(start_time: Time.now + 3.hour) }

            it "picks it up again" do
              expect(QueueRescheduleNotification.pending_for_client).to eq([event])
            end
          end
        end
      end
    end
  end

  describe "rescheduling event to earlier time" do
    let(:event) { create(:event, :with_associations) }

    before { event.update_attributes(start_time: event.start_time - 2.hours) }

    it "picks up the event" do
      expect(QueueRescheduleNotification.pending_for_client).to eq([event])
    end
  end

  describe "an event that has passed" do
    let(:old_event) { create(:event, :with_associations, :from_3_weeks_ago) }

    describe "rescheduling the event" do
      before { old_event.update_attributes(start_time: old_event.start_time + 1.hour) }

      it "does not include the old event as pending for clients" do
        expect(QueueRescheduleNotification.pending_for_client).not_to include(old_event)
      end

      it "does not include the old event as pending for users" do
        expect(QueueRescheduleNotification.pending_for_users).not_to include(old_event)
      end
    end
  end
end
