require 'spec_helper'

describe ScriptGroup do
  it { is_expected.to belong_to(:schedule) }
  it { is_expected.to belong_to(:email_script) }
  it { is_expected.to belong_to(:voice_script) }
end
