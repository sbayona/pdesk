require 'spec_helper'

describe Schedule do
  it { is_expected.to belong_to(:account) }
  it { is_expected.to have_many(:events).dependent(:destroy) }
  it { is_expected.to have_many(:clients).through(:events) }
  it { is_expected.to have_many(:schedule_subscriptions).dependent(:destroy) }
  it { is_expected.to have_many(:subscribers).through(:schedule_subscriptions) }
  it { is_expected.to have_many(:schedule_timings).dependent(:destroy) }
  it { is_expected.to have_many(:script_groups) }
  it { is_expected.to have_many(:email_scripts).through(:script_groups) }
  it { is_expected.to have_many(:sms_scripts).through(:script_groups) }
  it { is_expected.to have_many(:voice_scripts).through(:script_groups) }
  it { is_expected.to have_one(:caller_id) }

  Schedule::VALID_HOURS.each do |hour|
    it { is_expected.to allow_value(hour).for(:starts_at) }
    it { is_expected.to allow_value(hour).for(:ends_at) }
  end

  it { is_expected.to validate_inclusion_of(:frequency).in_array(Schedule::VALID_FREQUENCY) }
  it { is_expected.to validate_inclusion_of(:length).in_array(Schedule::VALID_LENGTH) }

  it { is_expected.not_to allow_value(1000).for(:starts_at) }
  it { is_expected.not_to allow_value(-1).for(:starts_at) }
  it { is_expected.not_to allow_value(1000).for(:ends_at) }
  it { is_expected.not_to allow_value(-1).for(:ends_at) }

  subject { create(:schedule, :with_account) }

  describe "on create" do
    it { expect(subject.default_timing).to_not be_blank }
    it { expect(subject.script_groups).to_not be_empty }
    it { expect(subject.email_scripts).to_not be_empty }
    it { expect(subject.events).to be_empty }
    it { expect(subject.schedule_subscriptions).to be_empty }
    it { expect(subject.clients).to be_empty }
    it { expect(subject.account).to be_present }
    it { expect(subject.starts_at).to be_present }
    it { expect(subject.ends_at).to be_present }
    it { expect(subject.frequency).to be_present }
    it { expect(subject.length).to be_present }
    it { expect(subject.business_days).to_not be_empty }
    it { expect(subject.reminder_preferences).to eq %w{1 2 3} }

  end

end
