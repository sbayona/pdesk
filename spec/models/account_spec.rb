require 'spec_helper'
require 'stripe_mock'

describe Account do
  it { is_expected.to have_many(:users) }
  it { is_expected.to have_many(:clients) }
  it { is_expected.to have_many(:schedules) }
  it { is_expected.to have_many(:events) }
  it { is_expected.to have_one(:account_owner) }
  it { is_expected.to belong_to(:subscription_plan) }
  it { is_expected.to have_many(:notes) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:client_name) }
  it { is_expected.to validate_presence_of(:event_name) }
  it { is_expected.to validate_numericality_of(:trial_period_in_days)}

  it { is_expected.to_not validate_presence_of(:subscription_plan) }

  it { is_expected.to callback(:create_default_schedule!).after(:create) }
  it { is_expected.to callback(:update_liquid_templates).after(:update) }
  it { is_expected.to callback(:update_stripe).before(:save) }
  it { is_expected.to callback(:persist_confirmation).before(:save) }
  it { is_expected.to callback(:set_trial_period).before(:create) }
  it { is_expected.to callback(:delete_stripe_subscriptions).before(:destroy) }

  let(:account) { create(:account, name: "test") }

  it { expect(account).to respond_to(:stripe_token)}

  it { expect(account.month_usage).to eq 0 }

  describe "subscription plan validation" do
    it "ensures subscription_plan_id if subscription plan is required" do
      account = Account.new
      account.validate_subscription_plan = true
      expect(account).to validate_presence_of(:subscription_plan)
    end
  end

  describe "trial period" do
    it "sets the trial period" do
      expect(account.trial_period_in_days).to eq ENV['TRIAL_PERIOD_IN_DAYS'].to_i
    end
  end

  describe "create defaults" do
    it "create a default schedule" do
      expect(account.schedules.count).to eq 1
    end
  end

  context "when updating the event_name or client_name" do

    it "" do
      expect do
        account.update_attributes(event_name: "interview")
      end.to change { account.scripts.first.message }
    end
  end

  it "versions should be an empty array" do
    expect(account.versions).to be_a ActiveRecord::Associations::CollectionProxy
  end

  it "scripts should return something" do
    expect(account.scripts).to be_present
  end

  context '.trial_period_ends_on' do

    it {expect(account.trial_period_ends_on).to be_an(ActiveSupport::TimeWithZone)}

    it {expect(account.trial_period_ends_on).to be_within(1.second).of 60.days.from_now}

  end

  context '.trial_period_ends_on_days' do
    it 'returns the days to expire the trial' do
      expect(account.trial_period_ends_on_days).to eq(60)
      Timecop.freeze(15.days.from_now) do
        expect(account.trial_period_ends_on_days).to eq(45)
      end
    end
  end

  context '.trial_expired?' do
    it 'returns false when the trial has expired' do
      expect(account.trial_expired?).to be false
      Timecop.freeze(60.days.from_now) do
        expect(account.trial_expired?).to be true
      end
    end
  end

  context '.needs_subscription_info?' do
    it "returns true when the trial has expired and the account has not been entered yet" do
      account.update_attributes(stripe_subscription_id: 'mock_subscription_id')
      expect(account.needs_subscription_info?).to be false
      Timecop.freeze(60.days.from_now) do
        expect(account.needs_subscription_info?).to be false
        account.update_attributes(
          subscription_plan_id: nil,
          stripe_subscription_id: nil
        )
        expect(account.needs_subscription_info?).to be true
      end
    end
  end

  context '.trial_about_to_expire?' do
    it "returns true when the trial is about to expire and the account has not been entered yet" do
      account.update_attributes(stripe_subscription_id: 'mock_subscription_id')
      expect(account.trial_about_to_expire?).to be false
      Timecop.freeze(54.days.from_now) do
        expect(account.trial_about_to_expire?).to be false
        account.update_attributes(
          subscription_plan_id: nil,
          stripe_subscription_id: nil
        )
        expect(account.trial_about_to_expire?).to be true
      end
    end
  end

  context 'account state' do

    let(:active) {create(:account, state: 'active')}
    let(:canceled) {create(:account, state: 'canceled')}
    let(:disabled) {create(:account, state: 'disabled')}

    describe 'state scopes' do
      it{ expect(Account.active).to eq([active])}
      it{ expect(Account.canceled).to eq([canceled])}
      it{ expect(Account.disabled).to eq([disabled])}
    end

    describe 'state constants' do
      it{ expect(Account::ACTIVE).to eq('active')}
      it{ expect(Account::CANCELED).to eq('canceled')}
      it{ expect(Account::DISABLED).to eq('disabled')}
    end

    describe 'state checking' do
      it{ expect(active.is_active?).to be true}
      it{ expect(canceled.is_canceled?).to be true}
      it{ expect(disabled.is_disabled?).to be true}
    end

  end

  describe "#state" do

    let(:stripe_helper) { StripeMock.create_test_helper }

    let(:plan) {
      Stripe::Plan.create(
        :amount => 2000,
        :interval => 'month',
        :name => 'Amazing Gold Plan',
        :currency => 'usd',
        :id => 'gold'
      )
    }

    let(:customer) {
      Stripe::Customer.create({
        email: 'homer@martianinteractive.com',
        source: stripe_helper.generate_card_token,
        plan: plan.id
      })
    }

    let(:subscription) { customer.subscriptions.first }

    let(:account) { create(:account, :with_account_owner, stripe_id: customer.id) }

    it "is nil when the object is initialized" do
      account = build(:account)
      expect(account.state).to be_nil
    end

    it "is active after the oject is created" do
      with_stripe_mock do
        expect(account.state).to eq(Account::ACTIVE)
      end
    end

    it "becomes canceled when the account is canceled" do
      with_stripe_mock do
        expect{ account.cancel! }.to change { account.state }.from(Account::ACTIVE).to(Account::CANCELED)
      end
    end

    it "deletes all the stripe subscriptions" do
      with_stripe_mock do
        # As Account#update_stripe is returning true always in test env, this value is updated manually
        account.update_attributes(stripe_subscription_id: subscription.id)
        expect{account.cancel!}.to change { Stripe::Customer.retrieve(account.stripe_id).subscriptions.count }.from(1).to(0)
      end
    end
  end

  context "trial period" do
    it "sets the value when explicitly set" do
      account = create(:account, trial_period_in_days: 30)
      expect(account.trial_period_in_days).to eq 30
    end

    it "can be set to zero" do
      account = create(:account, trial_period_in_days: 0)
      expect(account.trial_period_in_days).to eq 0
      expect(account.trial_expired?).to eq true
    end

    it "defaults to the environment variable when nil" do
      account = create(:account, trial_period_in_days: nil)
      expect(account.trial_period_in_days).to eq(60)
    end

    it "defaults to the environment variable when empty" do
      account = create(:account, trial_period_in_days: "")
      expect(account.trial_period_in_days).to eq(60)
    end

    describe "on_trail scope - with trial account" do
      let(:account) { create(:account, trial_period_in_days: 30) }

      it { expect(Account.on_trial).to eq [account] }
      it { expect(account.trial_expired?).to eq false }
    end

    describe "on_trail scope - with non trial account" do
      before { create(:account, created_at: Time.now - 65.days) }

      it { expect(Account.on_trial).to eq [] }
    end

  end
end


module StripeHelper
  def with_stripe_mock
    StripeMock.start
    yield
    StripeMock.stop
  end
end

RSpec.configure do |config|
  config.include(StripeHelper)
end
