require 'spec_helper'

describe "Notify" do
  describe "via email" do
    let!(:event) { create(:event, :with_associations, :prefers_email, :remind_now) }

    it "create the log" do
      expect {
      QueueClientConfirmationRequest.run!
      }.to change{ event.email_logs.count }
    end

    it "run the queue job" do
      Delayed::Worker.delay_jobs = true

      expect {
        QueueClientConfirmationRequest.run!
      }.to change { Delayed::Job.count }.from(0).to(1)

      Delayed::Worker.delay_jobs = false
    end

    it "sends an email" do
      expect {
        QueueClientConfirmationRequest.run!
      }.to change { ActionMailer::Base.deliveries.size }.by(1)
    end

    it "sets the completed at timestamp" do
      QueueClientConfirmationRequest.run!
      expect(event.email_logs.count).to eq(1)
      expect(event.email_logs.last.completed_at).to be_present
    end

  end

  describe "via sms" do
    let!(:event) { create(:event, :with_associations, :prefers_sms, :remind_now) }

    it "create the log" do
      expect {
      QueueClientConfirmationRequest.run!
      }.to change{ event.sms_logs.count }
    end

    it "run the queue job" do
      Delayed::Worker.delay_jobs = true

      expect {
        QueueClientConfirmationRequest.run!
      }.to change { Delayed::Job.count }.from(0).to(1)

      Delayed::Worker.delay_jobs = false
    end

    it "sends an sms" do
      expect_any_instance_of(ClientSmsMessenger).to receive(:request_confirmation)
      QueueClientConfirmationRequest.run!
    end

    it "sets the completed at timestamp" do
      QueueClientConfirmationRequest.run!
      expect(event.sms_logs.count).to eq(1)
      expect(event.sms_logs.last.completed_at).to be_present
    end

  end

  describe "via voice" do
    let!(:event) { create(:event, :with_associations, :prefers_voice, :remind_now) }

    it "create the log" do
      expect {
      QueueClientConfirmationRequest.run!
      }.to change{ event.voice_logs.count }
    end

    it "run the queue job" do
      Delayed::Worker.delay_jobs = true

      expect {
        QueueClientConfirmationRequest.run!
      }.to change { Delayed::Job.count }.from(0).to(1)

      Delayed::Worker.delay_jobs = false
    end

    it "sends an sms" do
      expect_any_instance_of(ClientNotificationCaller).to receive(:request_confirmation)
      QueueClientConfirmationRequest.run!
    end

    it "sets the completed at timestamp" do
      QueueClientConfirmationRequest.run!
      expect(event.voice_logs.count).to eq(1)
      expect(event.voice_logs.last.completed_at).to be_present
    end

  end
end

