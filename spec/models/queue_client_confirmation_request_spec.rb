require 'spec_helper'

describe QueueClientConfirmationRequest do
  let(:confirmable_events) do
    QueueClientConfirmationRequest.events_for_confirmation_request
  end

  before do
    create(
      :client,
      :with_associations,
      name: "Sergio Bayona",
      mobile_number: "323-316-4935"
    )
  end

  context "when event is 2 days from now with a 1 hour offset" do
    let!(:event) do
      create(
        :event,
        :with_associations,
        :two_days_from_now_at_9am,
        :prefers_email_sms_and_voice
      )
    end

    context "when now is one day before the event at 7:59am" do
      let(:current_time) { "1 day from now at 7:59am" }

      it "does not pick the reminder yet", tz: "UTC" do
        travel_to(current_time) do
          expect(confirmable_events).to be_empty
        end
      end
    end

    # context "with only one event reminder set for the day before" do
    #   before do
    #     event.event_timings.where(days_before: 0).destroy_all
    #   end

    #   context "when now is one day before the event at 7:59am" do
    #     let(:current_time) { "1 day from now at 7:59am" }

    #     it "does not pick the reminder yet", tz: "UTC" do
    #       travel_to(current_time) do
    #         expect(confirmable_events).to be_empty
    #       end
    #     end
    #   end

    #   context "when now is one day before the event at 8:01am" do
    #     let(:current_time) { "1 day from now at 8:01am" }

    #     it "picks the reminder", tz: "UTC" do
    #       travel_to(current_time) do
    #         expect(confirmable_events).to eql([event])
    #       end
    #     end

    #     it "is confirmable for email reminder (the first option)", tz: "UTC" do
    #       travel_to(current_time) do
    #         expect(confirmable_events.first.reminder_preference).to eql(
    #           Schedule::REMINDER_PREFERENCES[:EMAIL]
    #         )
    #       end
    #     end
    #   end

    #   context "when now is the day of the event at 7:59am", tz: "UTC" do
    #     let(:current_time) { "2 days from now at 7:59am" }

    #     it "does not pick the reminder", tz: "UTC" do
    #       travel_to(current_time) do
    #         expect(confirmable_events).to be_empty
    #       end
    #     end
    #   end

    #   context "when now is the day of the event at 8:01am" do
    #     let(:current_time) { "2 days from now at 8:01am" }

    #     it "does not pick the reminder", tz: "UTC" do
    #       travel_to(current_time) do
    #         expect(confirmable_events).to be_empty
    #       end
    #     end
    #   end
    # end

    describe "when now is the day of the event at 7:59am" do
      let(:current_time) { "2 days from now at 7:59am" }

      it "should not find the reminder for the day before", tz: "UTC" do
        travel_to(current_time) do
          expect(confirmable_events).to be_empty
        end
      end
    end

    describe "when now is the day of the event at 8:01am" do
      let(:current_time) { ("2 days from now at 8:01am") }

      it "finds the event for the day of event reminder", tz: "UTC" do
        travel_to(current_time) do
          expect(confirmable_events).to eql([event])
        end
      end

    end

    describe "running reminders" do
      let(:run) { QueueClientConfirmationRequest.run! }

      context "when now is one day before the event at 8:01am" do
        let(:current_time) { "1 day from now at 8:01am" }

        it "creates a confirmation event log entry for clients when is run", tz: "UTC" do
          travel_to(current_time) do
            expect { run }.to change { EmailLog.confirmations.for_clients.count }.by(1)
          end
        end

        it "timestamps as complete the log entry for clients when it is run", tz: "UTC" do
          travel_to(current_time) do
            run
            expect(EmailLog.confirmations.for_clients.last.completed_at).to be_present
          end
        end

        it "won't have confirmable events after it is run", tz: "UTC" do
          travel_to(current_time) do
            run
            expect(confirmable_events).to be_empty
          end
        end

        context "when the event preferences is blank" do
          before { event.update_attributes(reminder_preferences: []) }

          it "should not pickup the event", tz: "UTC" do
            travel_to(current_time) do
              expect(confirmable_events).to be_empty
            end
          end
        end
      end

      context "when now is the day of the event at 8:01am" do
        let(:current_time) { "2 days from now at 8:01am" }

        it "won't have confirmable events after it is run", tz: "UTC" do
          travel_to(current_time) do
            run
            expect(confirmable_events).to be_empty
          end
        end
      end
    end

    context "when the event is confirmed the first time" do
      before { event.confirm! }

      context "when now is the day of the event at 7:59am" do
        let(:current_time) { "2 days from now at 7:59am" }

        it "doesn't find the event", tz: "UTC" do
          travel_to(current_time) do
            expect(confirmable_events).to be_empty
          end
        end
      end

      context "when now is the day of the event at 8:01am" do
        let(:current_time) { "2 days from now at 8:01am" }

        it "doesn't find the event", tz: "UTC" do
          travel_to(current_time) do
            expect(confirmable_events).to be_empty
          end
        end
      end
    end

    context "when event has passed" do
      let(:current_time) { 'now' }
      let!(:past_event) { create(:event, :with_associations, :yesterday) }

      it "should be a confirmable event", tz: "UTC" do
        travel_to(current_time) do
          expect(confirmable_events).not_to include(past_event)
        end
      end
    end

    context "staggered reminders for each notification method" do
      let(:reminder_preference) do
        confirmable_events.map(&:reminder_preference)
      end

      context "day before at 8:01am" do
        let(:current_time) { "1 day from now at 8:01am" }

        it "is confirmable for email reminder (the first option)", tz: "UTC" do
          travel_to(current_time) do
            expect(reminder_preference).to eql([
              Schedule::REMINDER_PREFERENCES[:EMAIL]
            ])
          end
        end
      end

      context "day before at 8:05" do
        let(:current_time) { "1 day from now at 8:05am" }

        it "picks up email and sms", tz: "UTC" do
          travel_to(current_time) do
            expect(reminder_preference).to eql([
              Schedule::REMINDER_PREFERENCES[:EMAIL], Schedule::REMINDER_PREFERENCES[:SMS]
            ])
          end
        end
      end

      context "day before at 8:10 (ten minutes after the first email)" do
        let(:current_time) { "1 day from now at 8:10am" }

        it "picks up email, sms and voice", tz: "UTC" do
          travel_to(current_time) do
            expect(reminder_preference).to eql([
              Schedule::REMINDER_PREFERENCES[:EMAIL],
              Schedule::REMINDER_PREFERENCES[:SMS],
              Schedule::REMINDER_PREFERENCES[:VOICE]
            ])
          end
        end
      end

      context "day before at 9:01am" do
        let(:current_time) { "1 day from now at 9:01am" }

        it "the first round of notifications is no longer found", tz: "UTC" do
          travel_to(current_time) do
            expect(reminder_preference).to be_empty
          end
        end
      end
    end
  end

  context "when the event is 2 days from now and it has a fixed trigger time of 4pm" do
    let!(:schedule_timing) do
      create(:schedule_timing, :fixed_at_2pm)
    end

    let!(:event) do
      create(
        :event,
        :with_associations,
        :two_days_from_now_at_4pm,
        :prefers_email,
        account_time_zone: 'Tokyo', schedule_timing: schedule_timing
      )
    end

    context "when now is 1 day before the event at 1:30pm" do
      let(:current_time) { "1 day from now at 1:30pm" }

      it "doesn't find the event", tz: "Tokyo" do
        travel_to(current_time) do
          expect(confirmable_events).to be_empty
        end
      end
    end

    context "when now is 1 day before the event at 2:01pm" do
      let(:current_time) { "1 day from now at 2:01pm" }

      it "picks up the event", tz: "Tokyo" do
        travel_to(current_time) do
          expect(confirmable_events).to eql([event])
        end
      end

      it "picks up email reminder preference", tz: "Tokyo" do
        travel_to(current_time) do
          expect(confirmable_events.first.reminder_preference).to eql(
            Schedule::REMINDER_PREFERENCES[:EMAIL]
          )
        end
      end

      context "event with sms voice and email prefrences" do
        before { event.update_attributes(reminder_preferences: ['1', '2', '3']) }

        it "picks up the event", tz: "Tokyo" do
          travel_to(current_time) do
            expect(confirmable_events).to eql([event])
          end
        end
      end
    end

    context "when now is 1 day before the event at 2:30pm" do
      let(:current_time) { "1 day from now at 2:30pm" }

      it "picks the event for email", tz: "Tokyo" do
        travel_to(current_time) do
          expect(confirmable_events).to eql([event])
        end
      end

      context "event now has email, sms and voice preferences", vcr: { :cassette_name => "send_valid_sms" } do
        before { event.update_attributes(reminder_preferences: ['1', '2', '3'])}

        it "picks up the event for email and sms", tz: "Tokyo" do
          travel_to(current_time) do
            expect(confirmable_events).to eql([event, event, event])
          end
        end

        describe "logs after running reminders" do
          let(:run) { QueueClientConfirmationRequest.run! }

          it "creates to event logs", tz: "Tokyo" do
            travel_to(current_time) do
              expect { run }.to change { EventLog.count }.by(3)
            end
          end

          it "creates a log for email", tz: "Tokyo" do
            travel_to(current_time) do
              expect { run }.to change { EmailLog.confirmations.for_clients.count }.by(1)
            end
          end

          it "creates a log for sms", tz: "Tokyo" do
            travel_to(current_time) do
              expect { run }.to change { SmsLog.confirmations.for_clients.count }.by(1)
            end
          end

          it "creats a log for voice", tz: "Tokyo" do
            travel_to(current_time) do
              expect { run }.to change { VoiceLog.confirmations.for_clients.count }.by(1)
            end
          end
        end
      end
    end
  end
end
