require 'spec_helper'

describe "Client with outstanding notifications" do
  let!(:account) { create(:account) }
  let!(:creator) { create(:user, account: account)}
  let!(:schedule) { account.schedules.first }
  let!(:mobile) { "4049695121" }
  let!(:client_one) { create(:client, mobile_number: mobile, account: account, creator: creator) }
  let!(:client_two) { create(:client, mobile_number: mobile, account: account, creator: creator) }
  let!(:event_one) { create(:event, :in_the_future, client: client_one, account: account, user: creator, schedule: schedule) }
  let!(:event_two) { create(:event, :in_the_future, client: client_two, account: account, user: creator, schedule: schedule)}

  before do
    event_one.sms_logs.create(sent_at: Date.yesterday, delivered_at: Date.yesterday)
    event_one.sms_logs.create(sent_at: Date.yesterday, delivered_at: Date.today)
  end

  it "returns one client" do
    expect(Client.where(mobile_number: client_one.mobile_number).with_outstanding_sms_notification.count).to eq 1
  end

  it "returns the client with the outstanding event" do
    expect(Client.where(mobile_number: client_one.mobile_number).with_outstanding_sms_notification.first).to eq client_one
  end
end
