require 'spec_helper'

describe CallerId do
  it { is_expected.to belong_to(:schedule) }

  it { is_expected.to validate_presence_of(:number) }
  it { is_expected.to validate_presence_of(:schedule) }

  it { is_expected.to callback(:twilio_verify!).before(:create) }
  it { is_expected.to callback(:twilio_destroy!).before(:destroy) }

  context "is valid" do
    let(:caller_id) { create(:caller_id, :verified) }

    it { expect(caller_id.verified).to eq true }
    it { expect(caller_id.verified_at).to be_present }
    it { expect(caller_id.twilio_sid).to be_present }
  end

  # context "is invalid" do
  #   let(:caller_id) { create(:caller_id, :)}
  # end
end
