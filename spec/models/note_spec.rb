require 'spec_helper'

describe Note do
  it { is_expected.to belong_to(:account) }
  it { is_expected.to belong_to(:event) }

  it { is_expected.to validate_presence_of(:description) }
  it { is_expected.to validate_presence_of(:account) }
  it { is_expected.to validate_presence_of(:event) }

end
