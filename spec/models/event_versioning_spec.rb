require 'spec_helper'

describe Event do
  describe "on creation" do
    let!(:event) { create(:event, :with_associations) }
    let(:first_version) { event.versions.first }

    it { expect(event.versions.count).to be(1) }

    it { expect(first_version.event).to eq 'create' }

    it { expect(first_version.action).to be_nil }
  end

  describe "on reschedule" do
    let!(:event) { create(:event, :with_associations) }

    before { event.update_attributes(start_time: event.start_time + 1.hour) }

    it { expect(event.versions.last.event).to eq 'update' }

    it { expect(event.versions.last.action).to eq 'reschedule' }
  end

end
