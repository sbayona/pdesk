require 'spec_helper'

describe Event, "SMS Scripts" do
  let!(:account) { create(:account) }
  let!(:user) { create(:user, account: account) }
  let!(:client) { create(:client, creator: user, account: account) }
  let!(:schedule) { account.schedules.first }
  let!(:event) { create(:event, user: user, client: client, account: account, schedule: schedule) }

  it "sms confirmation request" do
    expect(event.sms_confirmation_request_message.strip!).to eq %Q{
      A reminder from #{account.name}. You have an appointment on #{event.start_time.to_s(:event_date)} at #{event.start_time.to_s(:event_time)}. \nText back #{Event::CONFIRMATION_CODE[:CONFIRMED]} to confirm, #{Event::CONFIRMATION_CODE[:CANCELED]} to cancel, or #{Event::CONFIRMATION_CODE[:CALLBACK]} for us to call you.
    }.strip!
  end

  it "sms confirmed reply" do
    expect(event.sms_confirmation_message.strip!).to eq %Q{
      Thank you for confirming your appointment. We look forward to serving you.
    }.strip!
  end

  it "sms canceled reply" do
    expect(event.sms_cancellation_message.strip!).to eq %Q{
      Your request to cancel the appointment was received.
    }.strip!
  end

  it "sms callback reply" do
    expect(event.sms_callback_response_message.strip!).to eq %Q{
      We'll be in touch with you soon.
    }.strip!
  end
end
