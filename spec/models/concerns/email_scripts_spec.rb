require 'spec_helper'

describe Event, "Email Scripts" do
  let!(:account) { create(:account) }
  let!(:user) { create(:user, account: account) }
  let!(:client) { create(:client, creator: user, account: account) }
  let!(:schedule) { account.schedules.first }
  let!(:event) { create(:event, start_time: DateTime.parse('2001-02-03T04:05:06+07:00'), user: user, client: client, account: account, schedule: schedule) }

  it "email confirmation request" do
    expect(event.email_confirmation_request_message.strip!).to eq %Q{
<h1>You have an appointment.</h1>

<p>Hi #{client.first_name},</p>
<p>You have an appointment at #{account.name} on #{event.start_time.to_s(:event_date)} at #{event.start_time.to_s(:event_time)}.</p>
    }.strip!
  end

  it "email confirmation subject" do
    expect(event.email_confirmation_request_subject.strip!).to eq %Q{
      Your appointment with #{account.name} on #{event.start_time.to_s(:event_date)} from #{event.start_time.to_s(:event_time)}
    }.strip!
  end

  it "email cancellation notification reply" do
    expect(event.email_cancellation_notification_message.strip!).to eq %Q{
<h1>Your appointment has been canceled.</h1>

<p>hi James Doe,</p>
<p>Your appointment at #{account.name} on #{event.start_time.to_s(:event_date)} at #{event.start_time.to_s(:event_time)} as been canceled.</p>
<p>If you have any questions or would like to reschedule, please call us at .</p>
    }.strip!
  end

  it "email cancellation notification subject" do
    expect(event.email_cancellation_notification_subject.strip!).to eq %Q{
      Cancellation Notice: Your appointment with #{account.name} on #{event.start_time.to_s(:event_date)} has been canceled.
    }.strip!
  end

  describe "reschedule" do

    before do
      event.update_attributes(start_time: event.start_time + 1.hour)
    end

    it "email reschedule notification subject" do
      expect(event.email_reschedule_notification_subject.strip!).to eq %Q{
        Reschedule Notice: Your appointment with #{account.name} on #{event.start_time.to_s(:event_date)} has been rescheduled.
      }.strip!
    end

    it "email reschedule notification message on the same day" do
      expect(event.email_reschedule_notification_message.strip!).to eq %Q{
<h1>Your appointment has been rescheduled.</h1>

<p>hi #{client.first_name},</p>
<p>Your appointment with #{account.name} on #{event.old_start_time.to_s(:short_date)} at #{event.old_start_time.to_s(:event_time)} has been rescheduled to #{event.new_start_time.to_s(:event_time)}.</p>
<p>If you have any questions or would like to reschedule, please call us at 404-969-5121.</p>
      }.strip!
    end

  end


end
