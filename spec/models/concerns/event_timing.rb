require 'spec_helper'

describe Event, "Event Timing Concern" do
  let!(:account) { create(:account) }
  let!(:schedule) { account.schedules.first }
  let!(:schedule_timing) { schedule.default_timing }

  let(:event) { create(:event, schedule: schedule) }

  context "when one of the schedule timing options has a null 'days_before'" do
    before do
      schedule_timing.schedule_timing_options.first.update_attributes(days_before: nil)
    end

    it "there should be one event timing" do
      expect(event.event_timings.count).to eq 1
    end
  end

  it "creates event_timing records when an event is created" do
    expect {
      event
    }.to change(EventTiming, :count).by(2)
  end

  it { expect(event.time_zone.name).to eq(event.account.time_zone) }
  it { expect(event.default_timing).to eq schedule.default_timing }
  it { expect(event.schedule_timing).to eq schedule.default_timing }
  it { expect(event.schedule_timings).to eq schedule.schedule_timings }


  describe "when #schedule_timing_id is explicitly set" do
    let!(:schedule_timing) { create(:schedule_timing, :fixed_at_2pm) }
    let!(:event) { create(:event, schedule_timing_id: schedule_timing.id) }

    it "the event should take the specific schedule_timing_id" do
      expect(event.schedule_timing_id).to eq(schedule_timing.id)
    end
  end

  describe "when #schedule_timing_id is NOT explicitly set" do
    let!(:event) { create(:event) }

    it "the event will pick up the schedule's default timing id" do
      expect(event.schedule_timing).to be_present
    end
  end

  context "when the event is created through an event series" do
    it "won't have event_timings but it will create them if updated" do
      event.event_timings.destroy_all
      event.start_time = Time.now + 2.hours
      expect {event.save}.to change { event.event_timings.count }.from(0).to(2)
    end
  end
end
