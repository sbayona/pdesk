require 'spec_helper'

describe Event, "Voice Scripts" do
  let!(:account) { create(:account) }
  let!(:user) { create(:user, account: account) }
  let!(:client) { create(:client, creator: user, account: account) }
  let!(:schedule) { account.schedules.first }
  let!(:event) { create(:event, user: user, client: client, account: account, schedule: schedule) }

  it "voice confirmation request" do
    expect(event.voice_confirmation_request_message.strip!).to eq %Q{
      Hello. This is #{account.name}. \nWe want to remind you about your appointment on #{event.start_time.to_s(:event_date)} at #{event.start_time.to_s(:event_time)}.\nPlease press #{Event::CONFIRMATION_CODE[:CONFIRMED]} to confirm. #{Event::CONFIRMATION_CODE[:CANCELED]} to cancel. Or #{Event::CONFIRMATION_CODE[:CALLBACK]} for us to call you back.
    }.strip!
  end

  it "voice confirmed reply" do
    expect(event.voice_confirmation_message.strip!).to eq %Q{
      Thank you for confirming your appointment. We look forward to serving you. Good bye.
    }.strip!
  end

  it "voice canceled reply" do
    expect(event.voice_cancellation_message.strip!).to eq %Q{
      Your request to cancel the appointment was received. Good bye.
    }.strip!
  end

  it "voice callback reply" do
    expect(event.voice_callback_response_message.strip!).to eq %Q{
      Thank you! We'll be in touch with you soon. Good bye.
    }.strip!
  end
end
