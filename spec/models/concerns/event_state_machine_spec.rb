describe Event, "States" do
  let!(:schedule) { create(:schedule, :with_account, length: 30) }
  let!(:creator) { create(:user, account: schedule.account) }
  let!(:client) {  create(:client, name: "Laura Bush", creator: creator) }
  let!(:start_time) { Time.zone.now }

  let(:valid_event_attributes) { {start_time: start_time, schedule_id: schedule.id, account: schedule.account, user: creator, client_attributes: {name: client.name} } }

  subject { Event.new(valid_event_attributes) }

  describe "confirm!" do
    let(:confirm!) { subject.confirm! }

    it "delivers email notification to subscribers" do
      expect_any_instance_of(Event).to receive(:email_subscribers_event_confirmation!)
      confirm!
    end

    it "sets the confirmed_at timestamp" do
      expect { confirm! }.to change { subject.confirmed_at }
    end

    it "changes the state" do
      expect { confirm! }.to change { subject.state }.from(nil).to('confirmed')
    end

    it "does not send sms reply" do
      expect_any_instance_of(Event).to_not receive(:respond_to_sms_confirmation_request!)
      confirm!
    end

    it "state_changed? is true" do
      expect { confirm! }.to change { subject.state_changed?}.from(nil).to(true)
    end

    describe "by the client via sms" do

      before do
        subject.updated_via = Event::UPDATE_VIA[:SMS]
        PaperTrail.whodunnit = 'client'
      end

      it "delivers sms notification to client" do
        expect_any_instance_of(Event).to receive(:respond_to_sms_confirmation_request!)
        confirm!
      end

    end
  end

  describe "cancel!" do
    let(:cancel!) { subject.cancel! }

    it "sets the confirmed_at timestamp" do
      expect { cancel! }.to change { subject.canceled_at }
    end

    it "changes the state" do
      expect { cancel! }.to change { subject.state }.from(nil).to('canceled')
    end

    it "delivers notification to subscribers" do
      expect_any_instance_of(Event).to receive(:email_subscribers_event_cancellation!)
      cancel!
    end

    it "state_changed? is true" do
      expect { cancel! }.to change { subject.state_changed?}.from(nil).to(true)
    end

    describe "when canceled by the user" do
      it "delivers notification to user" do
        PaperTrail.whodunnit = creator.id
        expect_any_instance_of(Event).to receive(:email_client_event_cancellation!)
        cancel!
      end
    end

    describe "when canceled by the client via sms" do

      before do
        subject.updated_via = Event::UPDATE_VIA[:SMS]
        PaperTrail.whodunnit = 'client'
      end

      it "sends cancelation notification email to subscribers" do
        expect_any_instance_of(Event).to receive(:email_subscribers_event_cancellation!)
        cancel!
      end
    end
  end

  describe "show!" do
    let(:show!) { subject.show! }

    it "sets the showed_at timestamp" do
      expect { show! }.to change { subject.showed_at }
    end

    it "changes the state" do
      expect { show! }.to change { subject.state }.from(nil).to('show')
    end

    it "state_changed? is true" do
      expect { show! }.to change { subject.state_changed?}.from(nil).to(true)
    end
  end

  describe "no_show!" do
    let(:no_show!) { subject.no_show! }

    it "sets the showed_at timestamp" do
      expect { no_show! }.to change { subject.no_showed_at }
    end

    it "changes the state" do
      expect { no_show! }.to change { subject.state }.from(nil).to('no_show')
    end

    it "state_changed? is true" do
      expect { no_show! }.to change { subject.state_changed?}.from(nil).to(true)
    end
  end

  describe "wen call back request" do
    let(:callback!) { subject.callback! }

    it "sets the callback_at timestamp" do
      expect { callback! }.to change { subject.callback_at }
    end

    it "changes the state" do
      expect { callback! }.to change { subject.state }.from(nil).to('callback')
    end

    it "state_changed? is true" do
      expect { callback! }.to change { subject.state_changed?}.from(nil).to(true)
    end

    it "should email subscribers" do
      expect_any_instance_of(Event).to receive(:email_subscribers_request_to_callback!)
      callback!
    end

    it "should sms back the client" do
      PaperTrail.whodunnit = 'client'
      subject.updated_via = Event::UPDATE_VIA[:SMS]
      expect_any_instance_of(Event).to receive(:respond_to_sms_callback_request!)
      callback!
    end
  end

end
