require 'spec_helper'

describe Event do
  it { is_expected.to belong_to(:schedule) }
  it { is_expected.to belong_to(:client) }
  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:account) }

  it { is_expected.to validate_presence_of(:schedule) }
  it { is_expected.to validate_presence_of(:account) }
  it { is_expected.to validate_presence_of(:client) }
  it { is_expected.to validate_presence_of(:user) }
  it { is_expected.to validate_presence_of(:start_time) }

  let!(:schedule) { create(:schedule, :with_account, length: 30) }
  let!(:creator) { create(:user, account: schedule.account) }
  let!(:client) {  create(:client, name: "Laura Bush", creator: creator) }
  let!(:start_time) { Time.zone.now }
  let!(:valid_event_attributes) { {start_time: start_time, schedule_id: schedule.id, account: schedule.account, user: creator, client_attributes: {name: client.name, mobile_number: "404-345-1234"} } }

  subject { Event.create!(valid_event_attributes) }

  it "has an event status" do
    expect(subject.status).to be_a(EventStatus)
  end

  context "when there are no schedule timings" do
    before { ScheduleTiming.delete_all }

    let(:event) { build(:event) }

    it "should return a base error when creating a new event" do
      event.valid?
      expect(event.errors[:base].first).to eq "At least one timing entry is required"
    end

  end

  context "short code" do
    it "" do
      event = Event.new(valid_event_attributes)
      expect {
        event.save!
      }. to change { event.short_code }.from(nil).to be_a(String)
    end
  end

  context "#in_the_future #in_the_past" do
    let!(:past_event) { create(:event, :in_the_past, :with_associations) }
    let!(:future_event) { create(:event, :in_the_future, :with_associations)}

    it { expect(Event.in_the_future).to_not include(past_event)}
    it { expect(Event.in_the_future).to include(future_event) }

    it { expect(Event.in_the_past).to_not include(future_event) }
    it { expect(Event.in_the_past).to include(past_event) }
  end

  context "if unavailable time" do
    let(:unavailable_event) { Event.new(unavailable_time: true) }
    it { expect(unavailable_event).to_not validate_presence_of(:client) }
    it { expect(unavailable_event).to_not validate_presence_of(:schedule_timing) }
  end

  context "end_time" do
    it "is set per schedule setttings" do
      event = Event.new(valid_event_attributes)
      event.valid?
      expect(event.end_time).to eq (start_time + schedule.length.minutes)
    end

    it "is not set when start_time is blank" do
      event = Event.new(valid_event_attributes.merge(start_time: nil))
      event.valid?
      expect(event.end_time).to be_nil
    end
  end

  context "expired time" do
    it "is true when time is past" do
      start_time = Time.now - 5.minutes
      event= Event.new(start_time: start_time)
      expect(event.expired_time).to eq true
    end

    it "is nil when time is future" do
      start_time = Time.now + 10.minutes
      event = Event.new(start_time: start_time)
      expect(event.expired_time).to be_nil
    end
  end

  context "creating an event" do
    context "when the client exists" do
      it "assigns the client" do
        expect(subject.client_id).to eq client.id
      end

      it "does not create a new client" do
        expect do
          Event.create!(valid_event_attributes)
        end.to_not change(Client, :count)
      end

      it "touches the client's scheduled_at timestamp" do
        expect do
          Event.create!(valid_event_attributes)
        end.to change {client.reload.scheduled_at}
      end
    end

    context "when the client does not exist" do
      let(:valid_event_with_new_client_attributes) { valid_event_attributes.merge(client_attributes: {name: "Peter Jones", mobile_number: "555-555-5555"}) }

      it "creates a new client record" do
        expect do
          Event.create!(valid_event_with_new_client_attributes)
        end.to change(Client, :count).by(1)
      end

      it "should set the client's schedule_at timestamp" do
        Event.create!(valid_event_with_new_client_attributes)
        expect(Client.last.scheduled_at).to_not be_nil
      end
    end

    context "without explicit script group" do
      it "stores the schedule's default script group" do
        expect(subject.script_group).to_not be_blank
        expect(subject.script_group).to eq subject.schedule.default_script_group
      end
    end

    context "with explicit script group" do
      let!(:another_script_group) { schedule.script_groups.create(ScriptGroup.default_values) }

      it "stores the provided script_group id" do
        event = Event.create!(valid_event_attributes.merge(script_group_id: another_script_group.id))
        expect(event.script_group_id).to_not be_blank
        expect(event.script_group).to eq another_script_group
      end
    end
  end

  describe "deltas" do
    context "day delta" do
      it "forwards the day" do
        subject.day_delta = 3
        subject.save!
        expect(subject.start_time).to eq start_time + 3.days
        expect(subject.end_time).to eq start_time + schedule.length.send(:minutes) + 3.days
      end

      it "rewinds the day" do
        subject.day_delta = -4
        subject.save!
        expect(subject.start_time).to eq start_time - 4.days
        expect(subject.end_time).to eq start_time + schedule.length.send(:minutes) - 4.days
      end
    end

    context "minute delta" do
      it "forwards the minutes" do
        subject.minute_delta = 3
        subject.save!
        expect(subject.start_time).to eq start_time + 3.minutes
        expect(subject.end_time).to eq start_time + schedule.length.send(:minutes) + 3.minutes
      end

      it "rewinds the minutes" do
        subject.minute_delta = -4
        subject.save!
        expect(subject.start_time).to eq start_time - 4.minutes
        expect(subject.end_time).to eq start_time + schedule.length.send(:minutes) - 4.minutes
      end
    end
  end

  describe "on create" do
    let!(:valid_event) { Event.new(valid_event_attributes) }

    it "delivers notification" do
      expect_any_instance_of(Event).to receive(:email_subscribers_event_creation!)
      valid_event.save!
    end
  end

  describe "when client responds with unknown code via sms" do
    let!(:valid_event) { Event.new(valid_event_attributes) }

   let(:unknown_reply) do
      subject.unknown_reply
    end

    before do
      subject.updated_via = Event::UPDATE_VIA[:SMS]
      subject.save!
    end

    it "system responds with error message" do
      expect_any_instance_of(Event).to receive(:respond_to_sms_unknown_reply!)
      unknown_reply
    end
  end

  describe "#notification_subscribers" do
    let!(:user) { create(:user, account: schedule.account, email: 'peter@prefersemail.com') }
    let!(:notification) { create(:schedule_subscription, user: user, schedule: schedule) }
    let!(:event) { Event.create!(valid_event_attributes) }

    it "returns users assigned to the schedule" do
      expect(event.notification_subscribers).to eq [user]
    end

    it "has_notification_subscribers?" do
      expect(event.has_notification_subscribers?).to eq true
    end

    describe "without subscribers" do
      before { ScheduleSubscription.destroy_all }

      it "returns no subscribers" do
        expect(event.notification_subscribers).to be_blank
      end

      it "has_notification_subscribers?" do
        expect(event.has_notification_subscribers?).to eq false
      end
    end
  end

  describe "versioning module" do
    # it "is nil when created" do
    #   expect(subject.updater).to be_nil
    # end

    it "returns false when it hasn't been rescheduled" do
      # expect(subject.rescheduled?).to eq false
      # pending refactor
    end

    context "rescheduling the event" do
      def with_frozen_time(time = nil)
        time ||= subject.start_time + 1.hour

        PaperTrail.whodunnit = creator.id
        Timecop.freeze do
          subject.update_attributes(start_time: time)
          yield
        end
      end

      it "true when it was been rescheduled" do
        with_frozen_time do
          expect(subject.rescheduled?).to eq true
        end
      end

      it "is present after it is udpated" do
        with_frozen_time do
          expect(subject.updater).to be_present
        end
      end

      it "returns the old start time" do
        with_frozen_time do
          expect(subject.old_start_time.to_s).to eq (subject.start_time - 1.hour).to_s
        end
      end

      it "returns the new stasrt time" do
        with_frozen_time do
          expect(subject.new_start_time.to_s).to eq subject.start_time.to_s
        end
      end

      describe "when the start time has been moved to another day" do
        it do
          time = subject.start_time + 2.days

          with_frozen_time(time) do
            expect(subject.on_the_same_day?).to eq false
          end
        end
      end
    end
  end

  describe "#creator" do
    it "has a creator when created" do
      expect(subject.creator).to be_present
    end
  end

  describe "#state" do
    it "is nil when the object is initialized" do
      event = Event.new(valid_event_attributes)
      expect(event.state).to be_nil
    end

    it "#unknown_reply returns an twilio SMS" do
      expect(subject.unknown_reply[:data][:body]).to eq "Sorry, the number you enter was incorrect. Please try again."
    end

    it "is pending after the oject is created" do
      expect(subject.state).to eq "pending"
    end

    context "from pending to confirmed" do
      it "changes the state" do
        expect do
          subject.confirm!
        end.to change { subject.confirmation.state }.from("pending").to("confirmed")
      end

      it "sets the timestamp" do
        Timecop.freeze do
          expect do
            subject.confirm!
          end.to change { subject.confirmed_at }.from(nil).to(Time.now)
        end
      end
    end

    context "from pending to no show" do
      it "changes the state" do
        expect do
          subject.no_show!
        end.to change { subject.confirmation.state }.from("pending").to("no_show")
      end

      it "sets the timestamp" do
        Timecop.freeze do
          expect do
            subject.no_show!
          end.to change { subject.no_showed_at }.from(nil).to(Time.now)
        end
      end
    end

    context "from pending to show" do
      it "changes the state" do
        expect do
          subject.show!
        end.to change { subject.confirmation.state }.from("pending").to("show")
      end

      it "sets the timestamp" do
        Timecop.freeze do
          expect do
            subject.show!
          end.to change { subject.showed_at }.from(nil).to(Time.now)
        end
      end
    end

    context "from pending to canceled" do
      it "changes the state" do
        expect do
          subject.cancel!
        end.to change { subject.confirmation.state }.from("pending").to("canceled")
      end

      it "sets the timestamp" do
        Timecop.freeze do
          expect do
            subject.cancel!
          end.to change { subject.canceled_at }.from(nil).to(Time.now)
        end
      end

      it { expect(subject.no_show!).to eq true }
      it { expect(subject.show!).to eq true }
    end

    context "from confirmed to no show" do
      before { subject.confirm! }

      it "changes the state" do
        expect do
          subject.no_show!
        end.to change { subject.confirmation.state }.from("confirmed").to("no_show")
      end

      it "sets the timestamp" do
        Timecop.freeze do
          expect do
            subject.no_show!
          end.to change { subject.no_showed_at }.from(nil).to(Time.now)
        end
      end
    end

    context "from confirmed to show" do
      before { subject.confirm! }

      it "changes the state" do
        expect do
          subject.show!
        end.to change { subject.confirmation.state }.from("confirmed").to("show")
      end

      it "sets the timestamp" do
        Timecop.freeze do
          expect do
            subject.show!
          end.to change { subject.showed_at }.from(nil).to(Time.now)
        end
      end
    end
  end

  describe "#available_transitions" do
    context "when pending" do
      it { expect(subject.available_transitions).to eq ["confirmed", "callback", "canceled", "show", "no_show"] }
    end

    context "when confirmed" do
      before { subject.confirm! }
      it { expect(subject.available_transitions).to eq ["canceled", "show", "no_show"] }
    end

    context "when cancel" do
      before { subject.cancel! }
      it { expect(subject.available_transitions).to be_empty }
    end

    context "when no_show" do
      before { subject.no_show! }
      it { expect(subject.available_transitions).to be_empty }
    end

    context "when show" do
      before { subject.show! }
      it { expect(subject.available_transitions).to be_empty }
    end
  end

  describe "#set_repeat_limit" do
    it "sets the limit to one day after start time" do
      expect(subject.start_time).to be_present
      expect(subject.until_date).to eq subject.start_time + 1.day
    end
  end

  describe "#email_client_event_reminder!" do
    it "sends an email to the client" do
      expect {subject.email_client_event_reminder!}.to change { ActionMailer::Base.deliveries.size }.by(1)
    end

    context "when client opted out from receiving emails" do
      before { subject.client.update_attributes(receive_email: false) }

      it "does not deliver email" do
        expect {subject.email_client_event_reminder!}.to_not change { ActionMailer::Base.deliveries.size }
      end
    end
  end

  describe "#reminder preferences" do
    it { expect(subject.reminder_preferences).to eq [] }
  end

  describe "#notify_via?" do
    %w{email sms voice}.each do |method|
      it { expect(subject.notify_via?(method)).to eq false }
    end

    it { expect{ subject.notify_via?(:bla)}.to raise_error("event can't do BLA")}

    describe "all methods enabled" do
      before { subject.update_attributes(reminder_preferences: %w{1 2 3}) }

      %w{email sms voice}.each do |method|
        it { expect(subject.notify_via?(method)).to eq true }
      end
    end
  end

  describe "#set_reschedule_timestamp" do
    it "set the reschedule_at timestamp when the start time changes" do
      Timecop.freeze do
        expect do
          subject.update_attributes(start_time: Time.now + 1.day)
        end.to change {subject.reload.rescheduled_at.to_i}.from(0).to(Time.now.to_i)
      end
    end

    it "set the reschedule_at timestamp when the end_time changes" do
      Timecop.freeze do
        expect do
          subject.update_attributes(end_time: Time.now + 1.day)
        end.to change {subject.reload.rescheduled_at}
      end
    end

    it "does not set the reschedule_at timestamp" do
      expect do
        subject.update_attributes(title: 'test')
      end.to_not change { subject.reload.rescheduled_at }
    end
  end

  it "has no contact info" do
    attrs = {start_time: start_time, schedule_id: schedule.id, account: schedule.account, user: creator, client_attributes: {name: "John Creek"}}
    event = Event.create(attrs)
    expect(event.has_no_contact_info).to eq true
  end
end
