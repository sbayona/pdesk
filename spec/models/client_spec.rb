require 'spec_helper'

describe Client do
  it { is_expected.to have_many(:events) }
  it { is_expected.to have_many(:schedules).through(:events) }
  it { is_expected.to belong_to(:account) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:account) }

  it { is_expected.to callback(:set_account).before(:validation).on(:create) }
  it { is_expected.to callback(:generate_token).before(:create) }

  let(:client_name) {"James O'hare"}
  let(:client) { create(:client, :with_associations, name: client_name) }

  it { expect(client.first_name).to eq "James" }
  it { expect(client.last_name).to eq "O'hare" }

  it "validates uniqueness of client name" do
    expect(build(:client)).to validate_uniqueness_of(:name).scoped_to(:account_id).case_insensitive
  end

  describe "mobile phone" do
    it "allows blanks" do
      client.mobile_number = ""
      expect(client).to be_valid
    end

    it "catches invalid numbers" do
      client.mobile_number = "6660000455050505050"
      client.valid?
      expect(client.errors["mobile_number"]).to eq ['is an invalid number']
    end

    it "normalizes the number" do
      client.mobile_number = "323-316-4935"
      client.save!
      expect(client.mobile_number).to eq "+13233164935"
    end

    it "stores it as it is" do
      client.mobile_number = "13233164935"
      client.save!
      expect(client.mobile_number).to eq "+13233164935"
    end
  end

  describe 'client events' do
    let(:past_event) {create(:event, :without_client_with_user, :in_the_past, client: client)}
    let(:future_event) {create(:event, :without_client_with_user, :in_the_future, client: client)}

    context '.upcoming_events' do
      it { expect(client.upcoming_events).to eq([future_event])}
    end

    context '.recent_events' do
      it {expect(client.past_events).to eq([past_event])}
    end
  end

  it "#missing_contact_info?" do
    client = Client.new
    expect(client.missing_contact_info?).to eq true
  end

end
