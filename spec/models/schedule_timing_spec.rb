require 'spec_helper'

describe ScheduleTiming do
  let!(:account) { create(:account) }
  let!(:schedule) { account.schedules.first }

  subject { schedule.schedule_timings.first }

  it { is_expected.to belong_to(:schedule) }
  it { is_expected.to validate_presence_of(:name) }

  it { is_expected.to callback(:set_first_record_as_default).before(:validation) }

  context "when a schedule is created" do
    it { expect(subject.options).to be_present }
    it { expect(subject.schedule).to eq schedule }
    it { expect(subject.default).to eq true }
    it { expect(subject.name).to be_present }
  end

  context "when a second reminder timing is created for a schedule" do
      let!(:second_schedule_timing) { create(:schedule_timing, :fixed_at_2pm) }

      it "there should always be one default per schedule" do
        expect(schedule.schedule_timings.where(default: true).count).to eq(1)
      end

      it "the default reminder timing should continue to be the first one" do
        expect(schedule.schedule_timings.where(default: true)).to eq([subject])
      end

      context "when a second reminder timing is set as default" do
        before { second_schedule_timing.update_attributes(default: true) }

        it { expect(schedule.schedule_timings.count).to eq(2) }
        it { expect(schedule.schedule_timings.where(default: true).count).to eq(1) }
        it { expect(schedule.schedule_timings.where(default: true)).to eq([second_schedule_timing]) }
      end
  end

  context "options" do
    it { expect(subject.options.size).to eq(2) }

    it { expect(subject.options.first).to eq({
        "value"=>60,
        "days_before"=>0
      })}

    it { expect(subject.options.last).to eq({
        "value"=>60,
        "days_before"=>1
      })}
  end
end
