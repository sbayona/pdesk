require 'spec_helper'

describe User do
 it { is_expected.to belong_to(:account) }
 it { is_expected.to have_many(:events) }
 it { is_expected.to have_many(:clients) }
 it { is_expected.to have_many(:schedule_subscriptions).dependent(:destroy) }
 it { is_expected.to have_many(:schedules).through(:schedule_subscriptions) }
 it { is_expected.to have_many(:demos) }

 it { is_expected.to validate_presence_of(:password) }
 it { is_expected.to validate_presence_of(:email) }
 it { is_expected.to validate_presence_of(:account) }
 it { is_expected.to validate_presence_of(:first_name) }
 it { is_expected.to validate_presence_of(:last_name) }
 it { is_expected.to validate_uniqueness_of(:email).case_insensitive }

 it { expect(subject.email).to be_blank }
 it { expect(subject.reset_password_token).to be_blank}
 it { expect(subject.reset_password_sent_at).to be_nil }
 it { expect(subject.remember_created_at).to be_nil }
 it { expect(subject.sign_in_count).to be_zero }
 it { expect(subject.failed_attempts).to be_zero }
 it { expect(subject.unlock_token).to be_nil }
 it { expect(subject.locked_at).to be_nil }
 it { expect(subject.account_owner).to eq false }
 it { expect(subject.role).to be_nil }

 it { is_expected.to delegate_method(:state).to(:account) }

 describe "#name" do
  let!(:user) { create(:user, :with_account, first_name: "Peter", last_name: "Doe") }

  it "responds to name" do
    expect(user.name).to eq("Peter Doe")
  end
 end

 describe "#full_name=" do
   let!(:user) { create(:user, :with_account, full_name: "Peter Vanbuster")}

   it { expect(user.first_name).to eq("Peter") }
   it { expect(user.last_name).to eq("Vanbuster")}
 end

 context "invite" do
  context "with missing attributes" do
    it "fails validation" do
      invited_user = User.invite!({}, nil)
      expect(invited_user.errors[:email]).to be_present
      expect(invited_user.errors[:first_name]).to be_present
      expect(invited_user.errors[:last_name]).to be_present
      expect(invited_user.errors[:account]).to be_present
      expect(invited_user.errors[:password]).not_to be_present
      expect(invited_user.errors[:password_confirmation]).not_to be_present
    end
  end
 end

end
