require 'spec_helper'

describe EventDecorator do
  subject(:event) do
    EventDecorator.new(
      create(:event, :with_associations, :prefers_email_and_sms)
    )
  end

  describe '#confirmed_via' do
    it 'returns reminder preference information' do
      expect(event.confirmed_via).to eql('EMAIL, SMS')
    end
  end
end
