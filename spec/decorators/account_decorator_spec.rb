require 'spec_helper'

describe AccountDecorator do
  date = 60.days.ago
  let(:account) { build_stubbed(:account, trial_period_in_days: 60, created_at: date).decorate }

  it 'returns the formatted trial period end date' do
    expect(account.trial_period_end_date).to eq Date.today.strftime("%B %d, %Y")
  end

  it 'returns the trial period end date on days' do
    expect(account.trial_period_end_date_on_days).to eq('0 days')
  end

  it "returns the proper string" do
    Timecop.freeze(59.days.ago) do
      expect(account.trial_period_end_date_on_days).to eq('59 days')
    end
  end
end
