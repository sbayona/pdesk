require 'spec_helper'

describe SubscriptionPlanDecorator do
  let(:subscription_plan) { build_stubbed(:subscription_plan, created_at: DateTime.new(2015,01,21)).decorate }

  it 'returns the formatted trial period end date' do
    expect(subscription_plan.formatted_price).to eq('$100')
  end
end
