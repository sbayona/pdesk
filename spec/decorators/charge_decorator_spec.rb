require 'spec_helper'

describe ChargeDecorator do
  let(:charge) { build_stubbed(:charge).decorate }

  it 'returns the amount as a currency' do
    expect(charge.amount_with_currency).to eq('$100.50 USD')
  end

  it 'returns the card information' do
    expect(charge.card_info).to eq('Visa **** **** **** 1234')
  end

  it 'returns the charge date formatted' do
    expect(charge.charged_at).to eq('2015-01-20')
  end

end
