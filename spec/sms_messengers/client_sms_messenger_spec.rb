require "spec_helper"

describe ClientSmsMessenger do
  let(:event) { create(:event, :pending, :with_sms_log, :with_associations) }
  let!(:sms_log) { event.sms_logs.first }

  describe "event confirmation request", vcr: { cassette_name: "sms_confirmation_request" } do

    let(:sms) { ClientSmsMessenger.new(event).request_confirmation(sms_log.id) }

    it { expect(sms.has_key?(:data)).to eq true}
    it { expect(sms[:data][:to]).to eq event.client.mobile_number }
    it { expect(sms[:data][:body]).to include("Text back 1 to confirm, 5 to cancel, or 9 for us to call you") }
    it { expect(sms[:data][:sid].present?).to eq true }

    it { expect{sms}.to change {event.versions.count }.by(1) }

    describe "custom event versioning" do
      before { sms }

      it "is a reminder" do
        expect(event.versions.last.action).to eq 'reminder_msg'
      end

      it "for the client" do
        expect(event.versions.last.target).to eq 'client'
      end

      it "via sms" do
        expect(event.versions.last.via).to eq 'sms'
      end

      it "has the number where the sms went" do
        expect(event.versions.last.info).to eq '+13233164935'
      end
    end
  end

  describe "event cancellation", vcr: { cassette_name: "sms_cancellation" } do
    let(:sms) { ClientSmsMessenger.new(event).respond_to_cancelation_request }

    it { expect(sms.has_key?(:data)).to eq true }

    it { expect(sms[:data][:to]).to eq event.client.mobile_number }
    it { expect(sms[:data][:body]).to eq "Your request to cancel the appointment was received."}
    it { expect(sms[:data][:sid].present?).to eq true }

    describe "custom event versioning" do
      before { sms }

      it "is a reminder" do
        expect(event.versions.last.action).to eq 'cancelation_reply'
      end

      it "for the client" do
        expect(event.versions.last.target).to eq 'client'
      end

      it "via sms" do
        expect(event.versions.last.via).to eq 'sms'
      end

      it "has the number where the sms went" do
        expect(event.versions.last.info).to eq '+13233164935'
      end
    end
  end

  describe "event confirmation", vcr: { cassette_name: "sms_confirmation" } do
    let(:sms) { ClientSmsMessenger.new(event).respond_to_confirmation_request }

    it { expect(sms.has_key?(:data)).to eq true }
    it { expect(sms[:data][:to]).to eq event.client.mobile_number }
    it { expect(sms[:data][:body]).to eq "Thank you for confirming your appointment. We look forward to serving you." }
    it { expect(sms[:data][:sid].present?).to eq true }

    describe "custom event versioning" do
      before { sms }

      it "is a reminder" do
        expect(event.versions.last.action).to eq 'confirmation_reply'
      end

      it "for the client" do
        expect(event.versions.last.target).to eq 'client'
      end

      it "via sms" do
        expect(event.versions.last.via).to eq 'sms'
      end

      it "has the number where the sms went" do
        expect(event.versions.last.info).to eq '+13233164935'
      end
    end
  end

  describe "event callback", vcr: { cassette_name: "sms_callback" } do
    let(:sms) { ClientSmsMessenger.new(event).respond_to_callback_request }

    it { expect(sms.has_key?(:data)).to eq true }
    it { expect(sms[:data][:to]).to eq event.client.mobile_number }
    it { expect(sms[:data][:body]).to eq "We'll be in touch with you soon." }
    it { expect(sms[:data][:sid].present?).to eq true }

    describe "custom event versioning" do
      before { sms }

      it "is a reminder" do
        expect(event.versions.last.action).to eq 'callback_reply'
      end

      it "for the client" do
        expect(event.versions.last.target).to eq 'client'
      end

      it "via sms" do
        expect(event.versions.last.via).to eq 'sms'
      end

      it "has the number where the sms went" do
        expect(event.versions.last.info).to eq '+13233164935'
      end
    end
  end

  describe "event unkown reply", vcr: { cassette_name: "sms_unknown" } do
    let(:sms) { ClientSmsMessenger.new(event).respond_to_unknown_reply }

    it { expect(sms.has_key?(:data)).to eq true }
    it { expect(sms[:data][:to]).to eq event.client.mobile_number }
    it { expect(sms[:data][:body]).to eq "Sorry, the number you enter was incorrect. Please try again." }
    it { expect(sms[:data][:sid].present?).to eq true }

    describe "custom event versioning" do
      before { sms }

      it "is a reminder" do
        expect(event.versions.last.action).to eq 'unknown_reply'
      end

      it "for the client" do
        expect(event.versions.last.target).to eq 'client'
      end

      it "via sms" do
        expect(event.versions.last.via).to eq 'sms'
      end

      it "has the number where the sms went" do
        expect(event.versions.last.info).to eq '+13233164935'
      end
    end
  end

  describe "time zone" do

    it "changes when the ClientSmsMessenger is initialized" do
      event = instance_double("Event", time_zone: "Amsterdam", reload: true)
      
      expect { 
        ClientSmsMessenger.new(event) 
        }.to change { Time.zone.name }.from('UTC').to('Amsterdam')
    end
  end

end
