FactoryGirl.define do
  factory :event_timing do
    days_before 1
    offset_in_seconds 3600
    schedule_timing_option

    trait :no_offset do
      offset_in_seconds 0
    end

    trait :one_hour_offset do
      offset_in_seconds 3600
    end

    trait :two_hours_offset do
      offset_in_seconds 7200
    end

    trait :day_before do
      days_before 1
    end

    trait :day_of do
      days_before 0
    end
  end
end
