FactoryGirl.define do
  factory :schedule do
    name "test schedule"

    trait :with_account do
      before(:create) { |schedule| schedule.account = create(:account) }
    end

    trait :with_subscriber do
      after(:create) { |schedule| schedule.subscribers << create(:user) }
    end

    trait :with_existing_subscriber do
      after(:create) { |schedule| schedule.subscribers << User.first }
    end

    trait :with_events do
      transient do
        number_of_events 3
      end

      after :create do |schedule, evaluator|
        FactoryGirl.create_list :event, evaluator.number_of_events, :with_existing_client, :with_existing_user, schedule: schedule, account: schedule.account
      end
    end
  end
end
