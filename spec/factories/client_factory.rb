FactoryGirl.define do
  factory :client do
    name { "James Doe #{rand(1000)}"}
    sequence :email do |n|
      "email#{n}@factory.com"
    end
    mobile_number "13233164935"

    trait :with_associations do
      before(:create) { |client|
        account = create(:account)
        client.account = account
        user = create(:user, account: account)
        client.creator = user
      }
    end

    trait :with_account do
      before(:create) { |client| client.account = create(:account) }
    end

    trait :with_creator do
      before(:create) { |client| client.creator = create(:user) }
    end

    trait :with_future_pending_event do
      after(:create) { |client| create(:event, :in_the_future, client: client) }
    end
  end
end
