FactoryGirl.define do
  factory :schedule_timing do
    name "test timing"
    before(:create) { |u| u.schedule = Schedule.first || create(:schedule) }

    trait :fixed_at_2pm do
      options [{value: "14:00", days_before: 0}, {value: "14:00", days_before: 1}]
    end
  end
end
