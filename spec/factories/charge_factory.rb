FactoryGirl.define do
  factory :charge do
    amount 100.50
    currency "usd"
    customer "cus_5WgyzEp70eg5Yg"
    description "test"
    statement "test"
    card_last4 "1234"
    card_brand "Visa"
    card_exp_month 1
    card_exp_year 2020
    card_country "US"
    charged_at "2015-01-20 12:20:28"
    account nil
  end

end
