FactoryGirl.define do
  factory :caller_id do
    number { generate(:phone_number) }
    friendly_name "Duluth Health Clinic"
    association :schedule, factory: [:schedule, :with_account]

    trait :verified do
      verified_at Time.zone.now - 1.day
      verified true
      twilio_sid "abcd1345"
    end

    trait :unverified do
      verified_at nil
      verified nil
      twilio_sid nil
    end

    trait :failed_verification do
      verified_at Time.zone.now - 1.day
      verified false
      twilio_sid nil
    end

  end
end
