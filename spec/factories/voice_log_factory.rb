FactoryGirl.define do
  factory :voice_log do
    trait :sent do
      delivered_at Time.now.utc - 1.day
      callback_payload "sent"
    end
  end
end
