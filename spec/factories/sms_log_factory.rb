FactoryGirl.define do
  factory :sms_log do
    trait :sent do
      delivered_at Time.now.utc - 1.day
      sent_at Time.now.utc - 10.minutes
      callback_payload "sent"
    end
  end
end
