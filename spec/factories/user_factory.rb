FactoryGirl.define do
  factory :user do
    first_name "James"
    last_name  "Doe"
    sequence(:email) {|n| "email#{n}@factory.com" }
    password "abcd1234"
    password_confirmation "abcd1234"

    factory :admin do
      role 'admin'
      mobile_number "+14049695121"
    end

    trait :with_account do
      account
    end

    trait :with_canceled_account do
      before(:create) { |user| user.account = create(:account, :canceled) }
    end

    trait :with_demos do
      before(:create) do |user|
        user.demos.build(greeting: File.open(Rails.root.join("spec/files/test.mp3")))
      end
    end
  end

  factory :account_owner do
    first_name "Peter"
    last_name "Van Buster"
    sequence(:email) { |n| "email#{n}@test.com" }
    password "abcd1234"
    password_confirmation "abcd1234"
    role 'admin'

    trait :with_account do
      before(:create) { |user| user.account = create(:account) }
    end
  end
end
