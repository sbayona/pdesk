FactoryGirl.define do
  factory :subscription_plan do
    sequence(:code) { |n| "abcd#{n}" }
    sequence(:name) { |n| "somename#{n}"}
    price_in_cents 10000
    currency "USD"
    active true
    max_users 1
    max_events_per_month 100
  end
end
