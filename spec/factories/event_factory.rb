FactoryGirl.define do

  factory :event do
    start_time { 2.days.from_now }

    transient do
      account_time_zone "UTC"
    end

    trait :no_validate do
      to_create {|instance| instance.save(validate: false) }
    end

    trait :with_user do
      before(:create) { |event| event.user = create(:user) }
    end

    trait :with_client do
      before(:create) { |event| event.client = create(:client) }
    end

    trait :with_schedule do
      before(:create) { |event| event.schedule = create(:schedule) }
    end

    trait :with_account do
      before(:create) { |event| event.account = create(:account) }
    end

    trait :with_existing_client do
      before(:create) { |event| event.client = Client.first }
    end

    trait :with_existing_user do
      before(:create) {|event| event.user = User.first }
    end

    trait :without_client do
      before(:create) do |event, evaluator|
        client = evaluator.client
        user = evaluator.user

        account = user.account
        schedule = create(:schedule, account: account)

        event.account = account
        event.user = user
        event.client = client
        event.schedule = schedule
      end
    end

    trait :without_client_with_user do
      before(:create) do |event, evaluator|
        account = create(:account, time_zone: evaluator.account_time_zone)
        user = create(:user, account: account)
        client = evaluator.client

        schedule = create(:schedule, account: account)

        event.account = account
        event.user = user
        event.client = client
        event.schedule = schedule
      end
    end

    trait :without_user do
      before(:create) do |event, evaluator|
        user = evaluator.user
        client = create(:client, creator: user)
        schedule = create(:schedule, account: user.account)

        event.account = user.account
        event.client = client
        event.schedule = schedule
      end
    end

    trait :without_schedule do
      before(:create) do |event, evaluator|
        user = evaluator.user
        schedule = evaluator.schedule
        client = create(:client, creator: user)

        event.account = user.account
        event.client = client
        event.schedule = schedule
      end
    end

    trait :with_associations do
      before(:create) do |event, evaluator|
        account = create(:account, time_zone: evaluator.account_time_zone)
        user = create(:user, account: account)
        client = create(:client, creator: user)
        schedule = create(:schedule, account: account)
        event.account = account
        event.user = user
        event.client = client
        event.schedule = schedule
      end
    end

    trait :in_the_future do
      title 'tomorrow'
      start_time { Chronic.parse("tomorrow") }
    end

    trait :in_the_past do
      title 'yesterday'
      start_time { Chronic.parse("yesterday") }
    end

    trait :two_days_from_now_at_9am do
      start_time { Chronic.parse("2 days from now at 9am") }
    end

    trait :two_days_from_now_at_345pm do
      start_time { Chronic.parse("2 days from now at 3:45pm") }
    end

    trait :two_days_from_now_at_350pm do
      start_time { Chronic.parse("2 days from now at 3:50pm") }
    end

    trait :two_days_from_now_at_355pm do
      start_time { Chronic.parse("2 days from now at 3:55pm") }
    end

    trait :two_days_from_now_at_4pm do
      start_time { Chronic.parse("2 days from now at 4pm") }
    end

    trait :yesterday do
      start_time { Chronic.parse("yesterday") }
    end

    trait :from_3_weeks_ago do
      start_time { Chronic.parse("3 weeks ago") }
    end

    trait :remind_now do
      start_time { Chronic.parse("40 minutes from now") }
    end

    trait :prefers_email do
      reminder_preferences ["1"]
    end

    trait :prefers_email_and_sms do
      reminder_preferences ["1", "2"]
    end

    trait :prefers_email_sms_and_voice do
      reminder_preferences ["1", "2", "3"]
    end

    trait :prefers_sms do
      reminder_preferences ["2"]
    end

    trait :prefers_voice do
      reminder_preferences ["3"]
    end

    trait :with_sms_log do
      after(:create) { |e| e.sms_logs << [create(:sms_log)] }
    end

    trait :with_email_log do
      after(:create) { |e| e.event_logs << [create(:event_log)] }
    end

    trait :with_voice_log do
      after(:create) { |e| e.voice_logs << [create(:voice_log)] }
    end

    trait :with_outstanding_sms_confirmation_request do
      state "pending"
      after(:create) { |e| e.sms_logs << [create(:sms_log, :sent)]}
    end

    trait :with_schedule_subscriber do
      after(:create) {|e| e.schedule.subscribers << User.first }
    end

    trait :pending do
      state "pending"
    end

    trait :confirmed do
      state "confirmed"
    end

    trait :canceled do
      state "canceled"
    end

    trait :showed do
      state "show"
    end

    trait :no_showed do
      state "no_show"
    end

    trait :rescheduled do
      rescheduled_at Time.now.utc - 5.minutes
    end
  end
end
