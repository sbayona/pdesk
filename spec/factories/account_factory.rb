FactoryGirl.define do
  sequence :name do |n|
    "name#{n}"
  end

  factory :account do
    name { generate(:name) }
    client_name "client"
    event_name "appointment"
    time_zone "Eastern Time (US & Canada)"
    last_4_digits "1234"
    stripe_token "abcd1234"
    phone "404-969-5121"

    trait :with_subscription do
      subscription_plan
    end

    trait :canceled do
      state "canceled"
    end

    trait :with_account_owner do
      after(:create) { |account| create(:account_owner, account: account) }
    end

    trait :expired_trial do
      trial_period_in_days 0
    end

  end
end
