FactoryGirl.define do
  sequence :phone_number do |n|
    "+1404#{n}452342"
  end
end
