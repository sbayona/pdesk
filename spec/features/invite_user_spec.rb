require 'spec_helper'

describe "the user invitation process", :type => :feature do
  let!(:admin) { create(:admin, :with_account) }
  let!(:invitee_email) { "test@test.com" }

  before { login(admin) }

  it "visits the users page and adds new user" do
    visit users_path
    expect(page).to have_link "Add New User"
    click_link "Add New User"
    expect(page).to have_content "Invite a new user to your PracticalDesk account"
  end

  it "fills and submits the form", js: true do
    fillout_invitation
    expect(page).to have_content "An invitation email has been sent to #{invitee_email}"
  end

  it "sends the invitation email" do
    fillout_invitation
    email = ActionMailer::Base.deliveries.last
    expect(email.subject).to eq("You're invited to join PracticalDesk")
    expect(email.to).to eq([invitee_email])
  end

  def fillout_invitation
    visit new_user_invitation_path
    fill_in "First name", with: "Joe"
    fill_in "Last name", with: "Doe"
    fill_in "Email", with: invitee_email
    click_button "Send invitation"
  end
end
