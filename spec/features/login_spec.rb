require 'spec_helper'

describe "the login process", :type => :feature, js: true do

  before { visit '/sign_in'}

  it "gets the signup page" do
    expect(page).to have_content "Sign in to continue"
    expect(page).to have_selector "input[id=email]"
    expect(page).to have_selector "input[id=password]"
  end

  context "with incomplete params" do

    it "fails with errors" do
      click_button "Sign in"

      expect(page).to have_content "Invalid email or password"
    end
  end

  context "with valid params"  do
    let!(:user) { create(:user, :with_account) }

    it "displays successful message" do
      login_with(user)
      expect(page).to have_content "Sample Schedule"
    end

    context "when loging out" do
      it "redirects back to the login page" do
        login_with(user)
        logout(user)
        expect(page).to have_content "You need to sign in or sign up before continuing."
      end
    end
  end
end
