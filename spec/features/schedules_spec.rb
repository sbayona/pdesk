require 'spec_helper'

describe "Schedule", :type => :feature do

  let!(:account)  { create :account, :with_account_owner }
  let!(:owner)    { account.account_owner }
  let!(:client)   { create(:client, creator: owner, account: account)}
  let!(:schedule) { create :schedule, :with_events, account: account }

  before { login(owner) }

  it "update schedule info" do
    visit edit_schedule_path(schedule)

    within(".schedule-form") do
      fill_in "Name", with: "Dr. Montes"

      expect(page).to have_field("schedule_name", with: "Dr. Montes")
    end
  end

  it "try to update schedule with invalid data" do
    visit edit_schedule_path(schedule)

    within(".schedule-form") do
      fill_in "Name", with: ""
      click_button "Update Schedule"
    end

    expect(page).to have_content "schedule name can't be blank"
  end

end
