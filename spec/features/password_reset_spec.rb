require 'spec_helper'

describe "the password reset process", :type => :feature do

  before { visit '/sign_in'}

  it "has a link to reset password" do
    expect(page).to have_link "I forgot my username/password"
  end

  describe "submit the form" do
    let!(:user) { create(:user, :with_account) }

    before { visit "/users/password/new" }

    it "shows the reset instructions" do
      expect(page).to have_content "Enter your email address below and we'll send you password reset instructions."
    end

    it "redirects to the sign-in page" do
      fill_out_password_reset_form
      expect(page.current_path).to eq "/sign_in"
    end

    it "sends the reset password email" do
      fill_out_password_reset_form
      email = ActionMailer::Base.deliveries.last
      expect(email.subject).to eq('Reset password instructions')
      expect(email.to).to eq([user.email])
    end
  end

  def fill_out_password_reset_form
    fill_in "email", with: user.email
    click_button "Send me reset password instructions"
  end

end
