require 'spec_helper'

describe "Event Notes CRUD", :type => :feature do
  
  let!(:account)  { create :account, :with_account_owner }
  let!(:owner)    { account.account_owner }
  let!(:client)   { create(:client, creator: owner, account: account)}
  let!(:schedule) { create :schedule, :with_events, account: account }  
  let!(:event)    { create(:event, user: owner, account: account, client: client, schedule: schedule) }
  let!(:note)     { create :note, account: account, event: event }

  before { login(owner) }
  
  scenario "adds new note", js: true do
    visit event_path(event)

    within("#new_note") do
      fill_in "note_description", with: "First Note"
      click_button "Create Note"
    end

    expect(page).to have_text("First Note")
  end

  scenario "edit a note", js: true do
    visit event_path(event)

    click_link "Edit"
    within(".edit_note") do
      fill_in "Description", with: "First Note Edited"
      click_button "Update Note"
    end
    
    expect(page).to have_text("First Note Edited")
  end

  scenario "delete a note", js: true do
    visit event_path(event)

    click_link "Delete"
    page.driver.browser.accept_js_confirms
    
    expect(page).not_to have_text("test description")
  end

end
