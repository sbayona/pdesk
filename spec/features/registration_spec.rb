require 'spec_helper'

feature "the registration process", type: :feature do
  let!(:subscription_plan) { create(:subscription_plan) }

  background do
    ActionMailer::Base.deliveries.clear
  end

  scenario "gets registered!", js: true do
    visit '/register'

    fill_in "account_name", with: "Acme Inc"
    fill_in "owner_full_name", with: "Peter Doe"
    fill_in "owner_email", with: "peter@test.com"
    fill_in "owner_password", with: "abcd1234"

    click_button "Start my free trial"

    expect(page).to have_content "Welcome! You have signed up successfully."

  end
end
