require 'spec_helper'

feature 'Reports' do
  def create_event(*args)
    create(:event, :without_user, *args)
  end

  let!(:admin) { create(:admin, :with_account) }

  let!(:event_1) { create_event(:with_email_log, user: admin, title: 'Email event') }
  let!(:event_2) { create_event(:with_sms_log, user: admin, title: 'SMS event') }
  let!(:event_3) { create_event(:with_voice_log, user: admin, title: 'Voice event') }

  let!(:event_other) { create_event(user: create(:admin, :with_account), title: 'Other email event') }

  background do
    login(admin)
  end

  scenario 'As a user I can view event report', js: true do
    visit(root_path)
    click_on 'Reports'

    within('.events') do
      expect(page).to have_content(event_1.title)
      expect(page).to have_content(event_2.title)
      expect(page).to have_content(event_3.title)

      expect(page).to_not have_content(event_other.title)
    end
  end

  scenario 'as a user i can filter reports by schedule', js: true do
    visit(root_path)
    click_on 'Reports'

    find('#q_schedule_id').set(event_2.schedule.id)

    click_on 'Filter'

    within('.events') do
      expect(page).to have_content(event_2.title)

      expect(page).to_not have_content(event_1.title)
      expect(page).to_not have_content(event_3.title)
      expect(page).to_not have_content(event_other.title)
    end

    expect(find('#q_schedule_id').value).to eql(event_2.schedule.id.to_s)
  end

  scenario 'as a user i can filter reports by client and status', js: true do
    client = create(:client, creator: admin)
    event = create(:event, :without_client, user: admin, client: client)

    event.confirm!

    visit(root_path)
    click_on 'Reports'

    find('#q_client_id').set(client.id)
    find('#q_state').set('confirmed')

    click_on 'Filter'

    within('.events') do
      expect(page).to have_content(event.title)

      expect(page).to_not have_content(event_1.title)
      expect(page).to_not have_content(event_2.title)
      expect(page).to_not have_content(event_3.title)

      expect(page).to_not have_content(event_other.title)
    end

    expect(find('#q_client_id').value).to eql(client.id.to_s)
    expect(find('#q_state').value).to eql('confirmed')
  end

  scenario 'As a user I can filter reports by start time using date-range picker', js: true do
    event = create_event(user: admin, start_time: Date.tomorrow)

    visit(root_path)
    click_on 'Reports'

    find(:css, '#reportrange').click
    find('li', text: 'Tomorrow').click

    click_on 'Filter'

    within('.events') do
      expect(page).to have_content(event.title)

      expect(page).to_not have_content(event_1.title)
      expect(page).to_not have_content(event_2.title)
      expect(page).to_not have_content(event_3.title)

      expect(page).to_not have_content(event_other.title)
    end
  end
end
