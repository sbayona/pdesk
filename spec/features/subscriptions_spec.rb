require 'spec_helper'

describe "Subscriptions", :type => :feature do

  let!(:account)  { create :account, :with_account_owner }
  let!(:owner)    { account.account_owner }
  let!(:client)   { create(:client, creator: owner, account: account)}
  let!(:schedule) { create :schedule, :with_events, account: account }
  let!(:user)     { create :user, account: account }
  let!(:user2)    { create :user, account: account, first_name: "Tom", last_name: "Sawyer" }
  let!(:schedule_subscription) { create :schedule_subscription, user: user, schedule: schedule }

  before { login(owner) }

  scenario "create subscription", js: true do
    visit schedule_schedule_subscriptions_path(schedule_id: schedule.id)

    click_link "Add user to notify"
    select user2.name, from: "schedule_subscription_user_id"
    click_button "Subscribe to Schedule"

    within("ul#usersToNotify") do
      expect(page).to have_content user2.name
    end
  end

  scenario "try to update schedule with invalid data", js: true do
    visit schedule_schedule_subscriptions_path(schedule_id: schedule.id)

    within("ul#usersToNotify") do
      expect(page).to have_content user.name
    end

    find(:css, 'a.trash').trigger('click')

    within("ul#usersToNotify") do
     expect(page).not_to have_content user.name
    end

  end

end
