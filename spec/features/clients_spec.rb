require 'spec_helper'

describe "Clients", type: :feature do
  let!(:admin) { create(:admin, :with_account) }

  before { login(admin) }

  describe "client creation", js: true do
    it "creates a client and redirects to the client's page" do
      visit new_client_path

      fillout_client_form
      expect(page).to have_content "Client was successfully created"
    end
  end

  describe "client update", js: true do
    let!(:client) { create(:client, creator: admin) }

    it "creates a client and redirects to the client's page" do
      visit edit_client_path(client)

      fill_in "Name", with: "Tom"
      click_button "Update Client"
      expect(page).to have_content "Client was successfully updated"
    end
  end

  def fillout_client_form
    fill_in "Name", with: "Joe"
    fill_in "Email", with: "joe@test.com"
    click_button "Create Client"
  end
end
