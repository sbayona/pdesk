require 'spec_helper'

describe "Appointments", :type => :feature do

  let!(:account)  { create :account, :with_account_owner }
  let!(:owner)    { account.account_owner }
  let!(:client)   { create(:client, creator: owner, account: account)}
  let!(:schedule) { create :schedule, :with_events, account: account }
  let!(:event)    { create(:event, :prefers_email, user: owner, account: account, client: client, schedule: schedule) }

  before { login(owner) }

  scenario "click on schedule creates modal dialog", js: true do
    visit root_path(schedule: schedule)

    first_hour = find(".fc-slot0")
    first_hour.find(".fc-widget-content").click

    expect(page).to have_css('div.modal-dialog')
    expect(page).to have_content('New Appointment')
  end

  scenario "create event with existing client", js: true do
    visit root_path(schedule: schedule)

    first_hour = find(".fc-slot0")
    first_hour.find(".fc-widget-content").click
    fill_in "Client Name", with: client.name
    click_button "Save changes"

    within "#calendar" do
      expect(page).to have_content(client.name)
      expect(page).to have_content("Pending Response")
    end
  end

  scenario "get error when create event without existing client", js: true do
    visit root_path(schedule: schedule)

    first_hour = find(".fc-slot0")
    first_hour.find(".fc-widget-content").click

    fill_in "Client Name", with: "Tom Sawyer"
    click_button "Save changes"

    expect(page).to have_content("Whoops, we need at least an email and/or mobile number")
  end

  # scenario "create event without existing client", js: true do
  #   visit root_path(schedule: schedule)

  #   first_hour = find(:css, ".fc-slot0")
  #   first_hour.find(:css, ".fc-widget-content").click

  #   fill_in "Client Name", with: "Tom Sawyer"
  #   find(:css, "a#toggleContactFields").click()
  #   fill_in "Email", with: "tom@sawyer.com"
  #   click_button "Save changes"

  #   within "#calendar" do
  #     expect(page).to have_content("Tom Sawyer")
  #     expect(page).to have_content("Pending Response")
  #   end
  # end


end
