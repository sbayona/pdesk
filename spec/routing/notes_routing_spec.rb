require "spec_helper"

describe NotesController do
  describe "routing" do

    it "routes to #create" do
      expect(:post => "/events/1/notes").to route_to("notes#create", :event_id => '1')
    end

  end
end
