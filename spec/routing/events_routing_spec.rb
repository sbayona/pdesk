require "spec_helper"

describe EventsController do
  describe "routing" do

    it "routes to #index" do
      expect(get("/events")).to route_to("events#index")
    end

    it "routes to #new" do
      expect(post("/events/new")).to route_to("events#new")
    end

    it "routes to #edit" do
      expect(get("/events/1/edit")).to route_to("events#edit", :id => "1")
    end

    it "routes to #create" do
      expect(post("/events")).to route_to("events#create")
    end

    it "routes to #update" do
      expect(put("/events/1")).to route_to("events#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(delete("/events/1")).to route_to("events#destroy", :id => "1")
    end

    it "confirm" do
      expect(get("/events/1/confirm")).to route_to("events#confirm", :id => "1")
    end

    it "cancel" do
      expect(get("/events/1/cancel")).to route_to("events#cancel", :id => "1")
    end

    it "show" do
      expect(get("/events/1/showed")).to route_to("events#showed", :id => "1")
    end

    it "no_show" do
      expect(get("/events/1/no_show")).to route_to("events#no_show", :id => "1")
    end

  end
end
