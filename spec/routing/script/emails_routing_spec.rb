require "spec_helper"

describe Script::EmailsController do
  describe "routing" do


    it "routes to #edit" do
      expect(get("/schedules/1/script/emails/1/edit")).to route_to("script/emails#edit", schedule_id: "1", id: "1")
    end

    it "routes to #update" do
      expect(put("/schedules/1/script/emails/1")).to route_to("script/emails#update", schedule_id: "1", id: "1")
    end

  end
end
