require "spec_helper"

describe StripesController do
  describe "routing" do

    it "routes to #charge" do
      expect(:post => "/stripe/charge.xml").to route_to("stripes#charge", :format => 'xml')
    end

  end
end
