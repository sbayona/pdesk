require "spec_helper"

describe ScheduleTimingsController do
  describe "routing" do

    it "routes to #index" do
      expect(get("/schedules/1/schedule_timings")).to route_to("schedule_timings#index", :schedule_id => "1")
    end

    it "routes to #new" do
      expect(get("/schedules/1/schedule_timings/new")).to route_to("schedule_timings#new", :schedule_id => "1")
    end

    it "routes to #show" do
      expect(get("/schedules/1/schedule_timings/1")).to route_to("schedule_timings#show", :id => "1", :schedule_id => "1")
    end

    it "routes to #edit" do
      expect(get("/schedules/1/schedule_timings/1/edit")).to route_to("schedule_timings#edit", :id => "1", :schedule_id => "1")
    end

    it "routes to #create" do
      expect(post("/schedules/1/schedule_timings")).to route_to("schedule_timings#create", :schedule_id => "1")
    end

    it "routes to #update" do
      expect(put("/schedules/1/schedule_timings/1")).to route_to("schedule_timings#update", :id => "1", :schedule_id => "1")
    end

    it "routes to #destroy" do
      expect(delete("/schedules/1/schedule_timings/1")).to route_to("schedule_timings#destroy", :id => "1", :schedule_id => "1")
    end

  end
end
