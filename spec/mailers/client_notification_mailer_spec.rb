require "spec_helper"

describe ClientNotificationsMailer do

  let(:event) { create(:event, :with_associations) }
  let(:client) { event.client }


  context "reminder message" do
    let(:email) { ClientNotificationsMailer.reminder_message(event.id).deliver_now }

    it { expect(email).to deliver_from ENV['DEFAULT_SENDER_EMAIL'] }
    
    it { expect(email).to have_reply_to ENV['DEFAULT_REPLY_TO_EMAIL'] }

    it { expect(email).to have_subject(/Your appointment with #{event.account.name}/) }


    it "creates an event version" do
      expect { email }.to change {event.versions.count }.by(1)
    end

    describe "custom event versioning" do
      before { email }

      it "is a reminder" do
        expect(event.versions.last.action).to eq 'reminder_msg'
      end

      it "for the client" do
        expect(event.versions.last.target).to eq 'client'
      end

      it "via email" do
        expect(event.versions.last.via).to eq 'email'
      end

      it "has the number where the email went" do
        expect(event.versions.last.info).to eq event.email_recipient
      end
    end

    it "delivers to client" do
      expect(email).to deliver_to("#{client.name} <#{client.email}>")
    end

    context "when no client email" do
      before { client.update_attributes(email: '') }

      it "will not send an email" do
        expect(email).to be_nil
      end
    end

    context "contains cancelation links" do
      it { expect(email).to have_body_text(/Please confirm your attendance/) }
      it { expect(email).to have_body_text(/Confirm/) }
      it { expect(email).to have_body_text(/Cancel/) }
    end

    it "contains the unsubscribe link" do
      expect(email).to have_body_text(/To stop receiving reminder emails from #{event.account.name}/) 
    end
  end

  context "cancellation" do
    let(:email) { ClientNotificationsMailer.notify_cancellation(event.id).deliver_now }

    it { expect(email).to deliver_from ENV['DEFAULT_SENDER_EMAIL'] }
    
    it { expect(email).to have_reply_to ENV['DEFAULT_REPLY_TO_EMAIL'] }

    it { expect(email).to have_subject(/Cancellation Notice: Your appointment with #{event.account.name}/) }

    it "delivers to client" do
      expect(email).to deliver_to("#{client.name} <#{client.email}>")
    end

    it "creates an event version" do
      expect { email }.to change {event.versions.count }.by(1)
    end

    describe "custom event versioning" do
      before { email }

      it "is a cancelation notice" do
        expect(event.versions.last.action).to eq 'cancelation_notice'
      end

      it "for the client" do
        expect(event.versions.last.target).to eq 'client'
      end

      it "via email" do
        expect(event.versions.last.via).to eq 'email'
      end

      it "has the number where the email went" do
        expect(event.versions.last.info).to eq event.email_recipient
      end
    end
  end

  context "reschedule" do
    let(:email) { ClientNotificationsMailer.notify_reschedule(event.id).deliver_now }

    it { expect(email).to deliver_from ENV['DEFAULT_SENDER_EMAIL'] }
    
    it { expect(email).to have_reply_to ENV['DEFAULT_REPLY_TO_EMAIL'] }

    it { expect(email).to have_subject(/Reschedule Notice: Your appointment with #{event.account.name}/) }

    it "delivers to client" do
      expect(email).to deliver_to("#{client.name} <#{client.email}>")
    end

    it "creates an event version" do
      expect { email }.to change {event.versions.count }.by(1)
    end

    describe "custom event versioning" do
      before { email }

      it "is a notice" do
        expect(event.versions.last.action).to eq 'reschedule_notice'
      end

      it "for the client" do
        expect(event.versions.last.target).to eq 'client'
      end

      it "via email" do
        expect(event.versions.last.via).to eq 'email'
      end

      it "has the number where the email went" do
        expect(event.versions.last.info).to eq event.email_recipient
      end
    end
  end
end
