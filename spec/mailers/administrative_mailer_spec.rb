require "spec_helper"

describe AdministrativeMailer do

  let!(:account_owner) { create(:account_owner, :with_account) }
  let!(:email) { AdministrativeMailer.signup_notification(account_owner).deliver_now }

  it { expect(email).to deliver_to(ENV['NEW_ACCOUNTS_EMAIL']) }
  it { expect(email).to deliver_from(ENV['DEFAULT_SENDER_EMAIL']) }
  it { expect(email).to have_body_text /#{account_owner.name}/ }
  it { expect(email).to have_body_text /#{account_owner.email}/ }
  it { expect(email).to have_body_text /#{account_owner.account.name}/ }
  it { expect(email).to have_body_text /#{account_owner.subscription_plan_name}/ }
  it { expect(email).to have_body_text /#{account_owner.trial_period_ends_on}/ }
end
