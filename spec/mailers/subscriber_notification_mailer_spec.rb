require "spec_helper"

shared_examples "a user notification email" do
  it "deliver from valid email" do
    expect(email).to deliver_from ENV['DEFAULT_SENDER_EMAIL']
  end

  it "delivers to user who prefers email" do
    expect(email).to deliver_to(subscriber.email)
  end

  it "creates an event version" do
    expect { email }.to change { event.versions.count }.by 1
  end

  it "makes email delivery" do
    expect { email }.to change { ActionMailer::Base.deliveries }
  end
end

describe SubscriberNotificationsMailer do

  let!(:schedule) { create(:schedule, :with_account) }
  let!(:account) { schedule.account }

  let!(:subscriber) { create(:user, account: account, email: 'peter@prefersemail.com') }
  let!(:subscription) { create(:schedule_subscription, user: subscriber, schedule: schedule) }

  let!(:client) { create(:client, name: "Peter Schey", creator: subscriber) }
  let!(:script_group) { schedule.script_groups.first }
  let!(:event) { create(:event, account: account, schedule: schedule, user: subscriber, script_group: script_group, client_attributes: { name: client.name }) }

  context "event created" do
    it_behaves_like "a user notification email" do
      let(:email) { SubscriberNotificationsMailer.notify_created(event.id).deliver_now }
    end
  end

  context "event confirmation" do
    it_behaves_like "a user notification email" do
      let(:email) { SubscriberNotificationsMailer.notify_confirmed(event.id).deliver_now }
    end
  end

  context "event cancellation" do
    it_behaves_like "a user notification email" do
      let(:email) { SubscriberNotificationsMailer.notify_canceled(event.id).deliver_now }
    end
  end

  context "event update" do
    before do
      event.update_attributes(start_time: Time.now + 3.days)
    end

    it_behaves_like "a user notification email" do
      let(:email) { SubscriberNotificationsMailer.notify_updated(event.id).deliver_now }
    end
  end
end


describe SubscriberNotificationsMailer, "without subscribers" do
  let!(:schedule) { create(:schedule, :with_account) }
  let!(:account) { schedule.account }

  let!(:user) { create(:user, account: account) }

  let!(:client) { create(:client, name: "Peter Schey", creator: user) }
  let!(:script_group) { schedule.script_groups.first }
  let!(:event) { create(:event, account: account, schedule: schedule, user: user, script_group: script_group, client_attributes: { name: client.name }) }


  let(:email) { SubscriberNotificationsMailer.notify_created(event.id).deliver_now }

  it "" do
    expect {email}.to_not change { ActionMailer::Base.deliveries }
  end
end
