require 'spec_helper'

describe QueueClientConfirmationRequest, '.query' do
  def self.create_event(name, *types)
    let!(name) { create(:event, :with_associations, *types) }
  end

  let(:events) do
    QueueClientConfirmationRequest.query(offset, log_type, reminder_preference)
  end

  let(:vars) { QueueClientConfirmationRequest::QUERY_VARIABLES }
  let(:email_vars) { vars[:EMAIL] }

  context "EMAIL type" do
    let(:offset) { email_vars[:OFFSET] }
    let(:log_type) { email_vars[:LOG_TYPE] }
    let(:reminder_preference) { email_vars[:REMINDER_PREFERENCE] }

    context "when event is 2 days from now with a 1 hour offset" do
      create_event(:event_0, :in_the_past, :prefers_email)
      create_event(:event_1, :two_days_from_now_at_345pm, :prefers_email)
      create_event(:event_2, :two_days_from_now_at_350pm, :prefers_email_and_sms)
      create_event(:event_3, :two_days_from_now_at_355pm, :prefers_sms)
      create_event(:event_4, :two_days_from_now_at_355pm, :prefers_voice)
      create_event(:event_5, :two_days_from_now_at_350pm, :prefers_email_sms_and_voice)
      create_event(:event_6, :two_days_from_now_at_355pm, :prefers_email)
      create_event(:event_7, :two_days_from_now_at_4pm, :prefers_email)
      create_event(:event_8, :two_days_from_now_at_4pm, :prefers_email)
      create_event(:event_9, :two_days_from_now_at_4pm, :prefers_email)

      it "returns empty array when time criteria aren't met", tz: "UTC" do
        travel_to("2 days from now at 4:01pm") do
          expect(events).to be_empty
        end
      end

      it "returns events that match type, timing criteria", tz: "UTC" do
        travel_to("2 days from now at 3:45pm") do
          expect(events).to match_array([event_2, event_5, event_6, event_7, event_8, event_9])
        end
      end

      it "returns events that match type, timing criteria and including confirmed ones", tz: "UTC" do
        event_8.confirm!

        travel_to("2 days from now at 3:46pm") do
          expect(events).to match_array([event_2, event_5, event_6, event_7, event_8, event_9])
        end
      end

      it "returns events that match type, timing criteria and excludes canceled ones", tz: "UTC" do
        event_8.cancel!

        travel_to("2 days from now at 3:47pm") do
          expect(events).to match_array([event_2, event_5, event_6, event_7, event_9])
        end
      end

      it "returns events that have no logs associated", tz: "UTC" do
        travel_to("2 days from now at 3:48pm") do
          event_8.email_logs
            .create!(
          source: EventLog::SOURCE[:REQUEST_CONFIRMATION],
          user_type: EventLog::USER_TYPE[:CLIENT]
          )

          expect(events).to match_array([event_2, event_5, event_6, event_7, event_9])
        end
      end

      it "returns events that have not been deleted", tz: "UTC" do
        event_8.update_attributes(deleted_at: Time.now)

        travel_to("2 days from now at 3:49pm") do
          expect(events).to match_array([event_2, event_5, event_6, event_7, event_9])
        end
      end
    end
  end
end
