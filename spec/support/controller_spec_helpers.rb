
module ControllerSpecHelpers
  RSpec::Matchers.define :be_unauthorized do
    expected_status_code = Rack::Utils.status_code(:unauthorized)

    match do |response|
      if response.content_type == "text/html"
        response.should render_template('shared/unauthorized')
      end
      response.status.should == expected_status_code
    end

    failure_message do |response|
      "expected #{response.status} to be #{expected_status_code}"
    end
  end

  RSpec::Matchers.define :be_not_found do
    expected_status_code = Rack::Utils.status_code(:not_found)

    match do |response|
      if response.content_type == "text/html"
        response.should render_template('shared/not_found')
      end
      response.status.should == expected_status_code
    end

    failure_message do |response|
      "expected #{response.status} to be #{expected_status_code}"
    end
  end

  RSpec::Matchers.define :be_created do
    expected_status_code = Rack::Utils.status_code(:created)

    match do |response|
      response.status.should == expected_status_code
    end

    failure_message do |response|
      "expected #{response.status} to be #{expected_status_code}"
    end
  end

  RSpec::Matchers.define :be_accepted do
    expected_status_code = Rack::Utils.status_code(:accepted)

    match do |response|
      response.status.should == expected_status_code
    end

    failure_message do |response|
      "expected #{response.status} to be #{expected_status_code}"
    end
  end

  RSpec::Matchers.define :be_unprocessable do
    expected_status_code = Rack::Utils.status_code(:unprocessable_entity)

    match do |response|
      response.status.should == expected_status_code
    end

    failure_message do |response|
      "expected #{response.status} to be #{expected_status_code}"
    end
  end
end