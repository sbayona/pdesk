module TimeTravelHelper
  def travel_to(time, &block)
    Timecop.travel(Chronic.parse(time), &block)
  end

  def chronic_tz(tz = 'UTC')
    Chronic.time_class = ActiveSupport::TimeZone.new(tz)
    yield
  ensure
    Chronic.time_class = Time
  end
end

RSpec.configure do |config|
  config.include(TimeTravelHelper)

  config.around do |example|
    tz = example.metadata[:tz]

    if tz
      chronic_tz(example.metadata[:tz]) do
        example.run
      end
    else
      example.run
    end
  end
end
