include Warden::Test::Helpers

module FeatureHelpers
  def login_with(user)
    fill_in "email", with: user.email
    fill_in "password", with: user.password
    click_button "Sign in"
  end

  def logout(user)
    click_link user.name
    click_link "Logout"
  end

  def login(user)
    login_as user
    user
  end
end
