source 'https://rubygems.org'
ruby "2.4.3"

#core app
gem 'rails', '~> 4.2'
gem 'pg', '0.20'
gem 'sass-rails'
gem 'uglifier'
gem 'coffee-rails'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'turbolinks'
gem 'jbuilder'
gem 'unicorn-rails'
gem 'rake', '< 11.0'

# app-specific gems
# gem 'sms_confirm', path: "../sms_confirm"
gem 'devise'
gem 'haml-rails'
gem 'therubyracer'
gem 'bootstrap-sass'
gem 'twitter-typeahead-rails', github: 'yourabi/twitter-typeahead-rails'
gem 'simple_form'
gem 'hogan_assets'
gem 'kaminari'
gem 'paper_trail', '~> 4.0.0.rc'
gem 'soft_deletion'
gem 'devise_invitable'
gem 'fullcalendar-rails', github: "martianinteractive/fullcalendar-rails"
gem 'cancan'
gem 'liquid'
gem 'delayed_job_active_record'
gem 'daemons'
gem 'full-name-splitter'
gem 'micromachine'
gem 'whenever', require: false
gem 'flashlight', github: "martianinteractive/flashlight"
gem 'textacular'
gem 'twilio-ruby'
gem 'exception_notification'
gem 'phony_rails'
gem 'stripe'
gem 'figaro' #configuration
gem 'money-rails'
gem 'chronic'
gem 'draper', '2.1.0'  # Decorators/View-Models for Rails Applications
gem 'wicked_pdf'
gem 'sequenced' # Generate scoped sequential IDs for ActiveRecord models
gem 'flutie' # view helpers
gem 'actionview-encoded_mail_to'
gem 'nprogress-rails'
gem 'virtus'
gem 'activeadmin', github: 'activeadmin'
gem "rails_best_practices"
gem "redcarpet"
gem "chart-js-rails"
gem "carrierwave"
gem "ivona_speech_cloud"

group :development, :test do
  gem 'pry-rails'
  gem 'pry-byebug'
  gem 'letter_opener'
  gem 'active_record_query_trace'
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'i18n-tasks'
end

group :development do
  gem 'capistrano', require: false
  gem 'capistrano-rails', require: false
  gem 'capistrano-rvm', require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano3-postgres', require: false
  gem 'quiet_assets'
  gem 'spring-commands-rspec'
  gem 'rack-mini-profiler', require: false
  gem 'better_errors'
  gem "binding_of_caller"
end

group :test do
  gem 'minitest'
  gem 'rspec-rails'
  gem 'capybara-webkit'
  gem 'shoulda-matchers', require: false
  gem 'shoulda-callback-matchers'
  gem 'vcr'
  gem 'webmock'
  gem 'database_cleaner'
  gem 'capybara'
  gem 'machinist', '>= 2.0.0.beta2'
  gem 'simplecov', :require => false, :group => :test
  gem 'guard-rspec', :require => false
  gem 'timecop'
  gem 'email_spec'
  gem 'stripe-ruby-mock'
  gem 'test_xml'
  gem 'rspec-activemodel-mocks'
end
